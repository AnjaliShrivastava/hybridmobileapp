/*package com.company.auto;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.Random;
import org.json.JSONArray;
import org.json.JSONObject;

import com.company.pages.PagesAcceptance;
import com.company.pages.PagesCheckList;
import com.company.pages.PagesDeliveriesTransfers;
import com.company.pages.PagesHomeMenus;

public class BCCheckList extends TestBase {


	public BCCheckList(MyClient client) {
		this.client = client;
	}

	public void search(Map<String, String> data) {

		if (data.get("AWBPrefix").isEmpty() == false) {
			//client.click("WEB", PagesHomeMenus.btnAWB, 0, 1);
			client.click("WEB", PagesHomeMenus.txtPrefixNumber, 0, 1);
			client.sendText(data.get("AWBPrefix"));
			client.closeKeyboard();
			client.click("WEB", PagesHomeMenus.txtSerialNumber, 0, 1);
			client.sendText(data.get("AWBSerial"));
			client.closeKeyboard();

		} else {
			client.report("AWB value in datasheet not updated properly", false);
		}

		client.assertJUnitAndSeeTestReport(client.waitForElement("WEB",
				"xpath=//*[contains(text(),'" + data.get("AWBPrefix") + " " + data.get("AWBSerial") + "')]", 0, 10000),
				"Autopopulate Search - Success", "Autopopulate Search - Failed");
		if (data.get("AutoPopulate").equalsIgnoreCase("YES")) {
			// ********** Click on Auto Suggest List********

			client.click("WEB",
					"xpath=//*[contains(text(),'" + data.get("AWBPrefix") + " " + data.get("AWBSerial") + "')]", 0, 1);
		} else {
			client.click("WEB", PagesHomeMenus.btnSearch, 0, 1);
		}

	}

	public void selectParts(Map<String, String> data) {
		client.assertJUnitAndSeeTestReport(client.waitForElement("WEB", PagesCheckList.PCs, 0, 8000),
				"Records are displaying", " Records are not displaying");
		client.click("WEB", PagesCheckList.PCs, 0, 1);		

		//client.assertJUnitAndSeeTestReport(client.isElementFound("WEB", PagesCheckList.checkListName, 0),"CheckList Name is displaying", "CheckList Name is not displaying");

		if(client.isElementFound("WEB", "xpath=//*[@text='Restart']")) {
			client.click("WEB", "xpath=//*[@text='Restart']", 0, 1);

			//client.waitForElement("WEB", PagesCheckList.checkListName, 0, 8000);
		}else {
			client.click("WEB", "xpath=//*[@text='VERIFIED']", 0, 1);

		}

	}

	public JSONObject  getAPIData(String AWBData) {
		JSONObject GetChecklistDetails= null;
		try {

			String Auth ="Bearer eyJhbGciOiJSUzI1NiJ9.eyJqdGkiOiJpZCIsImlhdCI6MTUyOTU2MjQxOCwic3ViIjoiNWE1MzU1YzAwOWJlYmYzNzU1ODBhMTRlIiwidXNlcl9uYW1lIjoibWljaGFlbC52aXJheUBjaGFtcC5hZXJvIiwiY2lkIjoiY2NzIiwiYXV0aG9yaXRpZXMiOlsiNWE2ZWNmYmVhOWJkNmEyZDE5YmMyOTMzIl0sInNjb3BlIjpbIlVTRVIiXSwic3RhdGlvbiI6eyJjb2RlIjoiWlJIIiwidHlwZSI6IkFJUlBPUlQifSwic3VicyI6WyJjc3AtaGFuZGxpbmctYWNjZXB0YW5jZSIsImNzcC1oYW5kbGluZy1haXJ3YXliaWxsIiwiY3NwLWhhbmRsaW5nLWJ1aWxkdXAiLCJjc3AtaGFuZGxpbmctY2hlY2tpbiIsImNzcC1oYW5kbGluZy1jb250YWluZXIiLCJjc3AtaGFuZGxpbmctZGlzY3JlcGFuY3kiLCJjc3AtaGFuZGxpbmctcGlja3VwIiwiY3NwLWhhbmRsaW5nLXRyYW5zcG9ydG1lYW5zIiwiY3NwLWhhbmRsaW5nLXdhcmVob3VzZSIsImNzcC1oYW5kbGluZy1yZWZlcmVuY2UtZGF0YSIsImNzcC1oYW5kbGluZy1jaGVja2xpc3QiLCJxdW90ZSJdLCJjbGllbnRfaWQiOiI1YTBlOTQ5NGU0NDk1OWVmY2FiMzU4NTgiLCJleHAiOjE1MzgyMDI0MTh9.J1a0EE_GEIOAe6qSfZIJ3LHArHOf0zNsrAhv_j3Ax0sd66qHr_CFwZCGAY64-scGJ0eOoNypx2HBIsBr_CciMJN8kQmYAADXZkvzOOVgooi80A1oFjIdAHWRgKcTObfx6lum-t4idbdTmbqca8H1AQvUXPEpceh0RirMmbbe9izgz-dTQ9EuBBA2-tzlcs0zI9GXnIOuiWmhfoqGYSy8VDLJSZfferPMNnyBs9YhY9adkNpWtfxNsjS0P2qLAbCNyFkSK50oNV4ruvlnoQxPvPVNgbAV4KBdtSX-VK45pns3ft2EwUo7epxwvVajnPpRxsgkjuQRA1Yk9bmaPpCMGg";
			String url = "http://cs-v-mobilitydev1.champ.aero/checklists/v1/airwaybills/"+AWBData ;
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			con.setRequestMethod("GET");
			con.setRequestProperty("accept", "application/json");
			con.setRequestProperty("Content-Type", "application/json");
			con.setRequestProperty("Authorization", Auth);
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;

			StringBuffer response = new StringBuffer();
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();

			JSONObject obj_JSONObject = new JSONObject(response.toString());

			JSONObject obj_embedded =obj_JSONObject.getJSONObject("_embedded");

			JSONArray obj_parts = obj_embedded.getJSONArray("parts");

			JSONObject obj_StrtPart = obj_parts.getJSONObject(0);

			JSONObject obj_embedded2 =obj_StrtPart.getJSONObject("_embedded");

			JSONArray checklistArray = obj_embedded2.getJSONArray("checklists");

			JSONObject firstChecklist =checklistArray.getJSONObject(0);

			String templateName = firstChecklist.getString("templateName");

			String CheckListid = firstChecklist.getString("id");

			String status = firstChecklist.getString("status");

			String AWBID = obj_JSONObject.getString("id");

			String PartID = obj_StrtPart.getString("id");

			System.out.println(CheckListid);

			System.out.println(AWBID);


			System.out.println(PartID);
			GetChecklistDetails = GetChecklistDetails(templateName ,AWBID,PartID,CheckListid);

		}catch(Exception e) {
			System.out.println(e.getMessage());
		}return GetChecklistDetails;
	}



	public JSONObject  GetChecklistDetails(String templateName ,String AWBID,String PartID, String CheckListid) {
		JSONObject obj_JSONObject = null;
		try {
			String Auth ="Bearer eyJhbGciOiJSUzI1NiJ9.eyJqdGkiOiJpZCIsImlhdCI6MTUyOTU2MjQxOCwic3ViIjoiNWE1MzU1YzAwOWJlYmYzNzU1ODBhMTRlIiwidXNlcl9uYW1lIjoibWljaGFlbC52aXJheUBjaGFtcC5hZXJvIiwiY2lkIjoiY2NzIiwiYXV0aG9yaXRpZXMiOlsiNWE2ZWNmYmVhOWJkNmEyZDE5YmMyOTMzIl0sInNjb3BlIjpbIlVTRVIiXSwic3RhdGlvbiI6eyJjb2RlIjoiWlJIIiwidHlwZSI6IkFJUlBPUlQifSwic3VicyI6WyJjc3AtaGFuZGxpbmctYWNjZXB0YW5jZSIsImNzcC1oYW5kbGluZy1haXJ3YXliaWxsIiwiY3NwLWhhbmRsaW5nLWJ1aWxkdXAiLCJjc3AtaGFuZGxpbmctY2hlY2tpbiIsImNzcC1oYW5kbGluZy1jb250YWluZXIiLCJjc3AtaGFuZGxpbmctZGlzY3JlcGFuY3kiLCJjc3AtaGFuZGxpbmctcGlja3VwIiwiY3NwLWhhbmRsaW5nLXRyYW5zcG9ydG1lYW5zIiwiY3NwLWhhbmRsaW5nLXdhcmVob3VzZSIsImNzcC1oYW5kbGluZy1yZWZlcmVuY2UtZGF0YSIsImNzcC1oYW5kbGluZy1jaGVja2xpc3QiLCJxdW90ZSJdLCJjbGllbnRfaWQiOiI1YTBlOTQ5NGU0NDk1OWVmY2FiMzU4NTgiLCJleHAiOjE1MzgyMDI0MTh9.J1a0EE_GEIOAe6qSfZIJ3LHArHOf0zNsrAhv_j3Ax0sd66qHr_CFwZCGAY64-scGJ0eOoNypx2HBIsBr_CciMJN8kQmYAADXZkvzOOVgooi80A1oFjIdAHWRgKcTObfx6lum-t4idbdTmbqca8H1AQvUXPEpceh0RirMmbbe9izgz-dTQ9EuBBA2-tzlcs0zI9GXnIOuiWmhfoqGYSy8VDLJSZfferPMNnyBs9YhY9adkNpWtfxNsjS0P2qLAbCNyFkSK50oNV4ruvlnoQxPvPVNgbAV4KBdtSX-VK45pns3ft2EwUo7epxwvVajnPpRxsgkjuQRA1Yk9bmaPpCMGg";
			String url = "http://cs-v-mobilitydev1.champ.aero/checklists/v1/airwaybills/"+AWBID+"/parts/"+PartID+"/checklists/"+CheckListid ;
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			con.setRequestMethod("GET");
			con.setRequestProperty("accept", "application/json");
			con.setRequestProperty("Content-Type", "application/json");
			con.setRequestProperty("Authorization", Auth);
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;

			StringBuffer response = new StringBuffer();
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();

			obj_JSONObject = new JSONObject(response.toString());

			System.out.println(obj_JSONObject);

		}catch( Exception e) {
			System.out.println(e.getMessage());
		}
		return obj_JSONObject;
	}









	public void PrepareACheckList(Map<String, String> data) throws Exception {
		try {
			JSONObject obj_JSONObject = getAPIData(data.get("AWBID"));
			System.out.println(obj_JSONObject);

			String CheckListName = obj_JSONObject.getString("name");
			client.waitForElement("WEB","xpath=//*[contains(text(),'"+CheckListName+"')]", 0, 8000);

			client.assertJUnitAndSeeTestReport(client.isElementFound("WEB", "xpath=//*[contains(text(),'"+CheckListName+"')]",0),CheckListName+" is displayed" , CheckListName+"is not displaying");

			// *******Getting Section Name **********
			JSONArray obj_sections = obj_JSONObject.getJSONArray("sections");
			int totalSections = obj_sections.length();
			//***********First Loop starts here *************
			for (int k = 0; k < totalSections; k++) {
				int nextCount = 0;
				JSONObject obj_Section = obj_sections.getJSONObject(k);
				System.out.println(obj_Section);
				String sectionName = obj_Section.getString("name");
				if (k==0) {
					client.click("WEB","xpath=//*[contains(text(),'"+ sectionName +"')]" , 0, 1);


				}
				client.waitForElement("WEB","xpath=//*[contains(text(),'"+ sectionName +"')]", 0, 10000);
				client.assertJUnitAndSeeTestReport(client.isElementFound("WEB","xpath=//*[contains(text(),'"+ sectionName +"')]", 0), sectionName +" is displayed", sectionName+ "is not displayed");
				// *******Getting Screens **********
				JSONArray obj_questionGroups = obj_Section.getJSONArray("questionGroups");
				int totalScreen = obj_questionGroups.length();
				//***** Second Loop Starts here ***********
				for (int a = 0; a < totalScreen; a++) {
					JSONObject obj_firstScreen = obj_questionGroups.getJSONObject(a);
					System.out.println(obj_firstScreen);
					// *******Getting Questions **********
					JSONArray obj_questions = obj_firstScreen.getJSONArray("questions");
					int totalQuestions = obj_questions.length();

					// ******** Third Loop Starts here ***********
					for (int i = 0; i < totalQuestions; i++) {
						JSONObject obj_ques = obj_questions.getJSONObject(i);
						// *******Getting Question values **********
						String question = obj_ques.getString("question");
						System.out.println(question);
						String questionNumber = obj_ques.getString("questionNumber");
						//						client.waitForElement("WEB","xpath=//*[contains(text(),'"+questionNumber+"."+question.replace("\r", "").replace("\n", "")+"')]",0,8000);
						//
						client.assertJUnitAndSeeTestReport(client.isElementFound("WEB","xpath=//*[contains(text(),'"+questionNumber+"."+question+"')]"), questionNumber+"."+question+ " is displayed ", questionNumber+"."+question+ " is not displayed" );
						//						boolean mandatoryStatus = obj_ques.getBoolean("mandatory");
						//												if (obj_ques.has("questionNote")) {
						//													 String questionNote = obj_ques.getString("questionNote");
						//													 client.assertJUnitAndSeeTestReport(client.isElementFound("WEB","xpath=//*[contains(text(),'"+questionNote+"')]"), "QuestionNote Exist", "QuestionNote doesn't Exist" );
						//
						//													 
						//												}
						//						if (mandatoryStatus == true) {
						//							client.assertJUnitAndSeeTestReport(client.isElementFound("WEB","xpath=//*[@src='assets/images/mandatorystar.svg']"), " Mandatory mark is displayed ", "Mandatory mark is not displayed" );
						//
						//						}

						// *******Getting Answers **********

						JSONObject obj_answerRange = obj_ques.getJSONObject("answerRange");

						String obj_answerType = obj_answerRange.getString("answerType");

						String questionText =questionNumber+"."+question.replace("\r", "").replace("\n", "");

						if (obj_answerRange.has("options"))  {


							JSONArray obj_options = obj_answerRange.getJSONArray("options");
							int totalOptions = obj_options.length();

							for (int j = 0; j < totalOptions; j++) {

								JSONObject obj_firstAnswer = obj_options.getJSONObject(j);

								String Answer_Option = obj_firstAnswer.getString("option");

								//						client.assertJUnitAndSeeTestReport(client.isElementFound("WEB","xpath=//*[@text='"+Answer_Option+"']"), Answer_Option+" Button is available on screen ", Answer_Option+" Button is not available on screen " );
								client.assertJUnitAndSeeTestReport(client.isFoundIn("WEB", "xpath=//*[@class='checklist-content-list item-padding label label-md']"+"["+(i+1)+"]", 0, "Inside", "WEB", "xpath=//*[@text='"+Answer_Option+"']", 0, 0), Answer_Option+" Option is available on screen ", Answer_Option+" Option is not available on screen " );			

								//								if(questionText.length()>=35 ) {
								//									client.assertJUnitAndSeeTestReport(client.isFoundIn("WEB", questionText, 0, "Down", "WEB", "xpath=//*[@text='"+Answer_Option+"']", 0, 0), Answer_Option+" Button is available on screen ", Answer_Option+" Button is not available on screen " );
								//								} 
							}


							if(questionText.length()<34 && i!=0 ) {
								JSONObject obj_firstAnswer = obj_options.getJSONObject(0);

								String Answer_Option = obj_firstAnswer.getString("option");

								client.clickIn("WEB","xpath=//*[contains(text(),'" + questionNumber+"."+question.replace("\r", "").replace("\n", "") +"')]", 0, "Down","WEB", "xpath=//*[contains(text(),'"+ Answer_Option + "')]", 0, 0, 0, 1);

							}else {

								Random r = new Random();
								int random = r.nextInt(totalOptions);
								JSONObject obj_firstAnswer = obj_options.getJSONObject(random);

								System.out.println(obj_firstAnswer);

								String Answer_Option = obj_firstAnswer.getString("option");

								client.setWebAutoScroll(false);

								int hight = Integer.parseInt(client.elementGetProperty("WEB", "xpath=//*[@class='checklist-content-list item-padding label label-md']"+"["+(i+1)+"]", 0, "height"));
								int headerhight = Integer.parseInt(client.elementGetProperty("WEB", "xpath=//*[@class='toolbar-background toolbar-background-md']", 0, "height"));


								System.out.println(hight);


								client.clickIn("WEB","xpath=//*[@class='checklist-content-list item-padding label label-md']"+"["+(i+1)+"]", 0, "Inside","WEB", "xpath=//*[contains(text(),'"+ Answer_Option + "')]", 0, 0, 0, 1);

								client.setDragStartDelay(300);        
								client.drag("WEB", "xpath=//*[@class='checklist-content-list item-padding label label-md']"+"["+(i+1)+"]", 0, 0, -(hight + headerhight));

								//client.elementSwipe("WEB","xpath=//*[@class='checklist-content-list item-padding label label-md']"+"["+(i+1)+"]", 0, "UP", 500, 1000);

								//client.elementSwipe("WEB","xpath=//*[@class='checklist-content-list item-padding label label-md']"+"["+(i+1)+"]", 0, "UP", hight, 1000);

							}
						}

						//								if(i== 0) {
						//									client.click("WEB", "xpath=//*[contains(text(),'"+ Answer_Option +"')]", 0, 1);
						//
						//								}else {
						//									//client.elementSwipeWhileNotFound("WEB", "xpath=//*[contains(text(),'"+ questionNumber+"."+question.replace("\r", "").replace("\n", "") +"')]" , "Down", 100, 2000, "WEB", "xpath=//*[contains(text(),'"+ Answer_Option + "')]", 0, 1000, 1, false);
						//									//client.swipeWhileNotFound("Down", 800, "WEB","xpath=//*[contains(text(),'"+ questionNumber+"."+question.replace("\r", "").replace("\n", "") +"') + and @hidden='false']", 1000, 1, false);
						//									//client.click("WEB", "xpath=//*[contains(text(),'"+ Answer_Option +"')]", 0, 1);
						//									client.clickIn("WEB","xpath=//*[contains(text(),'" + questionNumber+"."+question.replace("\r", "").replace("\n", "") +"')]", 0, "Down","WEB", "xpath=//*[contains(text(),'"+ Answer_Option + "')]", 0, 0, 0, 1);
						//									//client.clickIn("WEB", "xpath=//*[@text='                                                                                                ' and ./*[./*[@text='"+questionNumber+"."+question.replace("\r", "").replace("\n", "")+" ']]]", 0, "Inside", "WEB", "xpath=//*[contains(text(),'"+ Answer_Option + "')]", 0, 0, 0, 1);
						//
						//									//client.clickIn("WEB", "xpath=//*[@text='                                                                                                ' and ./*[./*[@text='2..When laboratory animals, such as monkeys, which may carry diseases communicable to humans are being shipped,has the carrier(s) been advised in orderto make the necessary arrangements? ']]]", 0, "INSIDE", "WEB", "xpath=//*[@text='YES']", 0, 0, 0, 1);
						//			} }}		



						else if (obj_answerType.equalsIgnoreCase("media")) {


						} else if (obj_answerType.equalsIgnoreCase("signature")) {
							client.click("WEB", PagesCheckList.signatureIcon, 2, 1);
							client.elementSwipe("WEB", PagesCheckList.signatureArea, 0, "Right", 20, 15);
							client.click("WEB", PagesCheckList.saveButton, 0, 1);

						} else if (obj_answerType.equalsIgnoreCase("numeric") || obj_answerType.equalsIgnoreCase("integer")) {
							client.waitForElement("WEB", PagesCheckList.checklistTextField, 0, 8000);
							client.click("WEB", PagesCheckList.checklistTextField, 0, 1);
							client.sendText("1234");
							client.closeKeyboard();

						} else {
							client.waitForElement("WEB", PagesCheckList.checklistTextField, 0, 8000);
							client.click("WEB", PagesCheckList.checklistTextField, 0, 1);
							client.sendText("Testing an String fields");
							client.closeKeyboard();

						}
					}

					if(nextCount!= totalScreen-1) {
						client.assertJUnitAndSeeTestReport(client.waitForElement("WEB", PagesCheckList.nextButton, 0, 8000),
								" Next Button is displayed on screen ", "Next Button is not displayed on screen");
						client.click("WEB", PagesCheckList.nextButton, 0, 1);

						nextCount ++;


						//Code for done also

					}else {

						client.assertJUnitAndSeeTestReport(client.waitForElement("WEB", PagesCheckList.doneButton, 0, 8000)," Done Button is displayed on screen ", "Done Button not is displayed on screen");
						client.click("WEB", PagesCheckList.doneButton, 0, 1);
					}


				}





			}

			client.assertJUnitAndSeeTestReport(client.waitForElement("WEB", PagesCheckList.completionText, 0, 12000), "Checklist Completion text is displayed", "Checklist Completion text is not displayed");

			client.drag("WEB",PagesCheckList.completionText, 0, 0, -300);
			Thread.sleep(2000);

			client.assertJUnitAndSeeTestReport(client.isElementFound("WEB", "xpath=//*[@value='verified']" ), "VERIFIED Button is diaplyed", "VERIFIED Button is not displayed");
			client.assertJUnitAndSeeTestReport(client.isElementFound("WEB", "xpath=//*[@text='Rejected']" ), "REJECTED Button is diaplyed", "REJECTED Button is not displayed");



			//client.swipeWhileNotFound("Down", 800, "WEB","xpath=//*[@nodeName='IMG' and @hidden='false']", 1000, 1, false);

			//client.assertJUnitAndSeeTestReport(client.isElementFound("WEB", PagesCheckList.signatureIcon ), "Signature Section is diaplyed", "Signature section is not displayed");

			client.click("WEB", PagesCheckList.signatureIcon, 0, 1);
			client.waitForElement("WEB", "xpath=//*[@text='Signature']", 0, 3000);
			client.elementSwipe("WEB", PagesCheckList.signatureArea, 0, "Right", 20, 10);
			client.click("WEB", PagesCheckList.saveButton, 0, 1);

			//			client.waitForElement("WEB", PagesCheckList.saveButton, 0, 8000);
			//			client.click("WEB", PagesCheckList.saveButton, 0, 1);
			//			client.assertJUnitAndSeeTestReport(client.isElementFound("WEB", PagesCheckList.lblSaved ), "Saved Message is displayed", "Saved Message is not displayed");
			//




		}catch (Exception e) {

			System.out.println("Exception occured" + e.getMessage());
		}
	}


	public void VerifiedCheckList(Map<String, String> data) throws Exception {

		try {
			JSONObject obj_JSONObject = getAPIData(data.get("AWBID"));
			System.out.println(obj_JSONObject);
			String CheckListName = obj_JSONObject.getString("name");
			client.waitForElement("WEB","xpath=//*[contains(text(),'"+CheckListName+"')]", 0, 8000);

			client.assertJUnitAndSeeTestReport(client.isElementFound("WEB", "xpath=//*[contains(text(),'"+CheckListName+"')]",0),CheckListName+" is displayed" , CheckListName+"is not displaying");

			// *******Getting Section Name **********
			JSONArray obj_sections = obj_JSONObject.getJSONArray("sections");
			int totalSections = obj_sections.length();
			//***********First Loop starts here *************
			for (int k = 0; k < totalSections; k++) {

				JSONObject obj_Section = obj_sections.getJSONObject(k);
				// *******Getting Screens **********
				JSONArray obj_questionGroups = obj_Section.getJSONArray("questionGroups");
				int totalQues = obj_questionGroups.length();
				//***** Second Loop Starts here ***********
				for (int a = 0; a < totalQues; a++) {
					JSONObject obj_firstScreen = obj_questionGroups.getJSONObject(a);

					// *******Getting Questions **********
					JSONArray obj_questions = obj_firstScreen.getJSONArray("questions");
					int totalQuestions = obj_questions.length();

					// ******** Third Loop Starts here ***********
					for (int i = 0; i < totalQuestions; i++) {
						JSONObject obj_ques = obj_questions.getJSONObject(i);
						// *******Getting Question values **********
						
					
					
						String question = obj_ques.getString("question");
//						question= question.replaceAll("“", "").replaceAll("�?", "");
//						question =question.replace("?", "");
//						question =+question+(char)34; 	
						
						
					
						String questionNumber = obj_ques.getString("questionNumber");
						client.assertJUnitAndSeeTestReport(client.isElementFound("WEB","xpath=//*[contains(text(),'"+questionNumber+" "+question+"')]"), questionNumber+question+ " is displayed ", questionNumber+question+ " is not displayed" );
						JSONArray obj_answerRange = obj_ques.getJSONArray("answers");

						JSONObject obj_Ans = obj_answerRange.getJSONObject(0);

						JSONArray obj_answerValue = obj_ques.getJSONArray("answerValues");
						JSONObject obj_AnsValue = obj_answerValue.getJSONObject(0);

						String Answer = obj_AnsValue.getString("value");

						client.assertJUnitAndSeeTestReport(client.isElementFound("WEB","xpath=//*[contains(text(),'"+Answer+"')]"), Answer+ " is displayed ", Answer+ " is not displayed" );



					}	}}											 

		}catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}

	
	


	public void serachPageValidation() throws InterruptedException {

		client.waitForElement("WEB", PagesCheckList.titleCheckList, 0, 8000);
		client.assertJUnitAndSeeTestReport(client.isElementFound("WEB", PagesCheckList.titleCheckList, 0), "CheckList Title displayed", "CheckList Title doesn't display");
		client.assertJUnitAndSeeTestReport(client.isElementFound("WEB", PagesCheckList.lblStartChecklist, 0), "Start CheckList text displayed", "Strat CheckList Text doesn't display");
		client.assertJUnitAndSeeTestReport(client.isElementFound("WEB", PagesCheckList.titleCheckList, 0), "CheckList Title displayed", "CheckList Title doesn't display");
		client.assertJUnitAndSeeTestReport(client.isElementFound("WEB",PagesHomeMenus.lblEnterNumber), "Enter Number label exists", "Enter Number label does not exist");
		client.assertJUnitAndSeeTestReport(client.isElementFound("WEB",PagesHomeMenus.lblorScan), "or Scan label exists", "or Scan label does not exist");
		client.assertJUnitAndSeeTestReport(client.isElementFound("WEB",PagesHomeMenus.btnAWB), "AWB toggle button exists", "AWB toggle not exists");
		client.assertJUnitAndSeeTestReport(client.isElementFound("WEB",PagesHomeMenus.btnScanBarcode), "Scan button exists", "Scan button does not exist");
		client.assertJUnitAndSeeTestReport(client.isElementFound("WEB",PagesHomeMenus.btnSearch), "Search button exists", "Searchbutton does not exist");
		client.assertJUnitAndSeeTestReport(client.isElementFound("WEB",PagesHomeMenus.txtPrefixNumber), "Prefix text box exists", "Prefix text box does not exist");
		client.assertJUnitAndSeeTestReport(client.isElementFound("WEB",PagesHomeMenus.txtSerialNumber), "Serial text box exists", "Serial text box does not exist");

		String specialChar = "A&&#$NJALi%^&" ;


		// ****** AWB VALIDATION *********


		client.click("WEB",PagesHomeMenus.btnAWB, 0, 1);
		client.click("WEB", PagesHomeMenus.txtPrefixNumber, 0, 1);
		client.sendText("950");
		client.closeKeyboard();
		client.click("WEB",PagesHomeMenus.txtSerialNumber, 0, 1);
		client.sendText("08856908");
		client.closeKeyboard();			
		client.click("WEB", PagesHomeMenus.btnSearch, 0, 1);
		client.assertJUnitAndSeeTestReport(client.isElementFound("WEB", "xpath=//*[@text='Invalid AWB Number']"), "AWB error message is displaying", "AWB error Message is not displaying");
	}
}



*/