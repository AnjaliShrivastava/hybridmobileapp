package com.company.auto;

import java.util.Map;

import com.company.pages.PagesAcceptance;
import com.company.pages.PagesHomeMenus;
import com.company.pages.PagesIrregularities;
import com.company.pages.PagesStatusAndHistory;
import com.experitest.client.Client;

public class BCStatusAndHistory extends TestBase {





	public void search(Map<String, String> data) {

		if (data.get("SearchUsing").equalsIgnoreCase("AWB")) {
			client.click("WEB", PagesHomeMenus.btnAWB, 0, 1);
			client.click("WEB", PagesHomeMenus.txtPrefixNumber, 0, 1);
			client.sendText(data.get("AWBPrefix"));
			client.closeKeyboard();
			client.click("WEB", PagesHomeMenus.txtSerialNumber, 0, 1);
			client.sendText(data.get("AWBSerial"));
			client.closeKeyboard();
			client.waitForElement("WEB", "//*[contains(text(),'"+data.get("AWBSerial")+"') and @class='highlight']", 0, 12000);
			client.click("WEB", "//*[contains(text(),'"+data.get("AWBSerial")+"') and @class='highlight']", 0, 1);

		} else if(data.get("SearchUsing").equalsIgnoreCase("HWB")) {
			client.click("WEB",PagesHomeMenus.btnHWB , 0, 1);
			ClickSendText(PagesHomeMenus.txtSerialNumber,data.get("HWB"));
			if(data.get("AutoPopulate").equalsIgnoreCase("YES")){
				client.waitForElement("WEB", "xpath=//*[contains(text(),'"+ data.get("HWB")+"')]", 0, 12000);
				client.click("WEB", "xpath=//*[contains(text(),'"+ data.get("HWB")+"')]", 0, 1);

			}
		}else if(data.get("SearchUsing").equalsIgnoreCase("ULD")) {
			client.click("WEB", PagesHomeMenus.btnULD, 0, 1);
			ClickSendText(PagesHomeMenus.txtULD,data.get("ULD"));
			client.waitForElement("WEB","xpath=//*[contains(text(),'"+data.get("ULD")+"')]", 0, 12000);
			client.click("WEB", "xpath=//*[contains(text(),'"+data.get("ULD")+"')]", 0, 1);


		}else if(data.get("SearchUsing").equalsIgnoreCase("Location")) {
			client.click("WEB", PagesStatusAndHistory.ICLocationField, 0, 1);
			client.sendText(data.get("Location"));
			client.closeKeyboard();
			client.waitForElement("WEB","xpath=//*[contains(text(),'"+data.get("Location")+"')]", 0, 12000);
			client.click("WEB", "xpath=//*[contains(text(),'"+data.get("Location")+"')]", 0, 1);


		}





	}

	public void relocate(Map<String, String> data) {
		if(data.get("Orientation").equalsIgnoreCase("Landscape")) {
			client.verifyElementFound("Web", PagesStatusAndHistory.lblCurrentLocation, 0);
		}
		client.click("WEB", PagesStatusAndHistory.pcsInputField, 0, 1);
		client.sendText(data.get("PCs"));
		client.closeKeyboard();
		client.click("WEB", PagesStatusAndHistory.relocateButton, 0, 1);
		client.waitForElement("Web", PagesStatusAndHistory.locationField, 0, 9000);
		client.click("WEB", PagesStatusAndHistory.locationField, 0, 1);
		client.sendText(data.get("Location"));
		client.closeKeyboard();
		client.click("WEB", PagesStatusAndHistory.saveButton, 0, 1);


		//if (client.isElementFound("Web", PagesStatusAndHistory.lblSaved, 0)==true ) {

		client.verifyElementFound("Web", PagesStatusAndHistory.lblSaved, 0);
		/*}else if((client.isElementFound("Web", "xpath=//*[@text='Error']"))== true){
			System.out.println("API issues ");*/


	}
	public void ULDRelocate(Map<String, String> data) {

		client.click("WEB", PagesStatusAndHistory.locationField, 0, 1);
		client.sendText(data.get("Location"));
		client.closeKeyboard();
		client.click("WEB", PagesStatusAndHistory.ULDStatusdropdown, 0, 1);

		client.click("WEB", "xpath=//*[contains(text(),'"+data.get("ULDStatus")+"')]", 0, 1);


		client.click("WEB", PagesStatusAndHistory.saveButton, 0, 1);


		client.verifyElementFound("Web", PagesStatusAndHistory.lblSaved, 0);



	}

	public void PauseCheck(Map<String, String> data) throws InterruptedException {
		int i=0 ; 
		while(client.isElementFound("Web", PagesStatusAndHistory.NClbl ,0) == true) {

			client.click("Web", PagesStatusAndHistory.PiecesFieldIV, i, 1);
			client.sendText(data.get("PCs"));
			client.closeKeyboard();
			
			client.click("Web","xpath=//*[contains(text(),'"+data.get("Location")+"')]" , 0, 1);
			if(data.get("Orientation").equalsIgnoreCase("Landscape")){
			i=1;
			}
		}
		client.click("Web", PagesStatusAndHistory.PAUSEBUTTON, 0, 1);
		client.waitForElement("Web", PagesStatusAndHistory.IVCheckSummary, 0, 4000);
		client.verifyElementFound("WEB", PagesStatusAndHistory.IVCheckSummary, 0);
		client.verifyElementFound("Web", PagesStatusAndHistory.FINALIZEBUTTON, 0);
		client.verifyElementFound("WEB", "xpath=//*[contains(text(),'"+data.get("Location")+"')]", 0);
		client.verifyElementFound("WEB", "xpath=//*[contains(text(),'"+data.get("PCs")+"')]", 0);
		client.verifyElementFound("WEB", PagesStatusAndHistory.PausedText, 0);
		client.click("Web", PagesStatusAndHistory.PausedText, 0, 1);
		if(data.get("Orientation").equalsIgnoreCase("Landscape")){
		client.waitForElement("Web", PagesStatusAndHistory.COMPLETEBUTTON, 0, 8000);
		}else {
			Thread.sleep(4000);
		}
		client.click("Web", PagesStatusAndHistory.COMPLETEBUTTON, 0, 1); 
		client.waitForElement("Web", PagesStatusAndHistory.IVCheckSummary, 0, 4000);
		client.verifyElementFound("WEB", PagesStatusAndHistory.CompletedText, 0);
		client.click("Web", PagesStatusAndHistory.FINALIZEBUTTON, 0, 1); 
		client.waitForElement("WEB", PagesAcceptance.successfullySavedMessage, 0, 4000);
		//client.verifyElementFound("WEB", PagesAcceptance.successfullySavedMessage , 0);
		//client.verifyElementFound("WEB", PagesStatusAndHistory.oneLocationCompleted, 0);
		client.verifyElementFound("WEB", PagesStatusAndHistory.PrintButton, 0);
		client.verifyElementFound("WEB", PagesStatusAndHistory.EmailButton, 0);
	}

	public void PauseFinalize(Map<String, String> data) {			
		client.click("Web", PagesStatusAndHistory.PiecesFieldIV, 0, 1);
		client.sendText(data.get("PCs"));
		client.closeKeyboard();
		client.click("Web", PagesStatusAndHistory.NClbl, 0, 1);

		client.click("Web", PagesStatusAndHistory.PAUSEBUTTON, 0, 1);
		client.waitForElement("Web", PagesStatusAndHistory.IVCheckSummary, 0, 4000);
		client.verifyElementFound("WEB", PagesStatusAndHistory.IVCheckSummary, 0);
		client.verifyElementFound("Web", PagesStatusAndHistory.FINALIZEBUTTON, 0);
		client.verifyElementFound("WEB", "xpath=//*[contains(text(),'"+data.get("Location")+"')]", 0);
		client.verifyElementFound("WEB", "xpath=//*[contains(text(),'"+data.get("PCs")+"')]", 0);
		client.verifyElementFound("WEB", PagesStatusAndHistory.PausedText, 0);

		client.click("Web", PagesStatusAndHistory.FINALIZEBUTTON, 0, 1);
		client.verifyElementFound("WEB", PagesStatusAndHistory.incompleteWarning, 0);
		client.verifyElementFound("WEB", PagesStatusAndHistory.ProceedButton, 0);
		client.click("Web", PagesStatusAndHistory.ProceedButton, 0, 1);
		client.waitForElement("WEB", PagesAcceptance.successfullySavedMessage, 0, 4000);
		//client.verifyElementFound("WEB", PagesAcceptance.successfullySavedMessage , 0);
		//client.verifyElementFound("WEB", PagesStatusAndHistory.oneLocationCompleted, 0);
		client.verifyElementFound("WEB", PagesStatusAndHistory.PrintButton, 0);
		client.verifyElementFound("WEB", PagesStatusAndHistory.EmailButton, 0);
	}


	public void AddShipmentAndFinalize(Map<String, String> data) {			
		client.click("Web", PagesStatusAndHistory.PiecesFieldIV, 0, 1);
		client.sendText(data.get("PCs"));
		client.closeKeyboard();
		client.click("Web", PagesStatusAndHistory.NClbl, 0, 1);

		client.click("Web", PagesStatusAndHistory.ADDSHIPMENT, 0, 1);		

		client.waitForElement("Web", PagesStatusAndHistory.lblAddShipments, 0, 4000);
		client.click("WEB", PagesHomeMenus.btnAWB, 0, 1);
		client.click("WEB", PagesHomeMenus.txtPrefixNumber, 0, 1);
		client.sendText(data.get("AWBPrefix"));
		client.closeKeyboard();
		client.click("WEB", PagesHomeMenus.txtSerialNumber, 0, 1);
		client.sendText(data.get("AWBSerial"));
		client.closeKeyboard();
		client.waitForElement("Web", "xpath=//*[contains(text(),'" + data.get("AWBPrefix") + " " + data.get("AWBSerial") + "')]", 0, 9000);
		client.click("WEB","xpath=//*[contains(text(),'" + data.get("AWBPrefix") + " " + data.get("AWBSerial") + "')]", 0, 1);



		client.waitForElement("Web", PagesStatusAndHistory.PiecesFieldIV, 0, 4000);
		client.click("WEB", PagesStatusAndHistory.PiecesFieldIV, 0, 1);
		client.sendText(data.get("PCs"));
		client.closeKeyboard();
		client.click("Web", PagesStatusAndHistory.saveButton, 0, 1);
		client.waitForElement("Web", PagesStatusAndHistory.SuccessfullyAddedMSd, 0, 4000);
		client.verifyElementFound("WEB", PagesStatusAndHistory.SuccessfullyAddedMSd, 0);
		client.waitForElement("Web", "xpath=//*[contains(text(),'"+data.get("Location")+"')]", 0, 4000);
		client.verifyElementFound("WEB", "xpath=//*[contains(text(),'"+data.get("Location")+"')]", 0);
		client.verifyElementFound("WEB", "xpath=//*[contains(text(),'" + data.get("AWBPrefix") + "-" + data.get("AWBSerial") + "')]", 0);

		//client.verifyIn("WEB", "xpath=//*[contains(text(),'" + data.get("AWBPrefix") + "-" + data.get("AWBSerial") + "')]", 0, "Right", "Web", "xpath=//*[contains(text(),'"+data.get("PCs")+"')]", 0, 0);


		client.click("Web", PagesStatusAndHistory.ADDSHIPMENT, 0, 1);					
		client.waitForElement("Web", PagesStatusAndHistory.lblAddShipments, 0, 4000);				
		client.click("WEB",PagesHomeMenus.btnHWB , 0, 1);
		ClickSendText(PagesHomeMenus.txtSerialNumber,data.get("HWB"));

		client.waitForElement("WEB", "xpath=//*[contains(text(),'"+ data.get("HWB")+"')]", 0, 12000);
		client.click("WEB", "xpath=//*[contains(text(),'"+ data.get("HWB")+"')]", 0, 1);

		client.waitForElement("Web", PagesStatusAndHistory.PiecesFieldIV, 0, 4000);
		client.click("WEB", PagesStatusAndHistory.PiecesFieldIV, 0, 1);
		client.sendText(data.get("PCs"));
		client.closeKeyboard();
		client.click("Web", PagesStatusAndHistory.saveButton, 0, 1);
		client.waitForElement("Web", PagesStatusAndHistory.SuccessfullyAddedMSd, 0, 4000);
		client.verifyElementFound("WEB", PagesStatusAndHistory.SuccessfullyAddedMSd, 0);
		client.waitForElement("Web", "xpath=//*[contains(text(),'"+data.get("Location")+"')]", 0, 4000);
		client.verifyElementFound("WEB", "xpath=//*[contains(text(),'"+data.get("Location")+"')]", 0);
		client.verifyElementFound("WEB", "xpath=//*[contains(text(),'"+"HWB-" +data.get("HWB")+"')]", 0);
		//client.verifyIn("WEB", "xpath=//*[contains(text(),'"+"HWB-" +data.get("HWB")+"')]", 0, "Right", "Web", "xpath=//*[contains(text(),'"+data.get("PCs")+"')]", 0, 0);


		client.click("Web", PagesStatusAndHistory.COMPLETEBUTTON, 0, 1);
		client.waitForElement("Web", PagesStatusAndHistory.IVCheckSummary, 0, 4000);
		client.verifyElementFound("WEB", PagesStatusAndHistory.IVCheckSummary, 0);
		client.verifyElementFound("Web", PagesStatusAndHistory.FINALIZEBUTTON, 0);
		client.click("Web", PagesStatusAndHistory.FINALIZEBUTTON, 0, 1);
		client.waitForElement("WEB", PagesAcceptance.successfullySavedMessage, 0, 4000);
		//client.verifyElementFound("WEB", PagesAcceptance.successfullySavedMessage , 0);
		//client.verifyElementFound("WEB", PagesStatusAndHistory.oneLocationCompleted, 0);
		client.verifyElementFound("WEB", PagesStatusAndHistory.PrintButton, 0);
		client.verifyElementFound("WEB", PagesStatusAndHistory.EmailButton, 0);
	}
	
	
	public void MultipleLocationFinalize(Map<String, String> data) {			
		client.click("Web", PagesStatusAndHistory.PiecesFieldIV, 0, 1);
		client.sendText(data.get("PCs"));
		client.closeKeyboard();
		client.click("Web", PagesStatusAndHistory.NClbl, 0, 1);

		client.click("Web", PagesStatusAndHistory.COMPLETEBUTTON, 0, 1);
		client.waitForElement("Web", PagesStatusAndHistory.IVCheckSummary, 0, 4000);
		client.verifyElementFound("WEB", PagesStatusAndHistory.IVCheckSummary, 0);
		client.verifyElementFound("Web", PagesStatusAndHistory.FINALIZEBUTTON, 0);
		client.verifyElementFound("WEB", "xpath=//*[contains(text(),'"+data.get("Location")+"')]", 0);
		
		client.verifyElementFound("WEB", PagesStatusAndHistory.CompletedText, 0);
        client.click("Web", PagesStatusAndHistory.NEXTLOCATIONBUTTON, 0, 1);
        client.waitForElement("WEB",PagesStatusAndHistory.ICLocationField, 0, 3000);
        client.click("WEB", PagesStatusAndHistory.ICLocationField, 0, 1);
		client.sendText(data.get("NewLocation"));
		client.closeKeyboard();
		client.waitForElement("WEB","xpath=//*[contains(text(),'"+data.get("NewLocation")+"')]", 0, 8000);
		client.click("WEB", "xpath=//*[contains(text(),'"+data.get("NewLocation")+"')]", 0, 1);
		client.click("Web", PagesStatusAndHistory.PiecesFieldIV, 0, 1);
		client.sendText(data.get("PCs"));
		client.closeKeyboard();
		client.click("Web", PagesStatusAndHistory.NClbl, 0, 1);

		client.click("Web", PagesStatusAndHistory.COMPLETEBUTTON, 0, 1);
		client.verifyElementFound("WEB", PagesStatusAndHistory.IVCheckSummary, 0);
		client.verifyElementFound("Web", PagesStatusAndHistory.FINALIZEBUTTON, 0);
		client.click("Web", PagesStatusAndHistory.FINALIZEBUTTON, 0, 1);
		client.waitForElement("WEB", PagesAcceptance.successfullySavedMessage, 0, 4000);
		//client.verifyElementFound("WEB", PagesAcceptance.successfullySavedMessage , 0);
		//client.verifyElementFound("WEB", PagesStatusAndHistory.oneLocationCompleted, 0);
		client.verifyElementFound("WEB", PagesStatusAndHistory.PrintButton, 0);
		client.verifyElementFound("WEB", PagesStatusAndHistory.EmailButton, 0);
	}
	

	public void ViewLocationHistory(Map<String, String> data) {			
		client.verifyElementFound("WEB", PagesStatusAndHistory.ShipmentDetails, 0);
		client.verifyElementFound("WEB", PagesStatusAndHistory.lblCurrentLocations, 0);
		client.verifyElementFound("WEB", PagesStatusAndHistory.lblInPieces, 0);
		client.verifyElementFound("WEB", PagesStatusAndHistory.lblOutPieces, 0);
		client.verifyElementFound("WEB", PagesHomeMenus.MenuLocationHistory, 0);
}
	

















}














