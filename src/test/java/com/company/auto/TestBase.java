package com.company.auto;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.rules.TestName;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;

import com.experitest.client.Client;
import com.experitest.client.GridClient;
import com.experitest.client.InternalException;
import com.jayway.jsonpath.DocumentContext;
/*import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;*/
import com.jayway.jsonpath.JsonPath;

public class TestBase {

	private int port;
	private String host;
	protected String usedDeviceName;		
	protected boolean didFail = false;
	protected Throwable exception = null;
	public static Client client = null;
	protected GridClient gridClient = null;	
	public Properties cloudProperties = new Properties();
	public Map<String, String> data = null;	
	public String testname ;





	@BeforeMethod
	public void setUp(Method method) throws Exception {
		
		testname = method.getName();			
		getTestData();	
		if (getProperty("Tool").equalsIgnoreCase("SeeTestAutomation")) {
			initClientAndDevice();
			client.setDevice(usedDeviceName);
			System.out.println(method);

			/*	
		if (application == null) {
			application = new BCHomeApplication();
		}
		if ("true".equalsIgnoreCase(getProperty("app.install.onInit"))) {
			if (usedDeviceName.startsWith("adb:")) {
				application.adbinstall(getProperty("app.location"));
			} else {
				application.iosinstall(getProperty("app.location"));
			}			
		}*/
			//client.clearDeviceLog();
			/*if (usedDeviceName.startsWith("adb:")) {
			application.adbinit(false,data);
		} else {
			application.iosinit(false,data);
		}*/
		}
		
	else if (getProperty("Tool").equalsIgnoreCase("SeeTestIo")) {
	//init("@serialnumber='"+getProperty("deviceID")+"'",  name.getMethodName() );
	init("@os='"+getProperty("platform")+"' and @category='"+getProperty("DeviceType")+"'", testname);	
		
	if(getProperty("platform").equalsIgnoreCase("IOS")) {
		
		client.install("cloud:com.champ.cargosystemsapp:"+getProperty("IOSversion"), true, false);
		client.launch("com.champ.cargosystemsapp", true, true);
		//client.startMonitor("com.champ.cargosystemsapp");
		client.startMonitor("com.champ.cargosystemsapp:battery");
		client.setMonitorPollingInterval(1000);
		}else {

	client.install("cloud:com.champ.cargosystemsapp/.MainActivity:"+getProperty("Androidversion"), true, false);
	client.launch("com.champ.cargosystemsapp/.MainActivity", true, true);
	//client.startMonitor("com.champ.cargosystemsapp/.MainActivity");
	client.startMonitor("com.champ.cargosystemsapp:battery");
	client.setMonitorPollingInterval(1000);
	
	}
	client.deviceAction(getProperty("Orientation"));
	}

			
	}
	
	public  String getProperty(String property, Properties props) {
		if (System.getProperty(property) != null) {
			return System.getProperty(property);
		} else if (System.getenv().containsKey(property)) {
			return System.getenv(property);
		} else if (props != null) {
			return props.getProperty(property);
		}
		return null;
	}

	private void initCloudProperties() throws FileNotFoundException, IOException {
		FileReader fr = new FileReader("cloud.properties");
		cloudProperties.load(fr);
		fr.close();
	}


	public void init(String deviceQuery, String testName) throws Exception {
		try {
		System.out.println("init method");
		initCloudProperties();
		System.out.println("initCloudProperties client");
		String url = getProperty("url", cloudProperties);
		String accessKey = getProperty("accessKey", cloudProperties);
		System.out.println("accessKey client");
		if (accessKey != null && !accessKey.isEmpty()) {
			System.out.println("grid client");
			gridClient = new GridClient(accessKey, url);
		} else {
			String username = getProperty("username", cloudProperties);
			String password = getProperty("password", cloudProperties);
			String project = getProperty("project", cloudProperties);
			gridClient = new GridClient(username, password, project, url);
		}
		String adhocDeviceQuery = System.getenv("deviceQuery");
		System.out.println("adhocDeviceQuery");
		if (adhocDeviceQuery != null) {
			System.out.println("[INFO] Redirecting test to the current device.");
			deviceQuery = adhocDeviceQuery;
		}
		System.out.println(deviceQuery);
		System.out.println(gridClient);
		System.out.println(testName);
		System.out.println(deviceQuery);
		client = gridClient.lockDeviceForExecution(testName, deviceQuery, 30, 300000);
		System.out.println("lockDeviceForExecution");
		File reporterDir = new File(System.getProperty("user.dir"), "reports");
		reporterDir.mkdirs();
		client.setReporter("xml", reporterDir.getAbsolutePath(), testName);
		}catch(Exception e) {
			System.out.println(e.getMessage());
		}
		//getTestData(getTestName());
	}
	@AfterMethod
	public void tearDown() {

		/*if (didFail) {
			System.out.println("<--- FAIL --- " + exception.toString() + " --->");
		} else {
			System.out.println("<--- SUCCESS --- " + usedDeviceName + " --- " + getTestName() + " :Test Case ##"
					+ name.getMethodName() + ") --->");
		}
		if ((didFail) && (exception == null)) {
			client.report("Test failed with null exception", false);
		}
		if ((didFail) && (exception instanceof InternalException)) {
			String cause = ((InternalException) exception).getCauseType();
			if (cause != null) {
				switch (cause) {
				case ("UNKNOWN"):
					//
					break;
				case ("INTERNAL_ERROR"):
					//
					break;
				case ("STOP_BY_USER"):
					//
					break;
				case ("USER_INPUT_ERROR"):
					//
					break;
				case ("DEVICE_INTERACTION"):
					//
					break;
				case ("ELEMENT_IDENTIFICATION"):
					//
					break;
				case ("OPERATION_FAILURE"):
					//
					break;
				}
			}
		}
	
		if ((didFail) && (!(exception instanceof AssertionError))) {
			client.report(exception.toString(), false);
		}
	*/	try{
			//client.releaseClient();
			client.getMonitorsData("D:\\MonitorData\\data.csv");
			client.generateReport();
			
		}
		catch (Exception e) {
		}
			client.generateReport();

	}

	

	public List<String> GetResponse(String urlvalue,String jsonxpath) throws Exception {



		System.out.println("urlvalue : "+urlvalue+ " : jsonxpath " +jsonxpath);



		String urlPath = getProperty("apiurl")+urlvalue;
		String headervalue = getProperty("apitoken");
		System.out.println(urlPath);
		String jsonData;
		List<String> values = null;
		URL url = new URL(urlPath);
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		con.setRequestMethod("GET");
		con.setRequestProperty("accept", "application/json");
		con.setRequestProperty("Content-Type", "application/json");
		con.setRequestProperty("Authorization", headervalue);

		try {
			System.out.println(con.getResponseCode());
			if (con.getResponseCode() == 200) {

				BufferedReader reader = new BufferedReader(new InputStreamReader(con.getInputStream()));
				jsonData = "";

				if ((jsonData = reader.readLine()) != null) {
					System.out.println(jsonData);
				}

				// Get the specific values from the json 
				DocumentContext jsonContext = JsonPath.parse(jsonData);
				values = jsonContext.read(jsonxpath);
				values = new ArrayList<String>(new HashSet<String>(values));
				//values = new ArrayList<String>(values);
				System.out.println("values size = "+values.size());
			}else {
				client.report("API Json details not extracted", false);
			}


		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return values;
	}

	public static String Gettextvalue(String jsonheader) {
		String filePath = getProperty("user.dir") + "\\"+getProperty("jsonfilepath");
		String jsonvalue = null;
		// Get the specific values from the json language file
		JSONParser parser = new JSONParser();

		try {
			Object obj = parser.parse(new FileReader(filePath));

			JSONObject jsonObject =  (JSONObject) obj;

			jsonvalue = (String) jsonObject.get(jsonheader);
			if (jsonvalue.isEmpty()) {
				System.out.println("json value not retrieved");
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println(jsonvalue);


		return jsonvalue;

	}

	public void initClientAndDevice() throws NumberFormatException, BiffException, IOException {
		initHost();
		initPort();
		//client = new MyClient(host, port);
		initDevice();
		initProjectBaseDirectory();
		getTestData(getTestName());
		initReport();		
	}

	private void initHost() {
		if (host == null) {
			host = getProperty("host");
		}
	}

	private void initPort() {
		if (port == 0) {
			port = Integer.valueOf(getProperty("port"));
		}
	}

	private void initDevice() {
		if (usedDeviceName != null) {
			return;
		}
		usedDeviceName = getProperty("device.name").contains(":") ? getProperty("device.name")
				: client.waitForDevice(getProperty("device.name"), 300000);
	}

	private void initProjectBaseDirectory() {
		String projectBaseDirectory = getProperty("user.dir") + getProperty("project.base.directory");
		client.setProjectBaseDirectory(projectBaseDirectory);
	}

	private void initReport() {
		String mainFolder = getProperty("execution.start.time");
		String suiteName = "";
		if (mainFolder == null) {
			mainFolder = "Single Tests\\";
		} else {
			mainFolder += "\\";
			suiteName = getProperty("suite.name") + " ";
		}
		String reportFolder = getProperty("user.dir") + "\\reports\\" + mainFolder + suiteName
				+ usedDeviceName.split(":")[1];
		System.out.println(reportFolder);
		String reportName = getTestName() + " (Test Name " + name.getMethodName() + ")";
		client.setReporter("xml", reportFolder, reportName);

	}

	private void getTestData(String testName) throws BiffException, IOException {
		int z;
		data = new HashMap<String, String>();
		Sheet dataSheet = Workbook.getWorkbook(new File(getProperty("data.spreadsheet.name"))).getSheet(getTestName());
		System.out.println(dataSheet.getRows());
		for (z=1;z<dataSheet.getRows();z++) {
			if (dataSheet.getCell(0, z).getContents().equalsIgnoreCase(name.getMethodName())) {
				System.out.println(name.getMethodName());
				break;
			}
		}

		for (int i = 0; i < dataSheet.getColumns(); i++) {
			String key = dataSheet.getCell(i, 0).getContents();
			System.out.println(key);
			String value = dataSheet.getCell(i, z).getContents();
			System.out.println(value);
			data.put(key, value);
		}
	}

	private static String getProperty(String property) {
		if (System.getProperty(property) != null) {
			return System.getProperty(property);
		}
		File setupPropFile = new File("setup.properties");
		if (setupPropFile.exists()) {
			Properties prop = new Properties();
			FileReader reader;
			try {
				reader = new FileReader(setupPropFile);
				prop.load(reader);
				reader.close();
				return prop.getProperty(property);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	public String getTestName() {		
		String[] klassNameSplit = this.getClass().getName().split("\\.");
		return klassNameSplit[klassNameSplit.length - 1];
	}

	public void ClickClearSendText(String Objname,String Textval) {
		String strzone = "WEB";
		int intindex = 0;
		int inttimeout = 1000;

		client.waitForElement(strzone, Objname, intindex, inttimeout);
		client.click(strzone, Objname, 0, 1);
		int digitcount = client.elementGetProperty(strzone, Objname, intindex, "value").length();
		for (int i=0; i<digitcount;i++) {
			client.sendText("{BKSP}");
		}
		client.sendText(Textval);
		client.closeKeyboard();
	}

	public void clearText(String Objname,String Textval) {
		String strzone = "WEB";
		int intindex = 0;
		int inttimeout = 1000;

		client.waitForElement(strzone, Objname, intindex, inttimeout);
		client.click(strzone, Objname, 0, 1);
		int digitcount = client.elementGetProperty(strzone, Objname, intindex, "value").length();
		for (int i=0; i<digitcount;i++) {
			client.sendText("{BKSP}");
		}

	}



	public void ClickSendText(String Objname,String Textval) {
		String strzone = "WEB";
		int intindex = 0;
		int inttimeout = 1000;

		client.waitForElement(strzone, Objname, intindex, inttimeout);
		client.click(strzone, Objname, 0, 1);		
		client.sendText(Textval);
		client.closeKeyboard();
	}

	@Rule	
	public TestName name = new TestName();

	@Rule
	public TestWatcher rule = new TestWatcher() {
		protected void failed(Throwable e, Description description) {
			didFail = true;
			exception = e;
			tearDown();
		}

		protected void succeeded(Description description) {
			tearDown();
		}
		protected void starting(Description description) {
			System.out.println("Starting test: " + description.getMethodName());
		}
	};
	private HSSFWorkbook workbook;

	public void getTestData() throws IOException {
		int z;
		data = new HashMap<String, String>();
		Row row;
		FileInputStream fileInputStream = new FileInputStream(getProperty("data.spreadsheet.name"));
		workbook = new HSSFWorkbook(fileInputStream);
		HSSFSheet dataSheet = workbook.getSheet(getTestName());

		for (z=1;z<dataSheet.getLastRowNum();z++) {
			row = dataSheet.getRow(z);
			if (row.getCell(0).getStringCellValue().equalsIgnoreCase(testname)) {
				break;
			}
		}

		for (int i = 0; i < dataSheet.getRow(0).getLastCellNum(); i++) {
			row = dataSheet.getRow(0);
			String key = row.getCell(i).getStringCellValue();
			System.out.println(key);
			row = dataSheet.getRow(z);
			String value = row.getCell(i).getStringCellValue();
			System.out.println(value);
			data.put(key, value);
		}
		//workbook.close();
	}

















}