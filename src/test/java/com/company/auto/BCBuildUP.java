package com.company.auto;

import java.util.Map;

import org.junit.Assert;

import com.company.pages.PagesAcceptance;
import com.company.pages.PagesBuildUp;
import com.company.pages.PagesFoundCargo;
import com.company.pages.PagesHomeMenus;
import com.experitest.client.Client;

public class BCBuildUP extends TestBase {

	



	public void search(Map<String, String> data) {

		if (data.get("SearchUsing").equalsIgnoreCase("Flight")== true) {
			if (data.get("Flight").isEmpty()==false) {	
				client.click("WEB", PagesHomeMenus.btnFlight, 0, 1);
				ClickSendText(PagesHomeMenus.txtSearchFlight,data.get("Flight"));
				//*[@text=' XS7801 ']
				client.click("WEB", "//*[contains(text(),' "+data.get("Flight")+"')]",0,1);
				client.click("WEB", PagesBuildUp.productArrow, 0, 1);
				client.click("WEB", "xpath=//*[contains(text(),'"+data.get("Product")+"')]",0,1);
				client.click("WEB", PagesHomeMenus.btnProceed, 0, 1);
			}else {
				client.report("Flight value in datasheet not updated properly", false);
				return;					
			}

		} else {
			client.report("AWB value in datasheet not updated properly", false); 
		} }
	
	
	public void LegSelection(Map<String, String> data) {
		client.waitForElement("WEB", "xpath=//*[contains(text(),'"+data.get("Legs")+"')]",0,5000);
		client.click("WEB", "xpath=//*[contains(text(),'"+data.get("Legs")+"')]",0,1);
		if(data.get("Orientation").equalsIgnoreCase("Landscape")) {
			client.click("WEB", PagesBuildUp.btnPROCEED,0,1);
			}
	}
	
	public void ShowULDs(Map<String, String> data) {
		client.click("WEB", PagesBuildUp.btnSHOWULDs,0,1);
		client.waitForElement("WEB", "xpath=//*[contains(text(),'"+data.get("ULD")+"')]",0,5000);
		client.click("WEB", "xpath=//*[contains(text(),'"+data.get("ULD")+"')]",0,1);
		client.waitForElement("WEB", PagesBuildUp.btnAssignShipment,0,5000);
		client.click("WEB", PagesBuildUp.btnAssignShipment,0,1);
		client.waitForElement("WEB", "xpath=//*[contains(text(),'"+data.get("AWB")+"')]",0,5000);
		client.click("WEB", "xpath=//*[contains(text(),'"+data.get("AWB")+"')]",0,1);
		client.waitForElement("WEB", PagesBuildUp.btnAssign,0,5000);
		client.isElementFound("WEB", "xpath=//*[contains(text(),'"+data.get("AWB")+"')]", 0);
		client.isElementFound("WEB", PagesBuildUp.StatusPRE, 0);
		client.click("WEB", PagesBuildUp.btnAssign,0,1);
		client.verifyElementFound("WEB", PagesAcceptance.successfullySavedMessage, 0);
		client.verifyElementNotFound("WEB", "xpath=//*[contains(text(),'"+data.get("AWB")+"')]",0);
	}
	
	public void ShowShipments(Map<String, String> data) {
		client.click("WEB", PagesBuildUp.btnSHOWSHIPMENT,0,1);
		client.waitForElement("WEB", "xpath=//*[contains(text(),'"+data.get("AWB")+"')]",0,5000);
		client.click("WEB", "xpath=//*[contains(text(),'"+data.get("AWB")+"')]",0,1);
		client.waitForElement("WEB", PagesBuildUp.btnAssign,0,5000);
		client.click("WEB", PagesBuildUp.btnAssign,0,1);
		client.waitForElement("WEB", PagesBuildUp.searchULDField,0,5000);
		client.click("WEB", PagesBuildUp.searchULDField,0,1);
		client.sendText(data.get("ULD"));
		client.closeKeyboard();
		client.isElementFound("WEB", "xpath=//*[contains(text(),'"+data.get("ULD")+"')]",0);
		client.click("WEB", PagesBuildUp.radioButton,0,1);
		client.click("WEB", PagesBuildUp.btnAssign,0,1);
		client.verifyElementFound("WEB", PagesAcceptance.successfullySavedMessage, 0);
		client.verifyElementNotFound("WEB", "xpath=//*[contains(text(),'"+data.get("AWB")+"')]",0);
	}
	
	public void DRAGANDDROP(Map<String, String> data) {
		client.waitForElement("WEB", PagesBuildUp.Filter,0,5000);
		client.click("WEB", PagesBuildUp.Filter,0,1);
		String[] status = {"PRE" , "ACG" , "ACD" , "ACC" , "NOT" ,"ON-HAND" } ;
		for (int i = 1; i < status.length; i++) {
		client.click("WEB", "xpath=//*[contains(text(),'"+status[i]+"')]",0,1);	
		}
		client.click("WEB", PagesBuildUp.btnApply,0,1);
		client.waitForElement("WEB", PagesBuildUp.AWBsearchBar,0,5000);	
		
		ClickSendText(PagesBuildUp.AWBsearchBar,data.get("AWB"));
		client.click("WEB", "xpath=//*[contains(text(),'"+data.get("AWB")+"')]",0,1);	
		
		client.setDragStartDelay(1000);
		// locate the element at coordinates x: 250, y: 100 and clicks on it three times
		client.dragDrop("WEB", "xpath=//*[contains(text(),'"+data.get("AWB")+"')]", 0, "xpath=//*[contains(text(),'"+data.get("ULD")+"')]", 0);
		client.verifyElementFound("WEB", "xpath=//*[contains(text(),'Do you want to assign "+data.get("AWB")+" to "+data.get("ULD")+"')]", 0);
		client.click("WEB", PagesHomeMenus.ProceedAssignment, 0, 1);
		//client.waitForElement("WEB", PagesBuildUp,0,5000);	
	}}


						


				




