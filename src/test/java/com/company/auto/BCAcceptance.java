package com.company.auto;

import java.util.List;
import java.util.Map;

import org.junit.Assert;

import com.company.pages.PagesAcceptance;
import com.company.pages.PagesFoundCargo;
import com.company.pages.PagesHomeMenus;
import com.experitest.client.Client;

public class BCAcceptance extends TestBase {
	long ACClastInstance ;
	long ACCFirstInstance ;
	long  ACCTotalTime ;

	public void AcceptanceSearchPageValidation() {
	}
	public void Search(Map<String, String> data) {
		switch(data.get("SearchUsing")) {
		case "AWB":
			if (data.get("AWBPrefix").isEmpty()==false) {
				client.waitForElement("Web", PagesHomeMenus.btnAWB, 0, 4000);
				client.click("WEB",PagesHomeMenus.btnAWB, 0, 1);
				client.click("WEB",PagesHomeMenus.txtPrefixNumber, 0, 1);
				client.sendText(data.get("AWBPrefix"));
				client.closeKeyboard();
				client.click("WEB",PagesHomeMenus.txtSerialNumber, 0, 1);
				client.sendText(data.get("AWBSerial"));
				client.closeKeyboard();	
				ACCFirstInstance = System.currentTimeMillis();
				
				
				//client.sendText(data.get("AWBPrefix")+"  "+data.get("AWBSerial"));
			}	
			if (data.get("AutoPopulate").equalsIgnoreCase("YES")){
				client.waitForElement("WEB", "//*[contains(text(),'"+data.get("AWBSerial")+"') and @class='highlight']", 0, 12000);
				client.verifyElementFound("WEB", "//*[contains(text(),'"+data.get("AWBSerial")+"') and @class='highlight']", 0);
				ACClastInstance = System.currentTimeMillis();
				ACCTotalTime = ACClastInstance - ACCFirstInstance;
				client.report("Time taken in displaying AWB Auto suggest value : " +ACCTotalTime +" MilliSeconds  or "+ ACCTotalTime/1000+ " in Seconds", true);
				
				client.click("WEB", "//*[contains(text(),'"+data.get("AWBSerial")+"') and @class='highlight']", 0, 1);
				ACCFirstInstance = System.currentTimeMillis();
			}else {
				client.click("WEB", PagesHomeMenus.btnSearch, 0, 1);	
				ACCFirstInstance = System.currentTimeMillis();
			}

			break;
			//Has to be altered
			//client.click("WEB",PagesAcceptance.lblQuanityPieces , 0, 1);

			//********** Create New Record **************
		case "HWB":
			if(data.get("HWB").isEmpty()==false) {
				client.click("WEB",PagesHomeMenus.btnHWB, 0, 1);
				client.click("WEB",PagesHomeMenus.txtSerialNumber, 0, 1);
				client.sendText(data.get("HWB"));
				client.closeKeyboard();			
			}
			if (data.get("AutoPopulate").equalsIgnoreCase("YES")){
				//Autopopulate Click Event
				client.click("WEB", "xpath=//*[contains(text(),'"+ data.get("HWB")+"')]", 0, 1);

			}else {
				client.click("WEB", PagesHomeMenus.btnSearch, 0, 1);		
			}

			client.click("WEB", "xpath=//*[contains(text(),'"+ data.get("AWBPrefix")+" "+data.get("AWBSerial")+"')]", 0, 1);
		}

	}

	public void CreateNewPart() {
		client.waitForElement("WEB", PagesAcceptance.createNewRecordbutton, 0, 4000);
		client.verifyElementFound("WEB",PagesAcceptance.createNewRecordbutton , 0);
		ACClastInstance = System.currentTimeMillis();
		ACCTotalTime = ACClastInstance - ACCFirstInstance;
		client.report("Time taken in redirect to Acceptance Part page : " +ACCTotalTime +" MilliSeconds  or "+ ACCTotalTime/1000+ " in Seconds", true);
	

		//client.assertJUnitAndSeeTestReport(client.waitForElement("WEB",PagesAcceptance.createNewRecordbutton, 0, 8000),"Create New Record button is displayed","Create New Record button is not displayed");
		client.click("WEB", PagesAcceptance.createNewRecordbutton, 0, 1);
		ACCFirstInstance = System.currentTimeMillis();

	}

	public void updateExistingPart() {

		//client.assertJUnitAndSeeTestReport(client.waitForElement("WEB","xpath=//*[contains(text(),'Pcs')]", 0, 8000),"Create New Record button is displayed","Create New Record button is not displayed");
		client.click("WEB", "xpath=//*[@text='Rcv:']", 0, 1);

	}


	public void shipmentDetails(Map<String, String> data) {

		//		//client.assertJUnitAndSeeTestReport(client.waitForElement("WEB", PagesAcceptance.lblstartExportAcceptance, 0, 3000), "Start an Export Acceptance label exists", "Start an Export Acceptance label does not exist");
		//		//client.assertJUnitAndSeeTestReport(client.isElementFound("WEB", PagesAcceptance.lblshipmentDetails), "Shipment Details label exists", "Shipment Details label does not exist");
		//		//client.assertJUnitAndSeeTestReport(client.isElementFound("WEB", PagesAcceptance.lblFlightDetails), "Flight Details label exists", "Flight Details label does not exist");
		//		//client.assertJUnitAndSeeTestReport(client.isElementFound("WEB", PagesAcceptance.lblPiecestitle), "Pieces label exists", "Pieces label does not exist");
		//		//client.assertJUnitAndSeeTestReport(client.isElementFound("WEB", PagesAcceptance.lblWeight), "weight label exists", "Weight label does not exist");
		//		//client.assertJUnitAndSeeTestReport(client.isElementFound("WEB", PagesAcceptance.lblUnit), "Unit label exists", "Unit label does not exist");
		//		//client.assertJUnitAndSeeTestReport(client.isElementFound("WEB", PagesAcceptance.lblKG), "KG label exists", "KG label does not exist");
		////client.assertJUnitAndSeeTestReport(client.waitForElement("WEB", PagesAcceptance.lblPiecesAlreadyReceived,0,500), "Already Received - Pieces label exists", "Already Received - Pieces label does not exist");
		////client.assertJUnitAndSeeTestReport(client.waitForElement("WEB", PagesAcceptance.lblWtAlreadyReceived,1,500), "Already Received - Weight label exists", "Already Received - Weight label does not exist");
		
		client.waitForElement("WEB", PagesAcceptance.lblstartExportAcceptance, 0, 3000);
		client.verifyElementFound("WEB",PagesAcceptance.lblstartExportAcceptance , 0);
		ACClastInstance = System.currentTimeMillis();
		ACCTotalTime = ACClastInstance - ACCFirstInstance;
		client.report("Time taken in redirect to Start Export Acceptance Screen : " +ACCTotalTime +" MilliSeconds  or "+ ACCTotalTime/1000+ " in Seconds", true);
	
		ClickClearSendText(PagesAcceptance.txtPieces,data.get("Pieces"));
		ClickClearSendText(PagesAcceptance.txtWeight,data.get("Weight"));

	}

	public void Dimensions(Map<String, String> data) {
		//client.assertJUnitAndSeeTestReport(client.isElementFound("WEB", PagesAcceptance.lblDimensions), "Dimensions label exists", "Dimensions label does not exist");
		//client.assertJUnitAndSeeTestReport(client.isElementFound("WEB", PagesAcceptance.lblvolumedetails), "Volume Details label exists", "Volume Details label does not exist");
		client.click("WEB",PagesAcceptance.lblDimensions,0,1);
		//		//client.assertJUnitAndSeeTestReport(client.waitForElement("WEB", PagesAcceptance.lblDimensions, 0, 8000), "Dimensions label exists", "Dimensions label does not exist");
		//		//client.assertJUnitAndSeeTestReport(client.isElementFound("WEB", PagesAcceptance.lblDimensions), "Dimensions title label exists", "Dimensions title label does not exist");
		//		//client.assertJUnitAndSeeTestReport(client.isElementFound("WEB", PagesAcceptance.lbltotalvolume), "TOtal Volume label exists", "Total Volume label does not exist");
		//		//client.assertJUnitAndSeeTestReport(client.isElementFound("WEB", PagesAcceptance.txttotalvolume), "Total Volume text box exists", "Total Volume text does not exist");
		//		//client.assertJUnitAndSeeTestReport(client.isElementFound("WEB", PagesAcceptance.toggleCF), "CF toggle exists", "CF toggle does not exist");
		//		//client.assertJUnitAndSeeTestReport(client.isElementFound("WEB", PagesAcceptance.toggleCMT), "CMT toggle exists", "CMT toggle does not exist");
		//		//client.assertJUnitAndSeeTestReport(client.isElementFound("WEB", PagesAcceptance.toggleINC), "INC toggle exists", "INC toggle does not exist");
		//		//client.assertJUnitAndSeeTestReport(client.isElementFound("WEB", PagesAcceptance.toggleMC), "MC toggle exists", "MC toggle does not exist");
		//		//client.assertJUnitAndSeeTestReport(client.isElementFound("WEB", PagesAcceptance.lblPieces), "Pieces label exists", "Pieces label does not exist");
		//		//client.assertJUnitAndSeeTestReport(client.isElementFound("WEB", PagesAcceptance.lblWeight), "Weight label exists", "Weight label does not exist");
		//		//client.assertJUnitAndSeeTestReport(client.isElementFound("WEB", PagesAcceptance.lblHeight), "Height label exists", "Height label does not exist");
		//		//client.assertJUnitAndSeeTestReport(client.isElementFound("WEB", PagesAcceptance.lblWidth), "Width label exists", "Width label does not exist");
		//		//client.assertJUnitAndSeeTestReport(client.isElementFound("WEB", PagesAcceptance.lbllength), "Length label exists", "Length label does not exist");
		client.click("WEB", PagesAcceptance.txtLength, 0, 1);
		client.sendText(data.get("Length"));
		client.closeKeyboard();
		client.click("WEB", PagesAcceptance.txtWidth, 0, 1);
		client.sendText(data.get("Width"));
		client.closeKeyboard();
		client.click("WEB", PagesAcceptance.txtHeight, 0, 1);
		client.sendText(data.get("Height"));
		client.closeKeyboard();
		if(data.get("Orientation").equalsIgnoreCase("Portrait")) {
			client.click("WEB", PagesAcceptance.btnDone, 0, 1);	
		}
	}

	//public void SHC(Map<String, String> data) {
	//		String[] shcvalues = data.get("SHC").split(",");
	//		System.out.println(shcvalues.length + data.get("SHC"));
	//		System.out.println(shcvalues);
	//		client.click("WEB", PagesAcceptance.lblSHC,0,1);
	//		client.waitForElement("WEB", PagesFoundCargo.lblADDSHC, 0, 2000);
	//		
	//		int counter = 0;
	//		for (String s:shcvalues) {				
	//			client.click("WEB","xpath=//*[@nodeName='INPUT']" , 0, 1);
	//			client.sendText(data.get("SHC"));
	//			client.click("WEB", "xpath=//*[contains(text(),'"+data.get("SHC")+"')]", 0, 1);

	//			counter = counter+1;
	//			System.out.println(counter);
	//			if  ((counter==10) && (data.get("TestId").contains("_N"))){
	//				//client.assertJUnitAndSeeTestReport(client.waitForElement("WEB", "xpath=//*[@text='"+data.get("ExpectedErrorMsg")+"']",0,3000), data.get("ExpectedErrorMsg")+" error message displayed as expected", data.get("ExpectedErrorMsg")+" error message not displayed as expected");
	//				client.click("WEB", PagesFoundCargo.btnOK, 0, 1);
	//				return;
	//			}				

	//		if (data.get("Orientation").equalsIgnoreCase("Portrait")) {
	//			client.click("WEB", PagesHomeMenus	.btnSave,0,1);
	//		
	//		for (String s:shcvalues) {				
	//			client.isElementFound("WEB", "xpath=//*[contains(text(),'"+s+"')]");				
	//		}
	//	}

	public void SHC(Map<String, String> data) {
		if(data.get("Orientation").equalsIgnoreCase("Portrait")) {
			client.swipeWhileNotFound("Down", 800, "WEB","xpath=//*[@text='SHC' and @hidden='false']", 1000, 1, false);
		}
		client.click("WEB", PagesAcceptance.lblSHC,0,1);
		if(data.get("Orientation").equalsIgnoreCase("Portrait")) {
			client.waitForElement("WEB", PagesFoundCargo.lblADDSHC, 0, 8000);
		}else {
			//client.waitForElement("WEB","xpath=//*[@placeholder='Start searching for SHC']", 0, 8000);
		}
		client.click("WEB","xpath=//*[@nodeName='INPUT']" , 0, 1);
		client.sendText(data.get("SHC"));
		client.click("WEB", "xpath=//*[contains(text(),'"+data.get("SHC")+"')]", 0, 1);
		if (data.get("Orientation").equalsIgnoreCase("Portrait")) {
			client.click("WEB", PagesAcceptance.btnDone,0,1);
		}

	}

	public void SHCNegativeValidation(Map<String, String> data) {
		if(data.get("Orientation").equalsIgnoreCase("Portrait")) {
			client.swipeWhileNotFound("Down", 00, "WEB","xpath=//*[@text='SHC' and @hidden='false']", 1000, 1, false);	
		}
		client.click("WEB", PagesAcceptance.lblSHC,0,1);
		client.waitForElement("WEB", PagesFoundCargo.lblADDSHC, 0, 8000);
		client.click("WEB","xpath=//*[@nodeName='INPUT']" , 0, 1);
		client.sendText(data.get("SHC"));
		client.closeKeyboard();

		int totalValue	= client.elementGetTableRowsCount("xpath=//*[@class='scroll-content']", 0, true);
		for (int i = 0; i < totalValue; i++) {
			client.click("WEB", "xpath=//*[contains(text(),'"+data.get("SHC")+"')]", i, 1);

		}

		client.click("WEB", "xpath=//*[contains(text(),'"+data.get("SHC")+"')]", 0, 1);
		if (data.get("Orientation").equalsIgnoreCase("Portrait")) {
			client.click("WEB", PagesHomeMenus	.btnSave,0,1);
		}

	}





	public void OriginDestination(Map<String, String> data) {	
		if(data.get("Orientation").equalsIgnoreCase("Landscape")) {
			client.click("WEB", "//*[contains(text(),' Origin')]", 0, 1);
			client.click("WEB","xpath=//*[@nodeName='INPUT']",0,1);

		}
		else if(data.get("Orientation").equalsIgnoreCase("Portrait")) {
			client.click("WEB", PagesAcceptance.lblOrigin,0,1);
			client.click("WEB", "xpath=//*[@placeholder='Search origin for "+data.get("AWBPrefix")+"-"+data.get("AWBSerial")+"']",0,1);
			//client.waitForElement("WEB", PagesFoundCargo.lblSelectOrigin, 0, 2000);
		}
		
		client.sendText(data.get("Origin"));
		ACCFirstInstance = System.currentTimeMillis();
		client.waitForElement("WEB", "xpath=//*[contains(text(),'"+ data.get("Origin")+"')]", 0, 3000);
		client.verifyElementFound("WEB", "xpath=//*[contains(text(),'"+ data.get("Origin")+"')]", 0);
		ACClastInstance = System.currentTimeMillis();
		ACCTotalTime = ACClastInstance - ACCFirstInstance;
		client.report("Time taken in displaying Origin autosuggest Value : " +ACCTotalTime +" MilliSeconds  or "+ ACCTotalTime/1000+ " in Seconds", true);

		client.click("WEB", "xpath=//*[contains(text(),'"+ data.get("Origin")+"')]", 0, 1);

		//******* Swipe if not found **************//

		if(data.get("Orientation").equalsIgnoreCase("Portrait")) {

			client.swipeWhileNotFound("Down", 800, "WEB","xpath=//*[@text='Destination' and @hidden='false']", 1000, 1, false);	
			client.click("WEB", PagesAcceptance.lblDestination,0,1);
			client.click("WEB", "xpath=//*[@placeholder='Search destination for "+data.get("AWBPrefix")+"-"+data.get("AWBSerial")+"']",0,1);
	}else {
		client.click("WEB","//*[contains(text(),' Destination ')]",0,1);
		client.click("WEB", PagesAcceptance.lblOD,0,1);
	}
		
		
		client.sendText(data.get("Destination"));
		ACCFirstInstance = System.currentTimeMillis();
		
		
		client.waitForElement("WEB", "xpath=//*[contains(text(),'"+ data.get("Destination")+"')]", 0, 8000);
		client.verifyElementFound("WEB", "xpath=//*[contains(text(),'"+ data.get("Destination")+"')]", 0);
		ACClastInstance = System.currentTimeMillis();
		ACCTotalTime = ACClastInstance - ACCFirstInstance;
		client.report("Time taken in displaying Destination autosuggest Value : " +ACCTotalTime +" MilliSeconds  or "+ ACCTotalTime/1000+ " in Seconds", true);

		client.click("WEB", "xpath=//*[contains(text(),'"+ data.get("Destination")+"')]", 0, 1);
	}

	public void handlinDetails(Map<String, String> data) {
		client.click("WEB", PagesAcceptance.lblHandlingDetails,0,1);

		//client.assertJUnitAndSeeTestReport(client.waitForElement("WEB", PagesAcceptance.lblHandlingDetails, 0,8000), "Handling Details label exists", "Handling Details label does not exist");
		client.click("WEB", PagesAcceptance.lblManual, 0, 1);
		client.click("WEB", PagesAcceptance.SSRText, 0, 1);
		client.sendText(data.get("SSR"));
		client.click("WEB", PagesAcceptance.OSIText, 2, 1);
		client.sendText(data.get("OSI"));
		client.closeKeyboard();
		if(data.get("Orientation").equalsIgnoreCase("Portrait")) {
			client.click("WEB", PagesAcceptance.btnDone, 0, 1);
		}

	}

	public void Secured(Map<String, String> data) {
		if(data.get("Orientation").equalsIgnoreCase("Portrait")) {
		client.swipeWhileNotFound("Down", 800, "WEB","xpath=//*[@text=' Secured' and @hidden='false']", 1000, 1, false);	
		//client.assertJUnitAndSeeTestReport(client.isElementFound("WEB", PagesAcceptance.lblSecured), "Secured label exists", "Secured label does not exist");
		}else {
			client.elementSwipeWhileNotFound("WEB", "xpath=//*[@class='left-split-col col']", "Down", 600, 500, "WEB", "xpath=//*[contains(text(),'Security Details') and @hidden='false']", 0, 1000, 3, true);
		}
		String str0 = client.elementGetProperty("WEB", PagesAcceptance.btnBoolean, 0, "aria-checked");
		if(str0.equals("false")) {
		client.click("WEB", PagesAcceptance.btnBoolean, 0, 1);
		
		}
		client.click("WEB", PagesAcceptance.btnBoolean, 1, 1);
		client.click("WEB", "xpath=//*[contains(text(),'"+ data.get("SecurityType")+"')]", 0, 1);	    
	}




	public void OCIAndBlockMenifest(Map<String, String> data) {	
		if(client.isElementFound("WEB", PagesAcceptance.btnBoolean, 0)== true){
		if(data.get("Orientation").equalsIgnoreCase("Portrait")) {

			//client.assertJUnitAndSeeTestReport(client.isElementFound("WEB", PagesAcceptance.lblOCIDetails), "OCI label exists", "OCI label does not exist");

			client.click("WEB", PagesAcceptance.btnBoolean, 2, 1);
		}else {


			client.elementSwipeWhileNotFound("WEB", "xpath=//*[@class='left-split-col col']", "Down", 600, 500, "WEB", "xpath=//*[contains(text(),'Block Manifest') and @hidden='false']", 0, 1000, 3, true);
			client.click("WEB", PagesAcceptance.btnBoolean, 0, 1);


		}
		}
	}

	public void StorageLocation(Map<String, String> data) {	

		//client.assertJUnitAndSeeTestReport(client.isElementFound("WEB", PagesAcceptance.lblStorageLocation), "Storage Location label exists", "Storage Location label does not exist");
		if(data.get("Orientation").equalsIgnoreCase("Landscape")) {
			client.click("WEB", PagesAcceptance.lblStorageLocation, 0, 1);
		}
		//client.swipeWhileNotFound("Down", 800, "WEB","xpath=//*[@placeholder='Storage location' and @hidden='false']", 1000, 1, false);
		ClickClearSendText(PagesAcceptance.StorageLocationField,data.get("Storage Location"));
		  
	}

	public void Remarks(Map<String, String> data) {
		if(data.get("Orientation").equalsIgnoreCase("Landscape")) {
			client.elementSwipeWhileNotFound("WEB", "xpath=//*[@class='left-split-col col']", "Down", 600, 500, "WEB", "xpath=//*[contains(text(),'Remarks') and @hidden='false']", 0, 1000, 3, true);
		}else {
			client.swipeWhileNotFound("Down", 800, "WEB","xpath=//*[contains(text(),'Remarks') and @hidden='false']", 1000, 1, false);
			//client.assertJUnitAndSeeTestReport(client.isElementFound("WEB", PagesAcceptance.Remarks), "Remark label exists", "Remark label does not exist");
			
		}
		client.click("WEB", PagesAcceptance.Remarks, 0, 1);
		//client.assertJUnitAndSeeTestReport(client.waitForElement("WEB", PagesAcceptance.RemarksField,0 ,8000), "Remark label exists", "Remark label does not exist");
		client.click("WEB", PagesAcceptance.RemarksField, 0, 1);
		client.sendText(data.get("Remark"));
		client.closeKeyboard();  
		if(data.get("Orientation").equalsIgnoreCase("Portrait")) {
			client.click("WEB", PagesAcceptance.btnDone, 0, 1);
		}
	}

	public void BUPDetails(Map<String, String> data) {
		
		if(client.isElementFound("WEB", "xpath=//*[contains(text(),'BUP Details') and @hidden='false']", 0)== true ||client.isElementFound("WEB", PagesAcceptance.lblBUP, 0)== true){
		

		if(data.get("Orientation").equalsIgnoreCase("Portrait")) {
			client.swipeWhileNotFound("Down", 800, "WEB","xpath=//*[contains(text(),'BUP Details') and @hidden='false']", 1000, 1, false);
			client.click("WEB", PagesAcceptance.lblBUP, 0, 1);
			//client.assertJUnitAndSeeTestReport(client.waitForElement("WEB", PagesAcceptance.lblBUP ,0 ,8000), "BUP Details label exists", "BUP Details label does not exist");
		}else {
			client.clickIn("WEB","xpath=//*[@class='left-split-col col']", 0, "Inside", "WEB", PagesAcceptance.lblBUP, 0, 0, 0, 1);

		}

		//client.assertJUnitAndSeeTestReport(client.isElementFound("WEB", PagesAcceptance.lblULDIdentifier), "ULD Identifier label exists", "ULD Identifier label does not exist");

		//client.assertJUnitAndSeeTestReport(client.isElementFound("WEB", PagesAcceptance.lblULDStorageLocation), "ULD Storage Location label exists", "ULD Storage Location label does not exist");

		client.click("WEB", PagesAcceptance.ULDIdentifier, 0, 1);

		client.sendText(data.get("ULD Identifier"));

		client.closeKeyboard(); 

		client.click("WEB", PagesAcceptance.ULDStorageLocation, 0, 1);

		client.sendText(data.get("Storage Location"));

		client.closeKeyboard();   



		if(data.get("Orientation").equalsIgnoreCase("Portrait")) {
			client.click("WEB", PagesAcceptance.ULDDestination,0,1);
			client.waitForElement("WEB", PagesAcceptance.lblDestination, 0, 2000);
			client.click("WEB", "xpath=//*[contains(text(),'"+"')]",0,1);
			client.sendText(data.get("Destination"));
			client.click("WEB", "xpath=//*[contains(text(),'"+ data.get("Destination")+"')]", 0, 1);
			client.click("WEB", PagesAcceptance.btnDone, 0, 1);
		}

		}

	}

	public void ReadyForCarriage(Map<String, String> data) {
		if(client.isElementFound("WEB", PagesAcceptance.lblReadyForCarriage, 0)== true ||client.isElementFound("WEB", PagesAcceptance.lblReadyForCarriage, 0)== true){
		if(data.get("Orientation").equalsIgnoreCase("Portrait")) {
			
			client.click("WEB", PagesAcceptance.lblReadyForCarriage, 0, 1);
			//client.assertJUnitAndSeeTestReport(client.waitForElement("WEB", PagesAcceptance.lblReadyForCarriage, 0,8000), "Ready for Carriage label exists", "Ready For Carriage does not exist");
		}else {
			
			client.clickIn("WEB","xpath=//*[@class='left-split-col col']", 0, "Inside", "WEB", PagesAcceptance.lblReadyForCarriage, 0, 0, 0, 1);
			}
		

		client.clickIn("WEB", PagesAcceptance.lblFWB, 0, "Right", "WEB", PagesAcceptance.btnBoolean, 0, 0, 0, 1);
		if(data.get("Orientation").equalsIgnoreCase("Portrait")) {
			client.click("WEB", PagesAcceptance.btnDone, 0, 1);
		}
		}
	}
	

	public void EUPallet(Map<String, String> data) {
		if(data.get("Orientation").equalsIgnoreCase("Portrait")) {
			//client.assertJUnitAndSeeTestReport(client.isElementFound("WEB", PagesAcceptance.lblEUPallet), "EU Pallet label exists", "EU Pallet label does not exist");
		}else {
		client.clickIn("WEB", "xpath=//*[@class='left-split-col col']", 0, "Inside", "WEB", PagesAcceptance.lblEUPallet, 0, 0, 0, 1);
		}
		client.clickIn("WEB", PagesAcceptance.lblEUPallet, 0, "Right", "WEB", PagesAcceptance.PalletIncrementButton, 0, 0, 0, 1);
	

	}

	public void Save() {
		client.click("WEB", PagesHomeMenus.btnSave,0,1);
		ACCFirstInstance = System.currentTimeMillis();	
		
		client.waitForElement("WEB", PagesAcceptance.successfullySavedMessage, 0, 10000);
		ACClastInstance = System.currentTimeMillis();
		ACCTotalTime = ACClastInstance - ACCFirstInstance;
		client.report("Time taken in creating a part post click event on Save button : " +ACCTotalTime +" MilliSeconds  or "+ ACCTotalTime/1000+ " in Seconds", true);


	}
	public void verifyThePart(Map<String, String> data) {
		
		Assert.assertTrue(client.isElementFound("WEB","xpath=//*[contains(text(),'"+data.get("Pieces")+" Pcs "+data.get("Weight")+" KG')]"));

	}
	
	
	

	public void NegativeValidation(Map<String, String> data) {
		String specialChar = "A&&#$NJALi%^&" ;


		// ****** HWB VALIDATION *********


		client.click("WEB",PagesHomeMenus.btnHWB, 0, 1);
		client.click("WEB",PagesHomeMenus.txtSerialNumber, 0, 1);
		client.sendText(data.get("HWB"));
		client.closeKeyboard();			
		client.click("WEB", PagesHomeMenus.btnSearch, 0, 1);
		//client.assertJUnitAndSeeTestReport(client.isElementFound("WEB", PagesAcceptance.HWBErrorMessage), "HWB error message is displaying", "HWB error Message is not displaying");
		clearText(PagesHomeMenus.txtSerialNumber,data.get("HWB"));
		client.click("WEB", PagesHomeMenus.btnSearch, 0, 1);

		client.verifyElementNotFound("WEB", PagesAcceptance.HWBErrorMessage, 0);


		client.click("WEB",PagesHomeMenus.txtSerialNumber, 0, 1);		

		client.sendText(specialChar);

		//client.getAllValues("WEB", PagesHomeMenus.txtSerialNumber, "text");
		String getText = client.elementGetText("WEB", PagesHomeMenus.txtSerialNumber, 0);
		System.out.println(getText);
		//client.assertJUnitAndSeeTestReport(getText.equals(specialChar.replaceAll("[^a-zA-Z0-9]", "")), "Special Character are not allowed", "Special Charcter are allowed");



	}



	public void HWBCard(Map<String, String> data) {	
		ClickClearSendText(PagesAcceptance.txtPieces,data.get("Pieces"));
		//client.assertJUnitAndSeeTestReport(client.isElementFound("WEB", PagesAcceptance.HWBTitle), "HWB Card exists", "HWB Card does not exist");
		client.click("WEB", PagesAcceptance.HWBTitle, 0, 1);
		//client.assertJUnitAndSeeTestReport(client.waitForElement("WEB", PagesAcceptance.HWBTitle, 0, 5000), "HWB title is displayed", "HWB title is not displayed");
		//client.assertJUnitAndSeeTestReport(client.isElementFound("WEB", PagesAcceptance.HWBListTitle), "HWB List text exists", "HWB list text does not exist");
		if(data.get("Orientation").equalsIgnoreCase("Portrait")) {
			client.click("WEB", PagesAcceptance.HWBNumber, 0, 1);
		}else {
			client.click("WEB", "xpath=//*[@text='Pieces Weight Volume']", 0, 1);

		}

		// *********** HWB Details Page **********************//

		//client.assertJUnitAndSeeTestReport(client.waitForElement("WEB", PagesAcceptance.HWBDetails, 0, 5000), "HWB Details title is displayed", "HWB Details title is not displayed");
		//		//client.assertJUnitAndSeeTestReport(client.isElementFound("WEB", PagesAcceptance.lblPiecestitle), "Pieces label exists", "Pieces label does not exist");
		//		//client.assertJUnitAndSeeTestReport(client.isElementFound("WEB", PagesAcceptance.lblWeight), "weight label exists", "Weight label does not exist");
		ClickClearSendText("xpath=//*[@placeholder='Piece(s)']",data.get("Pieces"));
		ClickClearSendText(PagesAcceptance.txtWeight,data.get("Weight"));	
		client.click("WEB","xpath=//*[@placeholder='Location']", 0, 1);
		client.sendText(data.get("Storage Location"));
		client.closeKeyboard(); 

		//***** dimensions ********************//

		client.click("WEB", PagesAcceptance.txtLength, 0, 1);
		client.sendText(data.get("Length"));
		client.closeKeyboard();
		client.click("WEB", PagesAcceptance.txtWidth, 0, 1);
		client.sendText(data.get("Width"));
		client.closeKeyboard();
		client.click("WEB", PagesAcceptance.txtHeight, 0, 1);
		client.sendText(data.get("Height"));
		client.closeKeyboard();
		client.click("WEB", PagesAcceptance.btnDone, 0, 1);	

		//client.assertJUnitAndSeeTestReport(client.waitForElement("WEB", PagesAcceptance.HWBListTitle, 0, 5000), "HWB List title is displayed", "HWB List title is not displayed");

		// ******** HWB List Back ********************** // ************ This code need to be changed once API implements  ****************
		if(data.get("Orientation").equalsIgnoreCase("Portrait")) {
			client.click( "WEB", PagesAcceptance.BackArrow, 0, 1);
		}


	}
}



