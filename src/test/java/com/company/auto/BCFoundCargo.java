package com.company.auto;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import com.company.pages.PagesFoundCargo;
import com.company.pages.PagesHomeMenus;
import com.experitest.client.Client;

import junit.framework.Assert;

public class BCFoundCargo extends TestBase {
	

	
	public void SearchAWB(Map<String, String> data) {
		
		// test  starts
		System.out.println("===============Test================");
		System.out.println(data);
		System.out.println("===============Test================");
		
		
		
		
		// test  ends
		client.waitForElement("WEB", PagesFoundCargo.lblFoundCargo, 0, 2000);
		
		//client.assertJUnitAndSeeTestReport(client.isElementFound("WEB",PagesFoundCargo.lblRegister), "Register a Found Cargo label exists", "Register a Found Cargo label does not exist");
		//client.assertJUnitAndSeeTestReport(client.isElementFound("WEB",PagesHomeMenus.lblEnterNumber), "Enter Number label exists", "Enter Number label does not exist");
		//client.assertJUnitAndSeeTestReport(client.isElementFound("WEB",PagesHomeMenus.lblorScan), "or Scan label exists", "or Scan label does not exist");
		//client.assertJUnitAndSeeTestReport(client.isElementFound("WEB",PagesHomeMenus.btnAWB), "AWB toggle button exists", "AWB toggle buttondoes not exist");
		//client.assertJUnitAndSeeTestReport(client.isElementFound("WEB",PagesFoundCargo.btnUnknownAWB), "UNKNOWN AWB button exists", "UNKNOWN AWB button does not exist");
		if(data.get("AWBPrefix").isEmpty()) {
			client.click("WEB",PagesFoundCargo.btnUnknownAWB, 0,1);
		}else {
			ClickSendText(PagesHomeMenus.txtPrefixNumber,data.get("AWBPrefix"));
			ClickSendText(PagesHomeMenus.txtSerialNumber,data.get("AWBSerial"));
				
			if (data.get("AutoPopulate").equalsIgnoreCase("YES")){
				
				client.waitForElement("WEB", "//*[contains(text(),'"+data.get("AWBSerial")+"') and @class='highlight']", 0, 12000);
				client.click("WEB", "//*[contains(text(),'"+data.get("AWBSerial")+"') and @class='highlight']", 0, 1);
				
			}else {
				client.click("WEB", PagesFoundCargo.btnSearch, 0, 1);		
				if (data.get("TestId").contains("_N")){
					//client.assertJUnitAndSeeTestReport(client.waitForElement("WEB", "xpath=//*[@text='"+data.get("ExpectedErrorMsg")+"']",0,3000), data.get("ExpectedErrorMsg")+" error message displayed as expected", data.get("ExpectedErrorMsg")+" error message not displayed as expected");
				}else {
				//client.assertJUnitAndSeeTestReport(client.waitForElement("WEB", PagesFoundCargo.lblOrigin, 0, 2000),"Found Cargo Report Search - Success","Found Cargo Report Search - Failed");
				}
			}
		}
	}
	
	@SuppressWarnings("deprecation")
	public void RegisterFC(Map<String, String> data) throws Exception {
		//String PieceValue = null;
		//API details
				
	/*	String url = "discrepancy/v1/airwaybills/?airwaybillPrefix="+(data.get("AWBPrefix")+"&airwaybillSerial="+(data.get("AWBSerial")));
		List<String> myobject = null;*/
		
		//client.click("WEB", "xpath=//*[contains(text(),' Origin')]", 0, 1);
		System.out.println("");
		client.waitForElement("WEB", PagesFoundCargo.lblOrigin,0,5000);
		String titlevalue = client.elementGetProperty("WEB", PagesFoundCargo.lbltitle, 0, "text");
		//client.assertJUnitAndSeeTestReport(client.waitForElement("WEB", PagesFoundCargo.btnSaveDisabled, 0, 2000),"Found Cargo Report - Save Disabled as expected","Found Cargo Report - Save Enabled");
		client.click("WEB", PagesFoundCargo.lblOrigin,0,1);
		if(data.get("Orientation").equalsIgnoreCase("Portrait")) {
			client.waitForElement("WEB", PagesFoundCargo.lblSelectOrigin, 0, 2000);
		}
		
		System.out.println("continue 2");
		client.click("WEB", "xpath=//*[@placeholder='"+TestBase.Gettextvalue("searchoriginfor")+" "+titlevalue+"']", 0, 1);
		client.sendText(data.get("Origin"));
		client.click("WEB", "xpath=//*[contains(text(),'"+ data.get("Origin")+"')]", 0, 1);
		
		if(data.get("Orientation").equalsIgnoreCase("Landscape")) {
			client.click("WEB","//*[contains(text(),' Shipment Details')]",0,1);
			
		}
		
		client.click("WEB", PagesFoundCargo.txtNatureofGoods,0,1);
		client.sendText(data.get("NatureofGoods"));
		client.closeKeyboard();
		
		System.out.println("continue 3");
		
		if(data.get("FoundPieces").isEmpty()== false) {
			client.click("WEB", PagesFoundCargo.txtFoundPieces,0,1);
			client.sendText(data.get("FoundPieces"));
			client.closeKeyboard();
			//client.assertJUnitAndSeeTestReport(client.waitForElement("WEB", PagesFoundCargo.btnSaveEnabled, 0, 2000),"Found Cargo Report - Save Enabled as expected","Found Cargo Report - Save button is not enabled");
		}else {
			/*myobject = GetResponse(url,"$..pieces");
			PieceValue = myobject.toString().replace("[","");
			PieceValue = PieceValue.replace("]","");*/
			//client.assertJUnitAndSeeTestReport(client.waitForElement("WEB", PagesFoundCargo.btnSaveEnabled, 0, 2000),"Found Cargo Report - Save Enabled as expected","Found Cargo Report - Save button is not enabled");
			/*if (client.elementGetProperty("WEB", PagesFoundCargo.txtFoundPieces,0,"value").equalsIgnoreCase(PieceValue)) {
				client.report("Found Pieces quantity is matching with the API pieces", true);
			}else {
				client.report("Found Pieces quantity is not matching with the API pieces - "+PieceValue, false);
			}
		}*/
		
		System.out.println("continue 4");
		
		/*if(data.get("FoundPieces").equalsIgnoreCase("Exceeds")) {
			client.click("WEB", PagesFoundCargo.txtFoundPieces,0,1);
			client.sendText("1");
			client.closeKeyboard();
			//client.assertJUnitAndSeeTestReport(client.waitForElement("WEB", PagesFoundCargo.btnSaveDisabled, 0, 2000),"Found Cargo Report - Save Disabled as AWB pieces count exceeds","Found Cargo Report - Save Enabled- Wrong Behaviour");
			client.click("WEB", PagesFoundCargo.txtFoundPieces,0,1);
			client.sendText("{BKSP}");
			client.closeKeyboard();
			//client.assertJUnitAndSeeTestReport(client.waitForElement("WEB", PagesFoundCargo.btnSaveEnabled, 0, 2000),"Found Cargo Report - Save Disabled as AWB pieces count equals","Found Cargo Report - Save Enabled- Wrong Behaviour");
		}*/
		
	/*	System.out.println("continue 5");
		
		String foundpiecesval =  client.elementGetProperty("WEB", PagesFoundCargo.txtFoundPieces,0,"value");*/
		}
		client.click("WEB", PagesFoundCargo.txtFoundWeight,0,1);
		client.sendText(data.get("FoundWeight"));
		client.closeKeyboard();
/*		String foundweightval = client.elementGetProperty("WEB", PagesFoundCargo.txtFoundWeight,0,"value");
		client.isElementFound("WEB", PagesFoundCargo.lblKg);*/
		if (data.get("SHC").isEmpty()) {
			
		}else {	
			String[] shcvalues = data.get("SHC").split(",");
			System.out.println(shcvalues.length + data.get("SHC"));
			System.out.println(shcvalues);
			client.swipe("Down",400,500);
			client.click("WEB", PagesFoundCargo.btnAddSHC,0,1);
			client.waitForElement("WEB", PagesFoundCargo.lblADDSHC, 0, 2000);
			int counter = 0;
			for (String s:shcvalues) {			
				final String txtSearchSHC ="xpath=//*[@placeholder='"+ TestBase.Gettextvalue("searchshcFor")+" "+titlevalue+"']";
				client.click("WEB", txtSearchSHC,0,1);
				client.sendText(s);
				client.click("WEB", "xpath=//*[contains(text(),'"+ s +"')]", 0, 1);
				if (data.get("Orientation").equalsIgnoreCase("Portrait")) {
				client.click("WEB", "xpath=//IMG", 0, 1);
				}
				counter = counter+1;
				System.out.println(counter);
				if  ((counter==10) && (data.get("TestId").contains("_N"))){
					//client.assertJUnitAndSeeTestReport(client.waitForElement("WEB", "xpath=//*[@text='"+data.get("ExpectedErrorMsg")+"']",0,3000), data.get("ExpectedErrorMsg")+" error message displayed as expected", data.get("ExpectedErrorMsg")+" error message not displayed as expected");
					if (data.get("Orientation").equalsIgnoreCase("Portrait")) {
					client.click("WEB", PagesFoundCargo.btnOK, 0, 1);
					}//return;
				}				
			}
			
				//client.click("WEB", PagesFoundCargo.btnSave,0,1);
			
			for (String s:shcvalues) {				
				client.isElementFound("WEB", "xpath=//*[contains(text(),'"+s+"')]");				
			}
			System.out.println("continue 6");	
		}
		
		if (data.get("Orientation").equalsIgnoreCase("Portrait")) {
				client.swipeWhileNotFound("Down", 400,2000, "WEB", PagesFoundCargo.txtLocation, 1000, 5, true);
			
		}else {
			client.isElementFound("WEB", PagesFoundCargo.mnustoragelocationsubtext);
			client.click("WEB", PagesFoundCargo.mnustoragelocation, 0, 1);
		}
		
		client.click("WEB", PagesFoundCargo.txtLocation,0,1);
		client.sendText(data.get("Location"));
		client.closeKeyboard();
		
		/*client.isElementFound("WEB", PagesFoundCargo.lblPreferredLocation);
		client.isElementFound("WEB", PagesFoundCargo.lblNotAvailable);
		client.isElementFound("WEB", PagesFoundCargo.lblStorageLocation);
		
		client.isElementFound("WEB", PagesFoundCargo.lblPieces);
		client.isElementFound("WEB", PagesFoundCargo.lblWeight);
		client.isElementFound("WEB", PagesFoundCargo.lblLocation);*/
		System.out.println("continue 7");
		/*if (foundpiecesval.equalsIgnoreCase(client.elementGetProperty("WEB", PagesFoundCargo.txtPieces,0,"value"))) {
			client.report("Found Pieces quantity is matching with the pieces", true);
		}else {
			client.report("Found Pieces quantity is not matching with the pieces", false);
		}
		*/
		/*if (foundweightval.equalsIgnoreCase(client.elementGetProperty("WEB", PagesFoundCargo.txtWeight,0,"value"))) {
			client.report("Found Weight quantity is matching with the weight", true);
		}else {
			client.report("Found Weight quantity is not matching with the weight", false);
		}*/
		System.out.println("continue 8");		
		client.click("WEB", PagesFoundCargo.btnSave,0,1);
		client.waitForElement("WEB", "xpath=//*[@text='Saved']" ,0, 4000);
		client.verifyElementFound("WEB", "xpath=//*[@text='Saved']",0);
		
		//client.assertJUnitAndSeeTestReport(client.waitForElement("WEB", PagesHomeMenus.lblSuccessfullysaved, 0, 5000), "Found Cargo Report created Successfully", "Found Cargo Report - Not created");
	
	}
}
	