package com.company.auto;

import java.util.Map;

import com.company.pages.PagesAcceptance;
import com.company.pages.PagesHomeMenus;
import com.company.pages.PagesStatusAndHistory;


public class BCHomeApplication extends TestBase{
	long loginFirstInstance   ;
	long loginlastInstance ;
	long totalTime ;
	// If the tool is seeTest Desktop Automation , Uncomment below lines
//protected MyClient client;


	
	public void handleElementIdentificationException() {
		
	}

	
	public void iosinstall(String appLocation) {
		client.install(appLocation + "\\champ.ipa", true, false);
	}
	
	public void adbinstall(String appLocation) {
		client.install(appLocation + "\\champcargosystems.apk", true, false);
	}

	
	public void iosinit(boolean clearData,Map<String, String> data) {
		//client.startMonitor("com.hexaware.gtt.generic3");
		client.launch("com.champ.cargosystemsapp", true, true);
		//client.startMonitor("com.hexaware.gtt.generic3:battery");
		client.waitForElement("WEB",PagesHomeMenus.mnuHome, 0, 12000);
		client.deviceAction(data.get("Orientation"));
	}
	
	public void adbinit(boolean clearData,Map<String, String> data) {
		//client.startMonitor("com.champcargosystems.app:battery");com.champ.cargosystemsapp/.MainActivity
		client.launch("com.champ.cargosystemsapp/.MainActivity",true,true);
		client.waitForElement("WEB",PagesHomeMenus.mnuHome, 0, 12000);
		client.deviceAction(data.get("Orientation"));
}


	public void choosemock() {
		client.waitForElement("WEB",PagesHomeMenus.mnuMore,0,4000);
		client.click("WEB",PagesHomeMenus.mnuMore,0,1);
		client.waitForElement("WEB", PagesHomeMenus.mnuitemAPI, 0, 2000);
		client.click("WEB", PagesHomeMenus.mnuitemAPI, 0, 1);
	}
	
	public void navigateToActivity(String viewName) throws InterruptedException {
		// TODO Auto-generated method stub
				
		switch(viewName) {
		case "HOME":
			client.waitForElement("WEB","//*[contains(text(),' Login')]" , 0,5000 );
			if(client.isElementFound("NATIVE", "xpath=//*[@id='OK']")== true) {
				client.click("NATIVE", "xpath=//*[@id='OK']", 0, 1);
			}
			/*if(client.isElementFound("WEB", "xpath=//*[contains(text(),' Scan to change')]")== true) {
				client.click("WEB", "xpath=//*[contains(text(),' Scan to change')]", 0, 1);	
				if(client.isElementFound("NATIVE", "xpath=//*[@id='OK']") == true) {
					client.click("NATIVE", "xpath=//*[@id='OK']", 0, 1);
				}
				client.simulateCapture("D:\\UATQR.png");
				if(client.isElementFound("NATIVE", "xpath=//*[@id='OK']") == true) {
					client.click("NATIVE", "xpath=//*[@id='OK']", 0, 1);
				}
				client.waitForElement("WEB","xpath=//*[contains(text(),' Scan to change')]" , 0,10000 );
			}*/
		
			
			if(client.isElementFound("Native","//*[@text='ALLOW']", 0)== true ){
			client.click("Native", "//*[@text='ALLOW']", 0, 1);	
			}
			if(client.isElementFound("NATIVE", "xpath=//*[@id='OK']") == true) {
				client.click("NATIVE", "xpath=//*[@id='OK']", 0, 1);
			}
			if(client.isElementFound("WEB","//*[contains(text(),' Krishnakumar g')]", 0)== true ){
				client.click("WEB", "//*[contains(text(),' Krishnakumar g')]", 0, 1);
				loginFirstInstance = System.currentTimeMillis();
				}else {
				if((client.isElementFound("WEB", "//*[contains(text(),' Logout')]", 0))== true ) {
				client.click("WEB", "//*[contains(text(),' Logout')]", 0, 1);				
			} 
				if(client.isElementFound("Native","xpath=//*[@text='ALLOW']", 0)== true ){
					client.click("Native", "//*[@text='ALLOW']", 0, 1);	
					}
			  client.click("WEB","//*[contains(text(),' Select environment')]" , 0,1 );
			 client.waitForElement("WEB","//*[contains(text(),' DEV_UAT')]" , 0,5000 );
			 client.click("WEB","//*[contains(text(),' DEV_UAT')]" , 0,1 );
			client.waitForElement("WEB","//*[contains(text(),' Login')]" , 0,5000 );
			client.click("WEB","//*[contains(text(),' Login')]" , 0,1 );			 
			 
			client.waitForElement("WEB","xpath=//*[@id='login_email']" , 0,4000 );
			client.click("WEB", "xpath=//*[@id='login_email']", 0, 1);
			client.sendText("krishnakumarguna@hexaware.com");
			client.click("WEB", "xpath=//*[@text='Password *']", 0, 1);
			client.sendText("krishna12345");
			client.click("WEB", "xpath=//*[@id='customer']", 0, 1);
			client.sendText("mob");
			
			client.click("WEB", "//*[@id='login_signIn']", 0, 1);
			 loginFirstInstance = System.currentTimeMillis();
				}
			
			//Thread.sleep(4000);
			
			client.waitForElement("WEB","xpath=(//*[@nodeName='IMG'])[1]" , 0,15000 );
			client.verifyElementFound("Web", "xpath=(//*[@nodeName='IMG'])[1]", 0);
			loginlastInstance = System.currentTimeMillis();
			totalTime = loginlastInstance - loginFirstInstance;
			client.report("Time taken to login the APP : " +totalTime +" MILLISECONDs or "+ totalTime/1000+ " in SECONDS", true);
			client.click("WEB", "xpath=(//*[@nodeName='IMG'])[1]", 0, 1);
			
			//verify menu text is available on the menu header			
			//while (client.waitForElement("WEB", PagesHomeMenus.btnArrow, 0,3000 )) {				
			 //client.click("WEB", PagesHomeMenus.btnArrow, 0, 1);	
			//}
			
			break;
		case "DAMAGEREPORT":
					
			Thread.sleep(4000);
			
			client.click("WEB","xpath=//*[contains(text(),'Warehouse')]",0,1);						
			//client.waitForElement("WEB",PagesHomeMenus.mnuitemDamage , 0,2000 );
			client.click("WEB",PagesHomeMenus.mnuitemDamage,0,1);
									
			break;
			
		case "IRREGULARITIES":	
			client.waitForElement("WEB", PagesHomeMenus.txtMenu, 0,1000 );
			client.click("WEB",PagesHomeMenus.mnuitemWarehouse,0,1);						
			client.waitForElement("WEB",PagesHomeMenus.mnuitemIrregularities , 0,2000 );
			client.click("WEB",PagesHomeMenus.mnuitemIrregularities,0,1);						
			break;
			
		case "DELIVERYTRANSFER":
			
			client.waitForElement("WEB", PagesHomeMenus.txtMenu, 0,1000 );
			client.click("WEB",PagesHomeMenus.mnuitemImport,0,1);
						
			client.waitForElement("WEB",PagesHomeMenus.mnuitemDeliveriesTransfers , 0,2000 );
			client.click("WEB",PagesHomeMenus.mnuitemDeliveriesTransfers,0,1);
						
			client.waitForElement("WEB",PagesHomeMenus.mnuitemReceiptBondTransfer , 0,2000);
			client.click("WEB",PagesHomeMenus.mnuitemReceiptBondTransfer,0,1);
			
			break;
		
		case "FOUNDCARGO":

			client.waitForElement("WEB", PagesHomeMenus.txtMenu, 0,1000 );
			client.click("WEB",PagesHomeMenus.mnuitemImport,0,1);
						
			client.waitForElement("WEB",PagesHomeMenus.mnuitemFoundCargo , 0,2000 );
			client.click("WEB",PagesHomeMenus.mnuitemFoundCargo,0,1);
			
			break;
			
		case "ACCEPTANCE":

			client.waitForElement("WEB", PagesHomeMenus.txtMenu, 0,1000 );
			
			client.click("WEB",PagesHomeMenus.mnuitemExport,0,1);
			loginFirstInstance = System.currentTimeMillis();
			
						
			client.waitForElement("WEB",PagesHomeMenus.mnuitemAcceptance , 0,2000 );
			client.verifyElementFound("WEB",PagesHomeMenus.mnuitemAcceptance , 0);
			loginlastInstance = System.currentTimeMillis();
			 totalTime = loginlastInstance - loginFirstInstance;
			client.report("Time taken in Appearing Acceptance Menu post click event on Export Menu  : " +totalTime +" MilliSeconds or "+ totalTime/1000+ " in Seconds", true);
			
			client.click("WEB",PagesHomeMenus.mnuitemAcceptance,0,1);
			loginFirstInstance = System.currentTimeMillis();
			
			break;
			
		case "ADDITIONALSERVICES":

             Thread.sleep(2000);
			
			client.click("WEB",PagesHomeMenus.mnuitemWarehouse,0,1);						
			//client.waitForElement("WEB",PagesHomeMenus.mnuitemDamage , 0,2000 );
			
			client.waitForElement("WEB",PagesHomeMenus.mnuitemAdditionalservices , 0,2000 );
			client.click("WEB",PagesHomeMenus.mnuitemAdditionalservices,0,1);
			
			break;
		case "AIRLINETRANSFER":

			client.waitForElement("WEB", PagesHomeMenus.txtMenu, 0,1000 );
			client.click("WEB",PagesHomeMenus.mnuitemImport,0,1);
						
			client.waitForElement("WEB",PagesHomeMenus.mnuitemDeliveriesTransfers , 0,2000 );
			client.click("WEB",PagesHomeMenus.mnuitemDeliveriesTransfers,0,1);
						
			client.waitForElement("WEB",PagesHomeMenus.mnuitemAirlineTransfer , 0,2000);
			client.click("WEB",PagesHomeMenus.mnuitemAirlineTransfer,0,1);
			
			break;
			
		case "CHECKLIST":
			
			client.waitForElement("WEB", PagesHomeMenus.txtMenu, 0,1000 );
			client.click("WEB",PagesHomeMenus.mnuitemExport,0,1);
			
			client.waitForElement("WEB", PagesHomeMenus.checklist, 0, 9000);
			client.click("WEB", PagesHomeMenus.checklist, 0, 1);
			
			break;
          case "BUILDUP":
			
			client.waitForElement("WEB", PagesHomeMenus.txtMenu, 0,1000 );
			client.click("WEB",PagesHomeMenus.mnuitemExport,0,1);
			
			client.waitForElement("WEB", PagesHomeMenus.BUILDUP, 0, 9000);
			client.click("WEB", PagesHomeMenus.BUILDUP, 0, 1);
			
			break;
			
			
          case "RELOCATION":
  			
  			client.waitForElement("WEB", PagesHomeMenus.mnuitemWarehouse, 0,1000 );
  			client.click("WEB",PagesHomeMenus.mnuitemWarehouse,0,1);
  			
  			client.waitForElement("WEB", PagesHomeMenus.MenuRelocation, 0, 9000);
  			client.click("WEB", PagesHomeMenus.MenuRelocation, 0, 1);
  			
  			break;
  			
          case "INVENTORYCHECK":
    			
    			client.waitForElement("WEB", PagesHomeMenus.mnuitemWarehouse, 0,1000 );
    			client.click("WEB",PagesHomeMenus.mnuitemWarehouse,0,1);
    			
    			client.waitForElement("WEB", PagesHomeMenus.MenuInventoryCheck, 0, 9000);
    			client.click("WEB", PagesHomeMenus.MenuInventoryCheck, 0, 1);
    			client.waitForElement("WEB",PagesStatusAndHistory.LblInventoryCheck, 0, 4000);
    			
    			break;
    			
    			
    			
          case "LOCATIONHISTORY":
  			
  			client.waitForElement("WEB", PagesHomeMenus.mnuitemWarehouse, 0,1000 );
  			client.click("WEB",PagesHomeMenus.mnuitemWarehouse,0,1);
  			
  			client.waitForElement("WEB", PagesHomeMenus.MenuLocationHistory, 0, 9000);
  			client.click("WEB", PagesHomeMenus.MenuLocationHistory, 0, 1);
  			client.waitForElement("WEB",PagesHomeMenus.MenuLocationHistory, 0, 4000);
  			
  			break;
          case "CHECKIN":
    			
        	client.waitForElement("WEB", PagesHomeMenus.txtMenu, 0,1000 );
  			client.click("WEB",PagesHomeMenus.mnuitemImport,0,1);
  						
  			client.waitForElement("WEB",PagesHomeMenus.checkinMenu , 0,2000 );
  			client.click("WEB",PagesHomeMenus.checkinMenu,0,1);
    			
    			break;
  			
  			
  		
			
			
		}
		
	}

}