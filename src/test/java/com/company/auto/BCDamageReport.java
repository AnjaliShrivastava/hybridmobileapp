package com.company.auto;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.junit.Assert;

import com.company.pages.PagesAcceptance;
import com.company.pages.PagesDamageReport;
import com.company.pages.PagesHomeMenus;





//public class BCDamageReport extends TestBase {
public class BCDamageReport extends TestBase {
	
	

public void SearchDamageReport(Map<String, String> data) {
	

		/*client.assertJUnitAndSeeTestReport(client.waitForElement("WEB", PagesDamageReport.lblDamageReport, 0, 2000),"Label" +PagesDamageReport.lblDamageReport + "displayed successfully","label missing"+PagesDamageReport.lblDamageReport);
		client.assertJUnitAndSeeTestReport(client.waitForElement("WEB", PagesDamageReport.lbltitleDamageReport, 0, 2000),"Label" +PagesDamageReport.lbltitleDamageReport + "displayed successfully","label missing"+PagesDamageReport.lbltitleDamageReport);
		client.assertJUnitAndSeeTestReport(client.waitForElement("WEB", PagesHomeMenus.lblorScan, 0, 2000),"Label" +PagesHomeMenus.lblorScan + "displayed successfully","label missing"+PagesHomeMenus.lblorScan);
		client.assertJUnitAndSeeTestReport(client.waitForElement("WEB", PagesHomeMenus.lblEnterNumber, 0, 2000),"Label" +PagesHomeMenus.lblEnterNumber + "displayed successfully","label missing"+PagesHomeMenus.lblEnterNumber);
		client.assertJUnitAndSeeTestReport(client.isElementFound("WEB", PagesHomeMenus.btnSearchDisabledinitial), "Search button disabled as expected", "Search button enabled -Expected to be disabled");

*/		
		if (data.get("HWB").isEmpty()) {
			
			ClickSendText(PagesHomeMenus.txtPrefixNumber,data.get("AWBPrefix"));
			ClickSendText(PagesHomeMenus.txtSerialNumber,data.get("AWBSerial"));
			if(data.get("AutoPopulate").equalsIgnoreCase("YES")){
				client.waitForElement("WEB", "//*[contains(text(),'"+data.get("AWBSerial")+"') and @class='highlight']", 0, 12000);
				client.click("WEB", "//*[contains(text(),'"+data.get("AWBSerial")+"') and @class='highlight']", 0, 1);			
			}
			//*[contains(text(),' 50005892') and @class='auto-list-text']
		}else {
			client.click("WEB",PagesHomeMenus.btnHWB , 0, 1);
			ClickSendText(PagesHomeMenus.txtSerialNumber,data.get("HWB"));
			if(data.get("AutoPopulate").equalsIgnoreCase("YES")){
				client.waitForElement("WEB", "xpath=//*[contains(text(),'"+ data.get("HWB")+"')]", 0, 12000);
				client.click("WEB", "xpath=//*[contains(text(),'"+ data.get("HWB")+"')]", 0, 1);
				
			}
		}

		if(data.get("AutoPopulate").equalsIgnoreCase("NO")) {
			client.click("WEB",PagesHomeMenus.btnSearch, 0, 1);	
		}

		if(client.waitForElement("WEB", PagesHomeMenus.lblResults, 0, 4000)) {
			client.click("WEB", "xpath=//*[contains(text(),'"+ data.get("AWBPrefix")+" "+data.get("AWBSerial") +"')]", 0, 1);			
		}

		if (data.get("TestId").contains("_N")){
			//client.assertJUnitAndSeeTestReport(client.isElementFound("WEB", "xpath=//*[@text='"+data.get("ExpectedErrorMsg")+"']"), data.get("ExpectedErrorMsg")+" error message displayed as expected", data.get("ExpectedErrorMsg")+" error message not displayed as expected");
		}else {
			//client.assertJUnitAndSeeTestReport(client.waitForElement("WEB",PagesDamageReport.btnCreateReport , 0, 2000), "Damage Report Search successful", "Damage Report Search failed");
		}
	}

	public void ShippingAddress(Map<String, String> data) throws Exception {
//		client.waitForElement("WEB", PagesDamageReport.btnCreateReport, 0, 2000);
//		client.click("WEB", PagesDamageReport.btnCreateReport,0,1);
		ShipmentDetails(data);
		PackingDetails(data);
		DamageDetails(data);
		DamageDetection(data);
		Recuperation(data);
		Remarks(data);		
	}

	public void AddProof(Map<String, String> data) {
		int badgecount = 0;
		int initialbadgecount =0;
		client.waitForElement("WEB", PagesDamageReport.lblDamageReportLanding, 0, 3000);
		if(data.get("Orientation").equalsIgnoreCase("Landscape")) {
			initialbadgecount = client.getElementCount("WEB",PagesDamageReport.btnBadge); 
		}
		client.click("WEB", PagesDamageReport.btnAddProof,0,1);		

	}

	public void ShipmentDetails(Map<String, String> data) {
		int badgecount = 0;
		int initialbadgecount =0;
		//client.waitForElement("WEB", PagesDamageReport.lblDamageReportLanding, 0, 3000);
		if(data.get("Orientation").equalsIgnoreCase("Landscape")) {
			//initialbadgecount = client.getElementCount("WEB",PagesDamageReport.btnBadge); 
		}
		if(client.isElementFound("WEB", PagesDamageReport.NextButton)) {
			client.click("WEB", PagesDamageReport.NextButton, 0, 1);
		}else {
		client.waitForElement("WEB", PagesDamageReport.btnShipmentDetails, 0, 3000);
		client.click("WEB", PagesDamageReport.btnShipmentDetails,0,1);
		}
		ClickSendText(PagesDamageReport.txtflight, data.get("Flight"));
		ClickSendText(PagesDamageReport.txtDamagedPieces,data.get("NoOfPieces"));
		ClickSendText(PagesDamageReport.txtWeight,data.get("Weight"));
		//client.assertJUnitAndSeeTestReport(client.waitForElement("WEB", PagesDamageReport.lblULDIdentifier, 0, 2000), "ULD Identifier label exists", "ULD Identifier label does not exist");
		ClickSendText(PagesDamageReport.txtULD,data.get("ULD"));
		if(data.get("Orientation").equalsIgnoreCase("Portrait")) {
			client.click("WEB", PagesDamageReport.btnNext,0,1);
			//client.assertJUnitAndSeeTestReport(client.waitForElement("WEB", PagesDamageReport.lblPackingDetails, 0, 2000), "Shipment Details - Updated Successfully", "Shipment Details - Not Updated");
		}else {
			badgecount = client.getElementCount("WEB",PagesDamageReport.btnBadge);
			//client.assertJUnitAndSeeTestReport((badgecount ==initialbadgecount+1),"Shipment Details Updated successfully and badge appears","Shipment Details -Not Updated successfully and badge does not appear");
		}

	}

	@SuppressWarnings("deprecation")
	public void PackingDetails(Map<String, String> data) {
		
		int initialbadgecount =0;
		int badgecount =0;
		if(data.get("Orientation").equalsIgnoreCase("Landscape")) {
			client.waitForElement("WEB", PagesDamageReport.lblPackingDetails, 0, 2000);
			client.click("WEB", PagesDamageReport.lblPackingDetails, 0, 1);
			//initialbadgecount = client.getElementCount("WEB",PagesDamageReport.btnBadge); 
		}
		System.out.println(data.get("PackingMaterial"));
		client.click("WEB","xpath=//*[contains(text(),' "+data.get("PackingMaterial")+"')]" ,0,1);
		//*[contains(text(),' Metal')]
		//Switch Case
		client.swipeWhileNotFound("Down", 900, "WEB", "xpath=//*[contains(text(),' "+data.get("PackingType")+"') and @visible='true']", 1000, 1, false);
		client.click("WEB","xpath=//*[contains(text(),' "+data.get("PackingType")+"')]" ,0,1);
		client.swipeWhileNotFound("Down", 900, "WEB", "xpath=//*[contains(text(),' "+data.get("InnerPacking")+"') and @visible='true']", 1000, 1, false);
		client.click("WEB","xpath=//*[contains(text(),' "+data.get("InnerPacking")+"')]" ,0,1);
		if(data.get("Orientation").equalsIgnoreCase("Landscape")) {
			badgecount = client.getElementCount("WEB",PagesDamageReport.btnBadge);
			//client.assertJUnitAndSeeTestReport((badgecount ==initialbadgecount+1),"Packing Details Updated successfully and badge appears","Packing Details -Not Updated successfully and badge does not appear");
		}else {
			client.click("WEB", PagesDamageReport.btnNext,0,1);
			//client.assertJUnitAndSeeTestReport(client.waitForElement("WEB", PagesDamageReport.lblDamageDetails, 0, 2000), "Packing Details - Updated Successfully", "Packing Details - Not Updated");
		}
	}

	@SuppressWarnings("deprecation")
	public void DamageDetails(Map<String, String> data) {
		
		int initialbadgecount =0;
		int badgecount =0;
		if(data.get("Orientation").equalsIgnoreCase("Landscape")) {
			initialbadgecount = client.getElementCount("WEB",PagesDamageReport.btnBadge);
			client.waitForElement("WEB", PagesDamageReport.lblDamageDetails, 0, 2000);
			client.click("WEB", PagesDamageReport.lblDamageDetails, 0, 1);
		}
		client.click("WEB","xpath=//*[contains(text(),' "+data.get("DamageToPacking")+"')]" ,0,1);
		client.swipe("Down", 500);
		client.click("WEB","xpath=//*[contains(text(),' "+data.get("ContentsCondition")+"')]" ,0,1);
		if(data.get("Orientation").equalsIgnoreCase("Landscape")) {
			badgecount = client.getElementCount("WEB",PagesDamageReport.btnBadge);
			//client.assertJUnitAndSeeTestReport((badgecount ==initialbadgecount+1),"Damage Details Updated successfully and badge appears","Damage Details -Not Updated successfully and badge does not appear");
		}else {

			client.click("WEB", PagesDamageReport.btnNext,0,1);
			//client.assertJUnitAndSeeTestReport(client.waitForElement("WEB", PagesDamageReport.lblDamageDetection, 0, 2000), "Damage Details - Updated Successfully", "Damage Details - Not Updated properly");
		}
	}

	public void DamageDetection(Map<String, String> data) {
		
		int initialbadgecount =0;
		int badgecount =0;
		if(data.get("Orientation").equalsIgnoreCase("Landscape")) {
			initialbadgecount = client.getElementCount("WEB",PagesDamageReport.btnBadge);
			client.waitForElement("WEB", PagesDamageReport.lblDamageDetection, 0, 2000);
			client.click("WEB", PagesDamageReport.lblDamageDetection, 0, 1);
		}
	
		client.click("WEB","//*[contains(text(),' "+data.get("Discovered")+"')]" ,0,1);
		client.click("WEB","xpath=//*[contains(text(),' "+data.get("DueTo")+"')]" ,0,1);
		if(data.get("Orientation").equalsIgnoreCase("Landscape")) {
			badgecount = client.getElementCount("WEB",PagesDamageReport.btnBadge);
			//client.assertJUnitAndSeeTestReport((badgecount ==initialbadgecount+1),"Damage Detection Details Updated successfully and badge appears","Damage Detection Details -Not Updated successfully and badge does not appear");
		}else {		
			client.click("WEB", PagesDamageReport.btnNext,0,1);
			//client.assertJUnitAndSeeTestReport(client.waitForElement("WEB", PagesDamageReport.lblRecuperation, 0, 2000), "Damage Detection Details - Updated Successfully", "Damage Detection Details - Not Updated");
		}
	}

	public void Recuperation(Map<String, String> data) {
		
		int initialbadgecount =0;
		int badgecount =0;
		if(data.get("Orientation").equalsIgnoreCase("Landscape")) {
			client.waitForElement("WEB", PagesDamageReport.lblRecuperation, 0, 2000);
			client.click("WEB", PagesDamageReport.lblRecuperation, 0, 1);
			initialbadgecount = client.getElementCount("WEB",PagesDamageReport.btnBadge); 
		}
		client.click("WEB","xpath=//*[contains(text(),'"+data.get("Recuperation")+"')]" ,0,1);
		if(data.get("Orientation").equalsIgnoreCase("Landscape")) {
			badgecount = client.getElementCount("WEB",PagesDamageReport.btnBadge);
			//client.assertJUnitAndSeeTestReport((badgecount ==initialbadgecount+1),"Recuperation Details Updated successfully and badge appears","Recuperation Details -Not Updated successfully and badge does not appear");
		}else {
			client.click("WEB", PagesDamageReport.btnNext,0,1);
			//client.assertJUnitAndSeeTestReport(client.waitForElement("WEB", PagesDamageReport.lblRemarks, 0, 3000), "Recuperation Details - Updated Successfully", "Recuperation Details - Not Updated");
		}
	}

	public void Remarks(Map<String, String> data) {
		
		int initialbadgecount =0;
		int badgecount =0;
		if(data.get("Orientation").equalsIgnoreCase("Landscape")) {
			client.waitForElement("WEB", PagesDamageReport.lblRemarksandBoxes, 0, 2000);
			client.click("WEB", PagesDamageReport.lblRemarksandBoxes, 0, 1);
			initialbadgecount = client.getElementCount("WEB",PagesDamageReport.btnBadge); 
		}
		client.click("WEB",PagesDamageReport.txtRemarks ,0,1);
		client.sendText(data.get("Remarks"));
		client.click("WEB",PagesDamageReport.txtBoxes ,0,1);
		client.sendText(data.get("NumberOfBoxes"));
		client.closeKeyboard();
		if(data.get("Orientation").equalsIgnoreCase("Landscape")) {
			badgecount = client.getElementCount("WEB",PagesDamageReport.btnBadge);
			//client.assertJUnitAndSeeTestReport((badgecount ==initialbadgecount+1),"Remarks and Boxes Details Updated successfully and badge appears","Remarks and Boxes Details -Not Updated successfully and badge does not appear");
		}
		client.click("WEB", PagesDamageReport.btnSave,0,1);
		//client.assertJUnitAndSeeTestReport(client.waitForElement("WEB", PagesHomeMenus.lblSuccessfullysaved, 0, 2000), "Remarks and Boxes Updated and Damage Report created Successfully", "Remarks and Boxes Details - Not Updated");
		//client.assertJUnitAndSeeTestReport(client.waitForElement("WEB", PagesDamageReport.btnPrint, 0, 3000), "Print Button Enabled Successfully", "Print Button - Not Visible");
		//client.assertJUnitAndSeeTestReport(client.waitForElement("WEB", PagesDamageReport.btnEmail, 0, 2000), "Email Button Enabled Successfully", "Email Button - Not Visible");		
	}

	public void createNewReport(Map<String, String> data) {

		client.waitForElement("WEB", PagesDamageReport.btnCreateReport, 0, 2000);
		client.click("WEB", PagesDamageReport.btnCreateReport,0,1);

	}


	public void CaptureImage (Map<String, String> data) throws InterruptedException {
		client.waitForElement("WEB", PagesDamageReport.btnAddProof, 0, 8000);
		//client.assertJUnitAndSeeTestReport(client.waitForElement("WEB", PagesDamageReport.btnAddProof, 0, 8000), "ADD Proof label exist", "ADD proof label does not exist");		
		if(data.get("Orientation").equalsIgnoreCase("Portrait")){

			client.click("WEB", PagesDamageReport.btnAddProof, 0, 1);
			client.waitForElement("WEB", PagesDamageReport.btnAddProof, 0, 8000);

		}

		client.click("WEB",PagesDamageReport.btnUseCamera, 0, 1);
		client.waitForElement("WEB", PagesDamageReport.btnCaptureImage, 0, 8000);
		client.click("WEB", PagesDamageReport.btnCaptureImage, 0, 1);

		switch(data.get("DeviceName")) {
		case "Nexus":
			client.waitForElement("NATIVE", "xpath=//*[@id='shutter_button']", 0, 8000);
			client.click("NATIVE", "xpath=//*[@id='shutter_button']", 0, 1);
			Thread.sleep(4000);
			client.click("NATIVE", "xpath=//*[@id='shutter_button']", 0, 1);
			break;

		case "Samsung":
			client.waitForElement("NATIVE", "xpath=//*[@id='MENUID_SHUTTER']", 0, 8000);
			client.click("NATIVE", "xpath=//*[@id='MENUID_SHUTTER']", 0, 1);
			Thread.sleep(2000);
			client.click("NATIVE", "xpath=//*[@id='okay']", 0, 1);
			break;
		case "Ipad":
			if(client.isElementFound("NATIVE", "xpath=//*[@id='OK']")) {
				client.click("NATIVE", "xpath=//*[@id='OK']", 0, 1);
			}
			client.waitForElement("NATIVE", "xpath=//*[@class='CAMShutterButtonRingView']", 0, 8000);
			Thread.sleep(2000);	
			client.click("NATIVE", "xpath=//*[@class='CAMShutterButtonRingView']", 0, 2);
			Thread.sleep(2000);			
			client.click("NATIVE", "xpath=//*[@text='Use Photo']", 0, 1);
			if(client.isElementFound("NATIVE", "xpath=//*[@id='OK']")) {
				client.click("NATIVE", "xpath=//*[@id='OK']", 0, 1);
			}
			break;
			
		case "Honeywell":
			client.waitForElement("NATIVE", "xpath=//*[@id='shutter_button']", 0, 8000);
			Thread.sleep(8000);
			client.click("NATIVE", "xpath=//*[@id='shutter_button']", 0, 3);
			Thread.sleep(10000);
			client.waitForElement("NATIVE", "xpath=//*[@id='done_button']", 0, 12000);
			client.click("NATIVE", "xpath=//*[@id='done_button']", 0, 1);
			break;

		}
	}

	public void CaptureVideo (Map<String, String> data) throws InterruptedException {

		//client.assertJUnitAndSeeTestReport(client.waitForElement("WEB", PagesDamageReport.btnAddProof, 0, 8000), "ADD Proof label exist", "ADD proof label does not exist");		
		if(data.get("Orientation").equalsIgnoreCase("Portrait")){

			client.click("WEB", PagesDamageReport.btnAddProof, 0, 1);
			client.waitForElement("WEB", PagesDamageReport.btnAddProof, 0, 8000);

		}

		client.click("WEB",PagesDamageReport.btnUseCamera, 0, 1);
		client.waitForElement("WEB", PagesDamageReport.btnRecordVideo, 0, 8000);
		client.click("WEB", PagesDamageReport.btnRecordVideo, 0, 1);

		switch(data.get("DeviceName")) {
		case "Nexus":
			client.waitForElement("NATIVE", "xpath=//*[@class='CUShutterButton']", 0, 8000);
			client.click("NATIVE", "xpath=//*[@class='CUShutterButton']", 0, 1);
			
			Thread.sleep(2000);	
			client.click("NATIVE", "xpath=//*[@class='CUShutterButton']", 0, 1);
			client.waitForElement("NATIVE", "xpath=//*[@text='Use Video']", 0, 8000);
			client.click("NATIVE", "xpath=//*[@text='Use Video']", 0, 1);
			break;

		case "Samsung":
			client.waitForElement("NATIVE", "xpath=//*[@id='MENUID_RECORDING']", 0, 8000);
			client.click("NATIVE", "xpath=//*[@id='MENUID_RECORDING']", 0, 1);
			Thread.sleep(3000);	
			client.click("NATIVE", "xpath=//*[@text='Stop']", 0, 1);
			client.waitForElement("NATIVE", "xpath=//*[@id='okay']", 0, 8000);
			client.click("NATIVE", "xpath=//*[@id='okay']", 0, 1);
			Thread.sleep(8000);
			break;
		case "Ipad":
			client.waitForElement("NATIVE", "xpath=//*[@class='CUShutterButton']", 0, 8000);
			Thread.sleep(5000);	
			client.click("NATIVE", "xpath=//*[@class='CUShutterButton']", 0, 1);
			Thread.sleep(2000);	
			client.click("NATIVE", "xpath=//*[@class='CUShutterButton']", 0, 1);
			client.waitForElement("NATIVE", "xpath=//*[@text='Use Video']", 0, 8000);
			client.click("NATIVE", "xpath=//*[@text='Use Video']", 0, 1);
			Thread.sleep(5000);
			break;

		}
	}


	public void useExitingGallery (Map<String, String> data) throws InterruptedException {

		client.waitForElement("WEB", PagesDamageReport.btnAddProof, 0, 8000);
		if(data.get("Orientation").equalsIgnoreCase("Portrait")){

			client.click("WEB", PagesDamageReport.btnAddProof, 0, 1);
			client.waitForElement("WEB", PagesDamageReport.btnAddProof, 0, 8000);

		}

		client.click("WEB",PagesDamageReport.btnUseGallery ,0, 1);
		client.waitForElement("WEB", PagesDamageReport.btnPickImage, 0, 8000);
		client.click("WEB", PagesDamageReport.btnPickImage, 0, 1);

		switch(data.get("DeviceName")) {
		case "Nexus":
			client.waitForElement("NATIVE", "xpath=//*[@text='Favorites']", 0, 8000);
			client.click("NATIVE", "xpath=//*[@text='Favorites']", 0, 1);
			client.waitForElement("NATIVE", "xpath=//*[@class='PUPhotoView']", 0, 8000);		
			client.click("NATIVE", "xpath=//*[@class='PUPhotoView']", 0, 1);
			break;

		case "Samsung":
			client.waitForElement("NATIVE", "xpath=//*[@id='icon_mime']", 0, 8000);
			client.click("NATIVE", "xpath=//*[@id='icon_mime']", 0, 1);
			
			break;
			
		case "Ipad":

			client.waitForElement("NATIVE", "xpath=//*[@text='Favorites']", 0, 8000);
			client.click("NATIVE", "xpath=//*[@text='Favorites']", 0, 1);
			client.waitForElement("NATIVE", "xpath=//*[@class='PUPhotoView']", 0, 8000);		
			client.click("NATIVE", "xpath=//*[@class='PUPhotoView']", 0, 1);
			break;

		}











		//		Thread.sleep(4000);
		//		//client.click("WEB", "xpath=//*[@text='Capture Image']", 0, 1);
		//
		//		client.click("WEB", "xpath=//*[@text='Record Video']", 0, 1);
		//		client.click("NATIVE", "xpath=//*[@id='shutter_button']", 0, 1);
		//		//
		//		client.click("NATIVE", "xpath=//*[@id='shutter_button']", 0, 1);
		//		Thread.sleep(4000);
		//
		//		//		client.click("WEB", "xpath=//*[contains(text(),'Use Gallery')]", 0, 1);
		//		//		client.click("WEB", "xpath=//*[@text='Pick Image']", 0, 1);
		//		//		client.click("NATIVE", "xpath=//*[@id='icon_thumb']", 0, 1);



	}


	public void cameraraIOS(Map<String, String> data) throws InterruptedException {

		client.waitForElement("WEB", PagesDamageReport.btnCreateReport, 0, 2000);
		client.click("WEB", PagesDamageReport.btnCreateReport,0,1);

		client.click("WEB", "xpath=//*[@src='assets/images/usecamera.svg']", 0, 1);
		Thread.sleep(4000);
		client.click("WEB", "xpath=//*[@text='Capture Image']", 0, 1);

		//client.click("WEB", "xpath=//*[@text='Record Video']", 0, 1);

		client.click("NATIVE", "xpath=//*[@class='CUShutterButton']", 0, 1);
		Thread.sleep(2000);



		client.click("NATIVE", "xpath=//*[@text='Use Photo']", 0, 1);



		//client.click("NATIVE", "xpath=//*[@id='shutter_button']", 0, 1);
		//Thread.sleep(4000);
		//client.click("NATIVE", "xpath=//*[@id='shutter_button']", 0, 1);
		//client.click("NATIVE", "xpath=//*[@id='shutter_button']", 0, 1);
		Thread.sleep(4000);


		//		client.click("WEB", "xpath=//*[contains(text(),'Use Gallery')]", 0, 1);
		//		client.click("WEB", "xpath=//*[@text='Pick Image']", 0, 1);
		//		client.click("NATIVE", "xpath=//*[@id='icon_thumb']", 0, 1);



	}

	public void cameraraSamsung(Map<String, String> data) throws InterruptedException {

		client.waitForElement("WEB", PagesDamageReport.btnCreateReport, 0, 2000);
		client.click("WEB", PagesDamageReport.btnCreateReport,0,1);

		client.click("WEB", "xpath=//*[@text=' Add Proof ']", 0, 1);
		Thread.sleep(4000);
		client.click("WEB", "xpath=//*[contains(text(),'Use Camera')]", 0, 1);
		Thread.sleep(4000);
		client.click("WEB", "xpath=//*[@text='Capture Image']", 0, 1);

		//client.click("WEB", "xpath=//*[@text='Record Video']", 0, 1);

		client.click("NATIVE", "xpath=//*[@id='MENUID_SHUTTER']", 0, 1);
		Thread.sleep(2000);

		client.click("NATIVE", "xpath=//*[@id='okay']", 0, 1);



		//client.click("NATIVE", "xpath=//*[@id='shutter_button']", 0, 1);
		//Thread.sleep(4000);
		//client.click("NATIVE", "xpath=//*[@id='shutter_button']", 0, 1);
		//client.click("NATIVE", "xpath=//*[@id='shutter_button']", 0, 1);
		Thread.sleep(4000);


		//		client.click("WEB", "xpath=//*[contains(text(),'Use Gallery')]", 0, 1);
		//		client.click("WEB", "xpath=//*[@text='Pick Image']", 0, 1);
		//		client.click("NATIVE", "xpath=//*[@id='icon_thumb']", 0, 1);
	}
	public void Save(){

		client.waitForElement("WEB", PagesDamageReport.MediaSaveButton, 0, 2000);
		client.click("WEB", PagesDamageReport.MediaSaveButton, 0, 1);
		client.waitForElement("WEB", PagesAcceptance.successfullySavedMessage, 0, 15000);
		//*[contains(text(),'01Apr19')]

	}
	public void verifyReport(){
		client.waitForElement("WEB", PagesDamageReport.btnEmail, 0, 15000) ;
		client.waitForElement("WEB", "xpath=//*[@nodeName='SPAN']", 0, 3000);
		Date d = new Date();
		SimpleDateFormat sf = new SimpleDateFormat("ddMMMYY");
		client.click("WEB", "xpath=//*[@nodeName='SPAN']", 0, 1);
		if(client.isElementFound("WEB", "xpath=//*[@text='Yes']") == true){
			client.click("WEB", "xpath=//*[@text='Yes']", 0, 1);
		}
		client.waitForElement("WEB", PagesDamageReport.btnCreateReport, 0, 15000);
		client.verifyElementFound("WEB", "//*[contains(text(),'"+sf.format(d)+"')]" , 0);
		//*[contains(text(),'01Apr19')]

	}
	
	
	public void Search(Map<String, String> data) {
		switch(data.get("SearchUsing")) {
		case "AWB":
			if (data.get("AWBPrefix").isEmpty()==false) {
				client.click("WEB",PagesHomeMenus.btnAWB, 0, 1);
				client.click("WEB",PagesHomeMenus.txtPrefixNumber, 0, 1);
				client.sendText(data.get("AWBPrefix"));
				client.closeKeyboard();
				client.click("WEB",PagesHomeMenus.txtSerialNumber, 0, 1);
				client.sendText(data.get("AWBSerial"));
				client.closeKeyboard();			
				//client.sendText(data.get("AWBPrefix")+"  "+data.get("AWBSerial"));
			}	
			if (data.get("AutoPopulate").equalsIgnoreCase("YES")){
				//Autopopulate Click Event
				client.click("WEB", "xpath=//*[contains(text(),'"+ data.get("AWBPrefix")+" "+data.get("AWBSerial")+"')]", 0, 1);			
			}else {
				client.click("WEB", PagesHomeMenus.btnSearch, 0, 1);		
			}

			break;
			//Has to be altered
			//client.click("WEB",PagesAcceptance.lblQuanityPieces , 0, 1);

			//********** Create New Record **************
		case "HWB":
			if(data.get("HWB").isEmpty()==false) {
				client.click("WEB",PagesHomeMenus.btnHWB, 0, 1);
				client.click("WEB",PagesHomeMenus.txtSerialNumber, 0, 1);
				client.sendText(data.get("HWB"));
				client.closeKeyboard();			
			}
			if (data.get("AutoPopulate").equalsIgnoreCase("YES")){
				//Autopopulate Click Event
				client.click("WEB", "xpath=//*[contains(text(),'"+ data.get("HWB")+"')]", 0, 1);

			}else {
				client.click("WEB", PagesHomeMenus.btnSearch, 0, 1);		
			}
         if(!data.get("AWBPrefix").equalsIgnoreCase("no")) {
			client.click("WEB", "xpath=//*[contains(text(),'"+ data.get("AWBPrefix")+" "+data.get("AWBSerial")+"')]", 0, 1);
		}else {
			System.out.println("Did not go inside if loop ");
		}
		}
	}
	
	public void scanBarcode() throws IOException {
		client.click("WEB", "xpath=//*[contains(text(),'SCAN BARCODE')]", 0, 1);
		
		
		//client.simulateCapture("D:\\AWB.PNG");
		
		File f = new File("D:\\QRCode.PNG");
        Desktop dt = Desktop.getDesktop();
        dt.open(f);
        f.exists();
     

        System.out.println("Notepad should now open.");
		
		
		
	}
	
//	public void getImagePath() {
//		Intent intent = new Intent(Intent.ACTION_PICK);
//		intent.setType("image/*");
//
//		startActivityForResult(Intent.createChooser(intent, "Share Photo"), SELECT_PICTURE);
//
//
//		@Override
//		public void onActivityResult(int requestCode, int resultCode, Intent data) {
//		    if (resultCode == RESULT_OK) {
//		        if (requestCode == SELECT_PICTURE) {
//		            Uri selectedImageUri = data.getData();
//		            String selectedImagePath = getPath(selectedImageUri);  
//
//		            selectedImageUri = selectedImageUri .replace("com.android.gallery3d","com.google.android.gallery3d");
//
//		            if (selectedImageUri .startsWith("content://com.google.android.gallery3d")
//		                || selectedImageUri .startsWith("content://com.sec.android.gallery3d.provider") ) {
//
//		                selectedImagePath = PicasaImage(selectedImageUri);
//		            }
//		            else
//		                selectedImagePath = getPath(selectedImageUri);
//		            }
//
//		            imageView.setImageBitmap(BitmapFactory
//		                            .decodeFile(selectedImagePath));
//
//		            Log.i("SELECTED IMAGE PATH: ", selectedImagePath);
//		    }
//		}
//
//		private String PicasaImage(Uri imageUri) {
//
//		    File cacheDir;
//		    // if the device has an SD card
//		    if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED)) {
//		        cacheDir = new File(android.os.Environment.getExternalStorageDirectory(),".OCFL311");
//		    } else {
//		        // it does not have an SD card
//		        cacheDir = getCacheDir();
//		    }
//
//		    if(!cacheDir.exists()) cacheDir.mkdirs();
//
//		    File f = new File(cacheDir, "tempPicasa");
//
//		    try {
//
//		        InputStream is = null;
//		        if (imageUri.toString().startsWith("content://com.google.android.gallery3d")
//		                || imageUri.toString().startsWith("content://com.sec.android.gallery3d.provider")) {
//
//		            is = getContentResolver().openInputStream(imageUri);
//		        } else {
//		            is = new URL(imageUri.toString()).openStream();
//		        }
//
//		        OutputStream os = new FileOutputStream(f);
//
//		        //Utils.InputToOutputStream(is, os);
//
//		        byte[] buffer = new byte[1024];
//		        int len;
//		        while ((len = is.read(buffer)) != -1) {
//		            os.write(buffer, 0, len);
//		        }
//
//		        return f.getAbsolutePath();
//		    } catch (Exception ex) {
//		        Log.i(this.getClass().getName(), "Exception: " + ex.getMessage());
//		        // something went wrong
//		        ex.printStackTrace();
//		        return null;
//		    }
//		}
//
//		public String getPath(Uri uri) {
//		    // just some safety built in
//		    if( uri == null ) {
//		        // TODO perform some logging or show user feedback
//		        return null;
//		    }
//		    // try to retrieve the image from the media store first
//		    // this will only work for images selected from gallery
//		    String[] projection = { MediaStore.Images.Media.DATA };
//		    Cursor cursor = managedQuery(uri, projection, null, null, null);
//		    if( cursor != null ){
//		        int column_index = cursor
//		                .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
//		        cursor.moveToFirst();
//		        return cursor.getString(column_index);
//		    }
//
//		    // for other file managers
//		    return uri.getPath();
//		}
//		shareimprove this answer
//		answered Sep 30 '15 at 9:18
//
//		user5155835
//		306218
//		add a comment
//		up vote
//		0
//		down vote
//		Try this to download a bitmap:
//
//		URL url = new URL(sUrl);
//		HttpURLConnection connection  = (HttpURLCo
//	}
//	
//	
	

}





