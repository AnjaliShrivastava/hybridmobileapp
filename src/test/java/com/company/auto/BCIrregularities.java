package com.company.auto;

import java.util.Map;
import com.company.pages.PagesIrregularities;
import com.experitest.client.Client;

import junit.framework.Assert;

import com.company.pages.PagesAcceptance;
import com.company.pages.PagesHomeMenus;

public class BCIrregularities extends TestBase {

	public void SearchIrrReport(Map<String, String> data) {
		
		//client.assertJUnitAndSeeTestReport(client.waitForElement("WEB", PagesIrregularities.lblIrrReport, 0, 2000),"Label" +PagesIrregularities.lblIrrReport + "displayed successfully","label missing"+PagesIrregularities.lblIrrReport);
		//client.assertJUnitAndSeeTestReport(client.waitForElement("WEB", PagesIrregularities.lblIrregularities, 0, 2000),"Label" +PagesIrregularities.lblIrregularities + "displayed successfully","label missing"+PagesIrregularities.lblIrregularities);
		//client.assertJUnitAndSeeTestReport(client.isElementFound("WEB", PagesHomeMenus.btnSearchDisabledinitial), "Search button disabled as expected", "Search button enabled -Expected to be disabled");
		
		if (data.get("HWB").isEmpty()) {
			client.click("WEB",PagesHomeMenus.btnAWB, 0, 1);
			ClickSendText(PagesHomeMenus.txtPrefixNumber,data.get("AWBPrefix"));
			ClickSendText(PagesHomeMenus.txtSerialNumber,data.get("AWBSerial"));
				
			if (data.get("AutoPopulate").equalsIgnoreCase("YES")){
				
				client.waitForElement("WEB", "//*[contains(text(),'"+data.get("AWBSerial")+"') and @class='highlight']", 0, 12000);
				client.click("WEB", "//*[contains(text(),'"+data.get("AWBSerial")+"') and @class='highlight']", 0, 1);
				}	
		}else {
			
			if(data.get("HWB").isEmpty()==false) {
				client.click("WEB",PagesHomeMenus.btnHWB, 0, 1);
				client.click("WEB",PagesHomeMenus.txtSerialNumber, 0, 1);
				client.sendText(data.get("HWB"));
				client.closeKeyboard();			
			}
			if (data.get("AutoPopulate").equalsIgnoreCase("YES")){
				//Autopopulate Click Event
				client.waitForElement("WEB", "xpath=//*[contains(text(),'"+ data.get("HWB")+"')]", 0, 8000);
				client.click("WEB", "xpath=//*[contains(text(),'"+ data.get("HWB")+"')]", 0, 1);

			}else {
				client.click("WEB", PagesHomeMenus.btnSearch, 0, 1);		
			}
         if(!data.get("AWBPrefix").equalsIgnoreCase("no")) {
			client.click("WEB", "xpath=//*[contains(text(),'"+ data.get("AWBPrefix")+" "+data.get("AWBSerial")+"')]", 0, 1);
		}else {
			System.out.println("Did not go inside if loop ");
		}
		}
			
			/*client.click("WEB",PagesHomeMenus.btnHWB , 0, 1);
			ClickSendText(PagesHomeMenus.txtSerialNumber,data.get("HWB"));
			if(data.get("AutoPopulate").equalsIgnoreCase("YES")){
				client.click("WEB", "xpath=//*[contains(text(),'"+ data.get("HWB")+"')]", 0, 1);			
			}
		}
		
		if(data.get("AutoPopulate").equalsIgnoreCase("NO")) {
			client.click("WEB",PagesHomeMenus.btnSearch, 0, 1);	
		}
		
		if(client.waitForElement("WEB", PagesHomeMenus.lblResults, 0, 2000)) {
			client.click("WEB", "xpath=//*[contains(text(),'"+ data.get("AWBPrefix")+" "+data.get("AWBSerial") +"')]", 0, 1);			
		}*/
		
		if (data.get("TestId").contains("_N")){
			//client.assertJUnitAndSeeTestReport(client.isElementFound("WEB", "xpath=//*[@text='"+data.get("ExpectedErrorMsg")+"']"), data.get("ExpectedErrorMsg")+" error message displayed as expected", data.get("ExpectedErrorMsg")+" error message not displayed as expected");
		}else {
		//client.assertJUnitAndSeeTestReport(client.waitForElement("WEB",PagesIrregularities.btnCreateReport , 0, 2000), "Irregularity Search successful", "Irregularity Search failed");
		}
	}
	
	public void CreateIrrReport(Map<String, String> data) throws Exception {
		client.waitForElement("WEB", PagesIrregularities.btnCreateReport, 0, 8000);
		client.click("WEB", PagesIrregularities.btnCreateReport, 0, 1);
		//client.waitForElement("WEB",PagesIrregularities.lblCreateIrrReport , 0, 2000);
		if(data.get("Pieces").equalsIgnoreCase("Exceeds")) {
			
		
		client.click("WEB", PagesIrregularities.txtPieces, 0, 1);
		client.sendText("1");
		//client.assertJUnitAndSeeTestReport(client.waitForElement("WEB",PagesIrregularities.lblExceedAWBpieces , 0, 2000),"Warning message appears for Exceed AWB Pieces","Warning message missing for Exceed AWB Pieces");
		client.click("WEB", PagesIrregularities.txtPieces, 0, 1);
		client.sendText("{BKSP}");	
		client.closeKeyboard();
		}else {
			/*String strpiecescount = client.elementGetProperty("WEB", PagesIrregularities.txtPieces, 0, "value");
			int piecescount = Integer.parseInt(strpiecescount);*/
			client.click("WEB", PagesIrregularities.txtPieces, 0, 1);
		String	Ptext = client.elementGetText("WEB", PagesIrregularities.txtPieces, 0);
		System.out.println(Ptext);
			if (Ptext.equals("")) {
				client.sendText(data.get("Pieces"));
			}else {
				for (int i = 0; i < Ptext.length(); i++) {
					client.sendText("{BKSP}");
				}
				client.sendText(data.get("Pieces"));
			}
						
			/*int revisedpiecescount = piecescount-1;
			client.sendText(Integer.toString(revisedpiecescount));*/
			client.closeKeyboard();
		}
		client.click("WEB", PagesIrregularities.txtweight, 0, 1);
		String	Ptext2 =client.elementGetText("WEB", PagesIrregularities.txtweight, 0);
		if (Ptext2.equals("")) {
			client.sendText(data.get("Weight"));
		}else {
			for (int i = 0; i < Ptext2.length(); i++) {
				client.sendText("{BKSP}");
			}
			client.sendText(data.get("Weight"));
		}
		
		client.closeKeyboard();
		client.waitForElement("WEB", PagesIrregularities.lblCodeandDesc, 0, 2000);
		client.click("WEB",PagesIrregularities.btnIrrDetails , 0, 1);
		client.waitForElement("WEB", PagesIrregularities.lblIrrDetails, 0, 1000);
		client.click("WEB", PagesIrregularities.txtSearchIrrDetails, 0, 1);
		client.sendText(data.get("IrrCode"));
		client.click("WEB", "xpath=//*[contains(text(),'"+ data.get("IrrCode") +"')]", 1, 1);
		client.click("WEB", PagesIrregularities.btnSave, 0, 1);
		client.waitForElement("WEB", PagesAcceptance.successfullySavedMessage, 0, 5000);
		client.verifyElementFound("WEB", PagesAcceptance.successfullySavedMessage, 0);
		//client.assertJUnitAndSeeTestReport(client.waitForElement("WEB",PagesIrregularities.lblSuccessfullysaved , 0, 2000),"Irregularity report created successfully","Irregularity report is not saved");
		client.waitForElement("WEB", PagesIrregularities.btnCreateReport, 0, 2000);
		
	}
}