package com.company.auto;

import java.util.Map;

import com.company.pages.PagesHomeMenus;
import com.company.pages.PagesImport;
import com.experitest.client.Client;

public class BCImport extends TestBase {
	
	public void CargoArrivalCompletion(Map<String, String> data) {
		
		client.waitForElement("Web", "xpath=//*[@text='Cargo Arrival Details']", 0, 4000);
		client.click("Web", "xpath=//*[@text='Cargo Arrival Details']", 0, 1);
		
		client.waitForElement("Web", "xpath=//*[contains(text(),'Cargo Arrival Complete')]", 0, 4000);
		client.clickIn("Web", "xpath=//*[@text=' Cargo Arrival Complete']", 0, "Right", "Web", "xpath=//*[@class='toggle-icon']", 0, 0, 0, 1);
		client.click("Web", PagesImport.saveButton, 0, 1);
		client.verifyElementFound("Web", PagesImport.lblSaved, 0);
	
		}
public void FlightCheckIn(Map<String, String> data) {
		
		client.waitForElement("Web", PagesImport.lblFgtCheckinCompleteButton, 0, 4000);
		client.clickIn("Web", PagesImport.lblFgtCheckinCompleteButton, 0, "Right", "Web", "xpath=//*[@class='toggle-icon']", 0, 0, 0, 1);
	
		}

	



	
}