package com.company.auto;

import java.util.List;
import java.util.Map;
import com.company.pages.PagesDeliveriesTransfers;
import com.company.pages.PagesHomeMenus;
import com.experitest.client.Client;

public class BCDeliveriesTransfers extends TestBase {
	

	public void SearchAirlineTransferReceipt(Map<String, String> data) {
		//client.assertJUnitAndSeeTestReport(client.waitForElement("WEB", PagesDeliveriesTransfers.lblHeader,0,2000),"navigation to DeliveriesTransfer screen successfully." ,"Navigation error" );
		//client.assertJUnitAndSeeTestReport(client.isElementFound("WEB", PagesHomeMenus.lblEnterNumber),"Label Enter Number is available","Label Enter Number is not available");
		//client.assertJUnitAndSeeTestReport(client.isElementFound("WEB", PagesDeliveriesTransfers.lblEnterTransferManifest),"Label Enter Transfer Manifest Number is available","Label Enter Transfer Manifest Number is not available");
		//client.assertJUnitAndSeeTestReport(client.isElementFound("WEB", PagesDeliveriesTransfers.lblorScan), "Label or Scan is available", "Label or Scan is NOT available");
		client.click("WEB",PagesDeliveriesTransfers.txtEnterNumber, 0, 1);
		client.sendText(data.get("ReceiptNumber"));
		client.click("WEB", PagesDeliveriesTransfers.btnAirlineSearch, 0, 1);	
		
		if (data.get("TestId").contains("_N")){			
			//client.assertJUnitAndSeeTestReport(client.waitForElement("WEB", "xpath=//*[@text='"+data.get("ExpectedErrorMsg")+"']", 0, 3000), "Expected Error message shown successfully", "Expected Error message - Failed");
			if (client.waitForElement("WEB",PagesDeliveriesTransfers.btnOK,0,1)) {
				client.click("WEB",PagesDeliveriesTransfers.btnOK,0,1);
			}			
		}else {
			//client.assertJUnitAndSeeTestReport(client.waitForElement("WEB", PagesDeliveriesTransfers.lblAirlineTransfer, 0, 10000), "Airline Transfer Search successful", "Airline Transfer Search - Unsuccessful");
		}
	}
	
	public void SearchDTReceipt (Map<String, String> data) {
		//client.assertJUnitAndSeeTestReport(client.waitForElement("WEB", PagesDeliveriesTransfers.lblHeader,0,2000),"navigation to DeliveriesTransfer screen successfully." ,"Navigation error" );
		//client.assertJUnitAndSeeTestReport(client.waitForElement("WEB", PagesDeliveriesTransfers.lblProcessDeliveryTransfer,0,2000),"navigation to DeliveriesTransfer screen successfully." ,"Navigation error" );
		//client.assertJUnitAndSeeTestReport(client.waitForElement("WEB", PagesHomeMenus.lblEnterNumber,0,2000),"Label Enter Number is visble" ,"Label Enter Number is not visble" );
		//client.assertJUnitAndSeeTestReport(client.isElementFound("WEB", PagesDeliveriesTransfers.lblorScan), "Label or Scan is available", "Label or Scan is NOT available");
		//client.assertJUnitAndSeeTestReport(client.isElementFound("WEB", PagesDeliveriesTransfers.btnScanBarcode), "Scan barcode button is available", "Scan barcode button is NOT available");
		client.click("WEB",PagesDeliveriesTransfers.txtReceiptNumber , 0, 1);
		client.sendText(data.get("ReceiptNumber"));
		client.click("WEB", PagesDeliveriesTransfers.btnSearch, 0, 1);	
		if (data.get("TestId").contains("_N")){
			//client.assertJUnitAndSeeTestReport(client.waitForElement("WEB", "xpath=//*[@text='"+data.get("ExpectedErrorMsg")+"']", 0, 3000), "Expected Error message shown successfully", "Expected Error message - Failed");
			if (client.waitForElement("WEB",PagesDeliveriesTransfers.btnOK,0,1)) {
				client.click("WEB",PagesDeliveriesTransfers.btnOK,0,1);
			}			
		}
	}
	
	@SuppressWarnings("deprecation")
	public void ProcessDTReceipt(Map<String, String> data) throws Exception {
		String strflag = "";
		String awbprefix = "";
		String awbserial = "";
		
		String url = "pickup/v1/deliveries/?deliveryNumber="+(data.get("ReceiptNumber"));
		System.out.println(url);		
		
		if (client.waitForElement("WEB", PagesDeliveriesTransfers.btnBondTransfer, 0, 3000)) {			
			strflag = "double";
			if(data.get("TestId").contains("Bond")) {
				//client.assertJUnitAndSeeTestReport(client.waitForElement("WEB",PagesDeliveriesTransfers.lblBondTransfer , 0, 3000), "Search successful", "Search Failed");
				client.click("WEB",PagesDeliveriesTransfers.btnBondTransfer , 0, 1);
				//client.assertJUnitAndSeeTestReport(client.waitForElement("WEB",PagesDeliveriesTransfers.lblBondTransfer , 0, 3000), "Header Bond Transfer - Successful", "Header Bond Transfer - Failed");
			}else {
				client.click("WEB",PagesDeliveriesTransfers.btnDeliveryReceipt , 0, 1);
				//client.assertJUnitAndSeeTestReport(client.waitForElement("WEB",PagesDeliveriesTransfers.lblDeliveryReceipt , 0, 3000), "Header Delivery Receipt - Successful", "Header Delivery Receipt - Failed");
			}
			
		}else {
			strflag = "single";
		}
							
		//Screen validation
		client.waitForElement("WEB", PagesDeliveriesTransfers.btnAWBHWB,0,2000);
		
		//Selection of checkbox
		if(data.get("View").equalsIgnoreCase("Location")) {
			client.click("WEB", PagesDeliveriesTransfers.btnLocation, 0, 1);
			
			List<String> myobject = GetResponse(url,"$..location");
			System.out.println(myobject.size());
			
			//select checkbox							
			for (int i=0;i<myobject.size();i++) {
					
					client.swipeWhileNotFound("Down", 900, "WEB", "xpath=//*[@text='"+myobject.get(i)+"' and @visible='true']", 1000, 8, false);
					client.swipeWhileNotFound("Up", 900, "WEB", "xpath=//*[@text='"+myobject.get(i)+"' and @visible='true']", 1000, 8, false);
					int yvalue = Integer.parseInt(client.elementGetProperty("WEB", "xpath=//*[@text='"+myobject.get(i)+"']",0, "y"));
					if (yvalue>1100 && yvalue<1450) {
						client.swipe("DOWN", 1000);
						client.click("WEB","xpath=//*[@text='"+myobject.get(i)+"'and @visible='true']",0,1);
					}else if (yvalue<60) {	
						client.swipe("Up", 200);
						client.click("WEB","xpath=//*[@text='"+myobject.get(i)+"'and @visible='true']",0,1);
					}else {
						client.click("WEB","xpath=//*[@text='"+myobject.get(i)+"'and @visible='true']",0,1);
					}
					if (data.get("Orientation").equals("Landscape")) {
						client.click("WEB","xpath=//*[@text='"+myobject.get(i)+"'and @visible='true']",1,1);
					}
			}	
		}else {
			client.click("WEB", PagesDeliveriesTransfers.btnAWBHWB, 0, 1);
			
			//Json value formats
			List<String> myobject1 = GetResponse(url,"$..airwaybillPrefix");
			List<String> myobject2 = GetResponse(url,"$..airwaybillSerial");
			
			if (myobject2.size()==1) {
				awbprefix = myobject1.toString().replace("[","");
				awbprefix = awbprefix.replace("]","");
				awbserial = myobject2.toString().replace("[","");
				awbserial = awbserial.replace("]","");
				client.waitForElement("WEB", "xpath=//*[@text='"+"AWB "+awbprefix+"-"+awbserial+"']",0,2000);
				client.click("WEB","xpath=//*[@text='"+"AWB "+awbprefix+"-"+awbserial+"']",0,1);
			}else {
				for (int i=0;i<myobject2.size();i++) {
					if(myobject1.size()==myobject2.size()) {
						awbprefix = myobject1.get(i);
					}else {
						awbprefix = myobject1.toString().replace("[","");
						awbprefix = awbprefix.replace("]","");
					}
										
					client.swipeWhileNotFound("Down", 900, "WEB", "xpath=//*[@text='"+"AWB "+awbprefix+"-"+myobject2.get(i)+"'and @visible='true']", 1000, 5, false);
					client.swipeWhileNotFound("Up", 900, "WEB", "xpath=//*[@text='"+"AWB "+awbprefix+"-"+myobject2.get(i)+"'and @visible='true']", 1000, 5, false);
					int yvalue = Integer.parseInt(client.elementGetProperty("WEB","xpath=//*[@text='"+"AWB "+awbprefix+"-"+myobject2.get(i)+"'and @visible='true']",0, "y"));
					if (yvalue>1100 && yvalue<1450) {
						client.swipe("DOWN", 1000);
						client.click("WEB","xpath=//*[@text='"+"AWB "+awbprefix+"-"+myobject2.get(i)+"'and @visible='true']",0,1);
					}else if (yvalue<60) {	
						client.swipe("Up", 200);
						client.click("WEB","xpath=//*[@text='"+"AWB "+awbprefix+"-"+myobject2.get(i)+"'and @visible='true']",0,1);
					}else {
						client.click("WEB","xpath=//*[@text='"+"AWB "+awbprefix+"-"+myobject2.get(i)+"'and @visible='true']",0,1);
					}
					if (data.get("Orientation").equals("Landscape")) {
						client.click("WEB","xpath=//*[@text='"+"AWB "+awbprefix+"-"+myobject2.get(i)+"'and @visible='true']",1,1);
					}
				}
			}
		}
		//UpdateDriverdetails
		client.click("WEB", PagesDeliveriesTransfers.btnProceed, 0, 1);
		client.click("WEB",PagesDeliveriesTransfers.txtPersonId,0,1);
		client.sendText(data.get("PersonId"));
		client.click("WEB",PagesDeliveriesTransfers.txtDrivername,0,1);
		client.sendText(data.get("DriverName"));	
		client.click("WEB",PagesDeliveriesTransfers.txtLicenseno,0,1);
		client.sendText(data.get("LicenseNo"));
		client.click("WEB",PagesDeliveriesTransfers.txtPassno,0,1);
		client.sendText(data.get("PassNo"));
		client.closeKeyboard();
		client.click("WEB",PagesDeliveriesTransfers.txttapSignature , 0,1);
		client.elementSwipe("WEB",PagesDeliveriesTransfers.txtSignature , 0,"Right",20, 25);
		client.click("WEB",PagesDeliveriesTransfers.btnSave,0,1);
		client.click("WEB",PagesDeliveriesTransfers.btnCommit,0,1);
		//client.assertJUnitAndSeeTestReport(client.waitForElement("WEB", PagesHomeMenus.lblSuccessfullysaved,0,5000), "Bond Transfer/Delivery Receipt Saved successfully", "Bond Transfer/Delivery Receipt - Unsuccessful commit");
	
		}
	
	@SuppressWarnings("deprecation")
	public void AirlineTransfer(Map<String, String> data) throws Exception{
		String awbprefix ="";
		String awbserial = "";
		String url = "pickup/v1/airline-transfers/?transferManifestNumber="+(data.get("ReceiptNumber"));
		
		//Screen Transfer Manifest
		client.waitForElement("WEB", PagesDeliveriesTransfers.btnAWBHWB,0,2000);
		
		//Select view and get the check boxes
		if(data.get("View").equalsIgnoreCase("Location")) {
			client.click("WEB", PagesDeliveriesTransfers.btnLocation, 0, 1);
			
			List<String> myobject = GetResponse(url,"$..location");
			System.out.println(myobject.size());
			
			//select checkbox							
			for (int i=0;i<myobject.size();i++) {
				
				if (client.swipeWhileNotFound("Down", 900, "WEB", "xpath=//*[@text='"+myobject.get(i)+"' and @visible='true']", 1000, 5, false)) {					
				}else {
					client.swipeWhileNotFound("Up", 900, "WEB", "xpath=//*[@text='"+myobject.get(i)+"' and @visible='true']", 1000, 5, false);
				}
				
				int yvalue = Integer.parseInt(client.elementGetProperty("WEB", "xpath=//*[@text='"+myobject.get(i)+"']",0, "y"));
				if (yvalue>1100 && yvalue<1450) {
					client.swipe("DOWN", 1000);
					client.click("WEB","xpath=//*[@text='"+myobject.get(i)+"'and @visible='true']",0,1);
				}else if (yvalue<60) {	
					client.swipe("Up", 200);
					client.click("WEB","xpath=//*[@text='"+myobject.get(i)+"'and @visible='true']",0,1);
				}else {
					client.click("WEB","xpath=//*[@text='"+myobject.get(i)+"'and @visible='true']",0,1);
				}
				if (data.get("Orientation").equals("Landscape")) {
					client.click("WEB","xpath=//*[@text='"+myobject.get(i)+"'and @visible='true']",1,1);
				}
			}	
		}else {
			client.click("WEB", PagesDeliveriesTransfers.btnAWBHWB, 0, 1);
			
			//Json value formats
			List<String> myobject1 = GetResponse(url,"$..airwaybillPrefix");
			List<String> myobject2 = GetResponse(url,"$..airwaybillSerial");
			
			if (myobject2.size()==1) {
				awbprefix = myobject1.toString().replace("[","");
				awbprefix = awbprefix.replace("]","");
				awbserial = myobject2.toString().replace("[","");
				awbserial = awbserial.replace("]","");
				client.waitForElement("WEB", "xpath=//*[@text='"+"AWB "+awbprefix+"-"+awbserial+"']",0,2000);
				client.click("WEB","xpath=//*[@text='"+"AWB "+awbprefix+"-"+awbserial+"']",0,1);
			}else {
				for (int i=0;i<myobject2.size();i++) {
					if(myobject1.size()==myobject2.size()) {
						awbprefix = myobject1.get(i);
					}else {
						awbprefix = myobject1.toString().replace("[","");
						awbprefix = awbprefix.replace("]","");
					}
					client.swipeWhileNotFound("Down", 900, "WEB", "xpath=//*[@text='"+"AWB "+awbprefix+"-"+myobject2.get(i)+"'and @visible='true']", 1000, 5, false);
					client.swipeWhileNotFound("Up", 900, "WEB", "xpath=//*[@text='"+"AWB "+awbprefix+"-"+myobject2.get(i)+"'and @visible='true']", 1000, 5, false);
					int yvalue = Integer.parseInt(client.elementGetProperty("WEB","xpath=//*[@text='"+"AWB "+awbprefix+"-"+myobject2.get(i)+"'and @visible='true']",0, "y"));
					if (yvalue>1150 && yvalue<1450) {
						client.swipe("DOWN", 1000);
						client.click("WEB","xpath=//*[@text='"+"AWB "+awbprefix+"-"+myobject2.get(i)+"'and @visible='true']",0,1);
					}else if (yvalue<60) {	
						client.swipe("Up", 200);
						client.click("WEB","xpath=//*[@text='"+"AWB "+awbprefix+"-"+myobject2.get(i)+"'and @visible='true']",0,1);
					}else {
						client.click("WEB","xpath=//*[@text='"+"AWB "+awbprefix+"-"+myobject2.get(i)+"'and @visible='true']",0,1);
					}
					if (data.get("Orientation").equals("Landscape")) {
						client.click("WEB","xpath=//*[@text='"+"AWB "+awbprefix+"-"+myobject2.get(i)+"'and @visible='true']",1,1);
					}
				}
			}
		}
		//Common for both views
		client.click("WEB", PagesDeliveriesTransfers.btnClose, 0, 1);
		//client.assertJUnitAndSeeTestReport(client.waitForElement("WEB", PagesDeliveriesTransfers.lblHeader,0,3000),"Airline Transfer closed successfully." ,"Airline Transfer not closed properly" );
	}
}