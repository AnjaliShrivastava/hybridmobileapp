package com.company.auto;

import java.util.Map;
import com.company.pages.PagesHomeMenus;
import com.experitest.client.Client;

import junit.framework.Assert;

import com.company.pages.PagesAdditionalServices;
import com.company.pages.PagesFoundCargo;

public class BCAdditionalServices extends TestBase {
	
	
	
	public void SearchPageValidation() {
		//Acceptance Search screen validation
		/*//client.assertJUnitAndSeeTestReport(client.waitForElement("WEB", PagesAdditionalServices.titleAdditionalServices, 0, 8000), "Additional Services search screen and title exists as expected", "Additional Services search screen or title does not exist");
		//client.assertJUnitAndSeeTestReport(client.waitForElement("WEB", PagesAdditionalServices.lblRegisterAdditionalServices, 0, 8000), "Register additional services title exists as expected", "Register additional services title does not exist");
		//client.assertJUnitAndSeeTestReport(client.isElementFound("WEB",PagesHomeMenus.lblEnterNumber), "Enter Number label exists", "Enter Number label does not exist");
		//client.assertJUnitAndSeeTestReport(client.isElementFound("WEB",PagesHomeMenus.lblorScan), "or Scan label exists", "or Scan label does not exist");
		//client.assertJUnitAndSeeTestReport(client.isElementFound("WEB",PagesHomeMenus.btnAWB), "AWB toggle button exists", "AWB toggle buttondoes not exist");
		//client.assertJUnitAndSeeTestReport(client.isElementFound("WEB",PagesHomeMenus.btnFlight), "Flight toggle button exists", "Flight toggle buttondoes not exist");
		//client.assertJUnitAndSeeTestReport(client.isElementFound("WEB",PagesHomeMenus.btnULD), "ULD toggle button exists", "ULD toggle buttondoes not exist");
		//client.assertJUnitAndSeeTestReport(client.isElementFound("WEB",PagesHomeMenus.btnScanBarcode), "Scan button exists", "Scan button does not exist");
		//client.assertJUnitAndSeeTestReport(client.isElementFound("WEB",PagesHomeMenus.btnSearchDisabledinitial), "Search button exists", "Searchbutton does not exist");
		//client.assertJUnitAndSeeTestReport(client.isElementFound("WEB",PagesHomeMenus.txtPrefixNumber), "Prefix text box exists", "Prefix text box does not exist");
		//client.assertJUnitAndSeeTestReport(client.isElementFound("WEB",PagesHomeMenus.txtSerialNumber), "Serial text box exists", "Serial text box does not exist");
	*/	client.click("WEB",PagesHomeMenus.txtPrefixNumber, 0, 1);
		client.sendText("950");
		client.closeKeyboard();
		client.click("WEB",PagesHomeMenus.txtSerialNumber, 0, 1);
		client.sendText("1");
		client.closeKeyboard();
		////client.assertJUnitAndSeeTestReport(client.isElementFound("WEB",PagesHomeMenus.btnSearchEnabled), "Search button Enabled as expected", "Search button does not exist/Disabled");
		ClickSendText(PagesHomeMenus.txtSerialNumber,"{BKSP}");
		////client.assertJUnitAndSeeTestReport(client.isElementFound("WEB",PagesHomeMenus.btnSearchDisabledreload), "Search button disabled as expected", "Search button does not exist/Enabled");
		client.click("WEB",PagesHomeMenus.btnFlight,0,1);
		//client.assertJUnitAndSeeTestReport(client.waitForElement("WEB",PagesHomeMenus.txtSearchFlight,0,2000), "Search Flight text box exists", "Search Flight text box does not exist");
		//client.assertJUnitAndSeeTestReport(client.isElementFound("WEB",PagesHomeMenus.btnSearchDisabledinitial), "Search button disabled as expected", "Search button does not exist/Enabled");
		//client.assertJUnitAndSeeTestReport(client.isElementFound("WEB",PagesHomeMenus.calendaricon), "Calendar button exists", "Calender button does not exist/Enabled");
		//client.assertJUnitAndSeeTestReport(client.isElementFound("WEB",PagesHomeMenus.txtlabelDate), "Date label exists", "Date label does not exist/Enabled");
		ClickSendText(PagesHomeMenus.txtSearchFlight,"a");
		//client.assertJUnitAndSeeTestReport(client.isElementFound("WEB",PagesHomeMenus.btnSearchEnabled), "Search button Enabled as expected", "Search button does not exist/Disabled");
		ClickSendText(PagesHomeMenus.txtSearchFlight,"{BKSP}");
		//client.assertJUnitAndSeeTestReport(client.isElementFound("WEB",PagesHomeMenus.btnSearchDisabledreload), "Search button disabled as expected", "Search button does not exist/Enabled");
		client.click("WEB",PagesHomeMenus.btnULD,0,1);
		//client.assertJUnitAndSeeTestReport(client.waitForElement("WEB",PagesHomeMenus.txtULD,0,2000), "ULD text box exists", "ULD text box does not exist");
		//client.assertJUnitAndSeeTestReport(client.isElementFound("WEB",PagesHomeMenus.btnSearchDisabledinitial), "Search button disabled as expected", "Search button does not exist/Enabled");
		ClickSendText(PagesHomeMenus.txtULD,"a");
		//client.assertJUnitAndSeeTestReport(client.isElementFound("WEB",PagesHomeMenus.btnSearchEnabled), "Search button Enabled as expected", "Search button does not exist/Disabled");
		client.click("WEB", PagesHomeMenus.btnSearchEnabled, 0, 1);
		//client.assertJUnitAndSeeTestReport(client.isElementFound("WEB",PagesHomeMenus.lblInvalidULD), "Invalid ULD Identifier label exists as expected", "Invalid ULD Identifier label does not exist/Disabled");
		ClickSendText(PagesHomeMenus.txtULD,"{BKSP}");
		//client.assertJUnitAndSeeTestReport(client.isElementFound("WEB",PagesHomeMenus.btnSearchDisabledreload), "Search button disabled as expected", "Search button does not exist/Enabled");
	}
	
	public void Search(Map<String, String> data) {
		switch(data.get("SearchUsing")) {
			case "AWB":
				if (data.get("AWBPrefix").isEmpty()==false) {
					client.click("WEB",PagesHomeMenus.btnAWB, 0, 1);
					client.click("WEB",PagesHomeMenus.txtPrefixNumber, 0, 1);
					client.sendText(data.get("AWBPrefix"));
					client.closeKeyboard();
					client.click("WEB",PagesHomeMenus.txtSerialNumber, 0, 1);
					client.sendText(data.get("AWBSerial"));
					client.closeKeyboard();			
					//client.sendText(data.get("AWBPrefix")+"  "+data.get("AWBSerial"));
				}	else {
					client.report("AWB value in datasheet not updated properly", false);
					return;
				}
				if (data.get("AutoPopulate").equalsIgnoreCase("YES")){
					
					client.waitForElement("WEB", "//*[contains(text(),'"+data.get("AWBSerial")+"') and @class='highlight']", 0, 12000);
					client.click("WEB", "//*[contains(text(),'"+data.get("AWBSerial")+"') and @class='highlight']", 0, 1);
				}else {
					client.click("WEB", PagesHomeMenus.btnSearch, 0, 1);		
				}
				//client.assertJUnitAndSeeTestReport(client.waitForElement("WEB","xpath=//*[contains(text(),'"+ data.get("AWBPrefix")+"-"+data.get("AWBSerial")+"')]", 0, 8000),"Autopopulate Search - Success","Autopopulate Search - Failed");
				break;
			case "FLIGHT":
				if (data.get("Flight").isEmpty()==false) {	
					client.click("WEB", PagesHomeMenus.btnFlight, 0, 1);
					ClickSendText(PagesHomeMenus.txtSearchFlight,data.get("Flight"));
					//*[@text=' XS7801 ']
					client.click("WEB", "//*[contains(text(),' "+data.get("Flight")+"')]",0,1);
					client.click("WEB", PagesHomeMenus.btnProceed, 0, 1);
				}else {
					client.report("Flight value in datasheet not updated properly", false);
					return;					
				}
				LegSelection(data);
				break;
			case "ULD":
				if (data.get("ULD").isEmpty()==false) {	
					client.click("WEB", PagesHomeMenus.btnULD, 0, 1);
					ClickSendText(PagesHomeMenus.txtULD,data.get("ULD"));
					client.click("WEB", "xpath=//*[contains(text(),'"+data.get("ULD")+"')]", 0, 1);
					//client.assertJUnitAndSeeTestReport(client.waitForElement("WEB","xpath=//*[@text='"+data.get("ULD")+"']",0,2000), " exists", "Search Flight text box does not exist");
				}else {
					client.report("ULD value in datasheet not updated properly", false);
					return;					
				}
				ULDAWBFLIGHTSelection(data);
				break;
		}
	}
	
	public void ULDAWBFLIGHTSelection(Map<String, String> data) {
		
		//client.assertJUnitAndSeeTestReport(client.waitForElement("WEB",PagesAdditionalServices.txtSearchForFlights,0,4000), "Search for Shipments text box exists", "Search for Shipments text box does not exist");
		//client.assertJUnitAndSeeTestReport(client.waitForElement("WEB",PagesAdditionalServices.lblRegisterAddServicesforFlight,0,2000), "Label Additional Services for Flight/AWB exists", "Label Additional Services for Flight/AWB does not exist");
		if((client.waitForElement("WEB",PagesAdditionalServices.lblflightcounttitle,0,1000)) || (client.waitForElement("WEB",PagesAdditionalServices.lblzeroflightcounttitle,0,1000)) ) {
			client.report("Flight title label exists", true);
		}else {
			client.report("Flight title label does not exist", false);
		}
		//client.assertJUnitAndSeeTestReport(client.waitForElement("WEB",PagesAdditionalServices.lblShipments,0,2000), "Label Shipments exists", "Label shipments does not exist");
		if (data.get("Flight").isEmpty()==false) {
			client.click("WEB", "xpath=//*[contains(text(),'"+data.get("Flight")+"')]", 0, 1);
			LegSelection(data);
		}
		if (data.get("AWBPrefix").isEmpty()==false) {
			client.click("WEB", "xpath=//*[contains(text(),'"+data.get("AWBPrefix")+"-"+data.get("AWBSerial")+"')]", 0, 1);
		}
	}
	
	public void LegSelection(Map<String, String> data) {
		if(data.get("LegDetails").equalsIgnoreCase("IMPORT")) {
			//client.assertJUnitAndSeeTestReport(client.waitForElement("WEB",PagesAdditionalServices.lblLegSelectionFor,0,2000), "Leg Selection for label exists", "Leg Selection for label does not exist");
			//client.assertJUnitAndSeeTestReport(client.isElementFound("WEB",PagesAdditionalServices.btnCancel), "Cancel button exists as expected", "Cancel button does not exist");
			client.click("WEB", PagesAdditionalServices.lblImport, 0, 1);
		}else if(data.get("LegDetails").equalsIgnoreCase("EXPORT")) {
			//client.assertJUnitAndSeeTestReport(client.waitForElement("WEB",PagesAdditionalServices.lblLegSelectionFor,0,2000), "Leg Selection for label exists", "Leg Selection for label does not exist");
			//client.assertJUnitAndSeeTestReport(client.isElementFound("WEB",PagesAdditionalServices.btnCancel), "Cancel button exists as expected", "Cancel button does not exist");
			client.click("WEB", PagesAdditionalServices.lblExport, 0, 1);
		}else {
			client.report("Leg Details is not updated properly in DataSheet", false);
		}
		//client.assertJUnitAndSeeTestReport(client.waitForElement("WEB","xpath=//*[contains(text(),'"+ data.get("Flight")+"')]", 0, 8000),"Flight Search - Success","Flight Search - Failed");
	}
	public void RegisterAddServices(Map<String, String> data) {
		//client.assertJUnitAndSeeTestReport(client.waitForElement("WEB", PagesAdditionalServices.lblRegisterAdditionalServices, 0, 2000),"Label Register Additional Services exist","Label Register Additional Services does not exist");
		//client.assertJUnitAndSeeTestReport(client.isElementFound("WEB", PagesAdditionalServices.titleAdditionalServices),"Label Additional Services Exists","Label Additional Services does not Exist");
		if (data.get("Orientation").equalsIgnoreCase("Landscape")) {
			//client.assertJUnitAndSeeTestReport(client.isElementFound("WEB", PagesAdditionalServices.txtSearchCode),"Search Code or decsription text box Exists","Search Code or decsription text box does not Exist");
		}	
		String[] codevalues = data.get("Code").split(",");	
		String[] Qtyvalues = data.get("Quantity").split(",");
		
		//for (int i=0;i < Qtyvalues.length;i++) {	
			if (data.get("Orientation").equalsIgnoreCase("Portrait")) {
				client.click("WEB", PagesAdditionalServices.imgAdditionalServices, 0, 1);
				//client.assertJUnitAndSeeTestReport(client.waitForElement("WEB", PagesAdditionalServices.lblAddServicesFor, 0, 2000),"Label Additional Services For exists","Label Additional Services For does not exist");
				//client.assertJUnitAndSeeTestReport(client.isElementFound("WEB", PagesAdditionalServices.titleAdditionalServices),"Label Additional Services Exists","Label Additional Services does not Exist");
			}
			client.click("WEB", PagesAdditionalServices.txtSearchCode,0,1);
			client.sendText(codevalues[0]);
			client.click("WEB", "xpath=//*[contains(text(),'"+codevalues[0]+"')]", 0, 1);
			//client.assertJUnitAndSeeTestReport(client.waitForElement("WEB", PagesAdditionalServices.lblQuantity, 0, 2000),"Label Quantity exists","Label Quantity does not exist");
			//client.assertJUnitAndSeeTestReport(client.isElementFound("WEB", PagesAdditionalServices.btnCancel),"Cancel Button Exists","Cancel button does not Exist");
			/*client.click("WEB", PagesAdditionalServices.txtQuantity, 0, 1);
			client.sendText(Qtyvalues[0]);*/
			ClickClearSendText(PagesAdditionalServices.txtQuantity,Qtyvalues[0]);
			client.click("WEB", PagesAdditionalServices.btnDone, 0, 1);
		
		client.swipe("Down",400,500);
		for (String s:codevalues) {					
			client.isElementFound("WEB", "xpath=//*[contains(text(),'"+s+"')]");				
		}
		
	}
	
	public void Save() {
		client.click("WEB", PagesHomeMenus.btnSave,0,1);
		client.waitForElement("Web", "xpath=//*[@text='Saved']", 0, 4000);
		client.verifyElementFound("Web", "xpath=//*[@text='Saved']", 0);
	
		//client.assertJUnitAndSeeTestReport(client.waitForElement("WEB", PagesHomeMenus.lblSuccessfullysaved, 0, 8000), "Additional Services Report created Successfully", "Additional Services Report - Not created");
	}
	
}