/*package com.company.auto;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.Random;
import org.json.JSONArray;
import org.json.JSONObject;
import com.company.pages.PagesCheckList;
import com.company.pages.PagesDeliveriesTransfers;
import com.company.pages.PagesHomeMenus;

public class BCCheckList2 extends TestBase {


	public BCCheckList2(MyClient client) {
		this.client = client;


	}




	public void search(Map<String, String> data) {

		if (data.get("AWBPrefix").isEmpty() == false) {
			//client.click("WEB", PagesHomeMenus.btnAWB, 0, 1);
			client.click("WEB", PagesHomeMenus.txtPrefixNumber, 0, 1);
			client.sendText(data.get("AWBPrefix"));
			client.closeKeyboard();
			client.click("WEB", PagesHomeMenus.txtSerialNumber, 0, 1);
			client.sendText(data.get("AWBSerial"));
			client.closeKeyboard();

		} else {
			client.report("AWB value in datasheet not updated properly", false);
		}

		client.assertJUnitAndSeeTestReport(client.waitForElement("WEB",
				"xpath=//*[contains(text(),'" + data.get("AWBPrefix") + " " + data.get("AWBSerial") + "')]", 0, 10000),
				"Autopopulate Search - Success", "Autopopulate Search - Failed");
		if (data.get("AutoPopulate").equalsIgnoreCase("YES")) {
			// ********** Click on Auto Suggest List********

			client.click("WEB",
					"xpath=//*[contains(text(),'" + data.get("AWBPrefix") + " " + data.get("AWBSerial") + "')]", 0, 1);
		} else {
			client.click("WEB", PagesHomeMenus.btnSearch, 0, 1);
		}

	}

	public void selectParts(Map<String, String> data) {
		client.assertJUnitAndSeeTestReport(client.waitForElement("WEB", PagesCheckList.PCs, 0, 8000),
				"Records are displaying", " Records are not displaying");
		client.click("WEB", PagesCheckList.PCs, 0, 1);
		client.assertJUnitAndSeeTestReport(client.isElementFound("WEB", PagesCheckList.checkListName, 0),
				"CheckList Name is displaying", "CheckList Name is not displaying");
		if (client.isElementFound("WEB", "xpath=//*[contains(text(),'pending')]")) {
			client.click("WEB", "xpath=//*[contains(text(),'pending')]", 0, 1);
			//client.waitForElement("WEB", PagesCheckList.checkListName, 0, 8000);


		}

	}

	public void PrepareACheckList() throws Exception {
		try {
			String url = "http://192.168.48.10/chk/templates/t001";
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			con.setRequestMethod("GET");
			con.setRequestProperty("accept", "application/json");
			con.setRequestProperty("Content-Type", "application/json");
			con.setRequestProperty("Authorization", "Basic YWRtaW46Y2hhbmdlaXQ=");
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;

			StringBuffer response = new StringBuffer();
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();

			JSONObject obj_JSONObject = new JSONObject(response.toString());

			// *******Getting Section Name **********

			JSONArray obj_sections = obj_JSONObject.getJSONArray("sections");


			int totalSections = obj_sections.length();

			for (int k = 0; k < totalSections; k++) {

				int nextCount = 0;


				JSONObject obj_firstPart = obj_sections.getJSONObject(k);

				System.out.println(obj_firstPart);

				String sectionName = obj_firstPart.getString("name");

				System.out.println(k);

				if (sectionName.equals("Header")) {

					client.click("WEB", PagesCheckList.checklistSectionHeader, 0, 1);

					client.waitForElement("WEB", PagesCheckList.checklistSectionHeader, 0, 8000);
				}




				// *******Getting Screens **********

				JSONArray obj_questionGroups = obj_firstPart.getJSONArray("questionGroups");

				int totalScreen = obj_questionGroups.length();

				for (int a = 0; a < totalScreen; a++) {



					JSONObject obj_firstScreen = obj_questionGroups.getJSONObject(a);

					System.out.println(obj_firstScreen);

					// *******Getting Questions **********

					JSONArray obj_questions = obj_firstScreen.getJSONArray("questions");

					int totalQuestions = obj_questions.length();
					for (int i = 0; i < totalQuestions; i++) {

						JSONObject obj_ques = obj_questions.getJSONObject(i);

						// *******Getting Question values **********
						String question = obj_ques.getString("question");
						System.out.println(question);
						int questionNumber = obj_ques.getInt("questionNumber");
						client.waitForElement("WEB","xpath=//*[contains(text(),'"+questionNumber+"."+question.replace("\r", "").replace("\n", "")+"')]",0,8000);

						String QuestionString = questionNumber+"."+question.replace("\r", "").replace("\n", "");



						// client.assertJUnitAndSeeTestReport(client.isElementFound("WEB",
						// "xpath=//*[contains(text(),'"+questionNumber+"."+question+"')]"), "Question
						// is displayed ", "Question is not displayed" );
						boolean mandatoryStatus = obj_ques.getBoolean("mandatory");
						if (obj_ques.has("questionNote")) {
							// String questionNote = obj_ques.getString("questionNote");
							// client.assertJUnitAndSeeTestReport(client.isElementFound("WEB",
							// "xpath=//*[contains(text(),'"+questionNote+"')]"), "QuestionNote is displayed
							// ", "QuestionNote is not displayed" );

							if (mandatoryStatus == true) {
								// client.assertJUnitAndSeeTestReport(client.isElementFound("WEB",
								// "xpath=//*[@src='assets/images/mandatorystar.svg']"), " Mandatory mark is
								// displayed ", "Mandatory mark is not displayed" );

							}
						}
						// *******Getting Answers **********

						JSONObject obj_answerRange = obj_ques.getJSONObject("answerRange");

						String obj_answerType = obj_answerRange.getString("answerType");

						String questionText =questionNumber+"."+question.replace("\r", "").replace("\n", "");

						if (obj_answerRange.has("options") && !obj_answerType.equalsIgnoreCase("integer"))  {


							JSONArray obj_options = obj_answerRange.getJSONArray("options");
							int totalOptions = obj_options.length();

							for (int j = 0; j < totalOptions; j++) {

								JSONObject obj_firstAnswer = obj_options.getJSONObject(j);

								String Answer_Option = obj_firstAnswer.getString("option");

								//client.assertJUnitAndSeeTestReport(client.isElementFound("WEB","xpath=//*[@text='"+Answer_Option+"']"), Answer_Option+" Button is available on screen ", Answer_Option+" Button is not available on screen " );
								if(questionText.length()>=35 ) {
									client.assertJUnitAndSeeTestReport(client.isFoundIn("WEB", questionText, 0, "Down", "WEB", "xpath=//*[@text='"+Answer_Option+"']", 0, 0), Answer_Option+" Button is available on screen ", Answer_Option+" Button is not available on screen " );
								} }

							if(questionText.length()<34 && i!=0 ) {
								JSONObject obj_firstAnswer = obj_options.getJSONObject(0);

								String Answer_Option = obj_firstAnswer.getString("option");

								client.clickIn("WEB","xpath=//*[contains(text(),'" + questionNumber+"."+question.replace("\r", "").replace("\n", "") +"')]", 0, "Down","WEB", "xpath=//*[contains(text(),'"+ Answer_Option + "')]", 0, 0, 0, 1);

							}else {

							Random r = new Random();
							int random = r.nextInt(totalOptions);
							JSONObject obj_firstAnswer = obj_options.getJSONObject(random);

							System.out.println(obj_firstAnswer);

							String Answer_Option = obj_firstAnswer.getString("option");

							if(i== 0) {
								client.click("WEB", "xpath=//*[contains(text(),'"+ Answer_Option +"')]", 0, 1);

							}else {


								client.clickIn("WEB","xpath=//*[contains(text(),'" + questionNumber+"."+question.replace("\r", "").replace("\n", "") +"')]", 0, "Down","WEB", "xpath=//*[contains(text(),'"+ Answer_Option + "')]", 0, 0, 0, 1);

							} }	}							


						else if (obj_answerType.equalsIgnoreCase("media")) {


						} else if (obj_answerType.equalsIgnoreCase("signature")) {
							client.click("WEB", PagesCheckList.signatureIcon, 2, 1);
							client.elementSwipe("WEB", PagesCheckList.signatureArea, 0, "Right", 20, 15);
							client.click("WEB", PagesCheckList.saveButton, 0, 1);

						} else if (obj_answerType.equalsIgnoreCase("numeric") || obj_answerType.equalsIgnoreCase("integer")) {
							client.waitForElement("WEB", PagesCheckList.checklistTextField, 0, 8000);
							client.click("WEB", PagesCheckList.checklistTextField, 0, 1);
							client.sendText("1234");
							client.closeKeyboard();

						} else {
							client.waitForElement("WEB", PagesCheckList.checklistTextField, 0, 8000);
							client.click("WEB", PagesCheckList.checklistTextField, 0, 1);
							client.sendText("Testing an String fields");
							client.closeKeyboard();

						}
					}

					if(nextCount!= totalScreen-1) {
						client.assertJUnitAndSeeTestReport(client.waitForElement("WEB", PagesCheckList.nextButton, 0, 8000),
								" Next Button is displayed on screen ", "Next Button is not displayed on screen");
						client.click("WEB", PagesCheckList.nextButton, 0, 1);

						nextCount ++;


						//Code for done also

					}else {

						client.assertJUnitAndSeeTestReport(client.waitForElement("WEB", PagesCheckList.doneButton, 0, 8000)," Done Button is displayed on screen ", "Done Button not is displayed on screen");
						client.click("WEB", PagesCheckList.doneButton, 0, 1);
					}


				}





			}

			//client.assertJUnitAndSeeTestReport(client.waitForElement("WEB", PagesCheckList.completionText, 0, 12000), "Checklist Completion text is displayed", "Checklist Completion text is not displayed");

			client.assertJUnitAndSeeTestReport(client.isElementFound("WEB", "xpath=//*[@value='verified']" ), "Verified button is diaplyed", "Verified button is not displayed");

			client.assertJUnitAndSeeTestReport(client.isElementFound("WEB", PagesCheckList.signatureIcon ), "Signature Section is diaplyed", "Signature section is not displayed");

			client.click("WEB", PagesCheckList.signatureIcon, 10, 1);
			client.elementSwipe("WEB", PagesCheckList.signatureArea, 0, "Right", 20, 10);
			client.click("WEB", PagesCheckList.saveButton, 0, 1);

			client.waitForElement("WEB", PagesCheckList.saveButton, 0, 8000);
			client.click("WEB", PagesCheckList.saveButton, 0, 1);
			client.assertJUnitAndSeeTestReport(client.isElementFound("WEB", PagesCheckList.lblSaved ), "Saved Message is displayed", "Saved Message is not displayed");





		}catch (Exception e) {

			System.out.println("Exception occured" + e.getMessage());
		}
	}


	public void chkList() {



		String str2 = "xpath=//*[@text='2.Proper Shipping Name and where Special Provision A78 applies, the supplementary information inbrackets [10.8.3.9.1, Step 2] ']";

		String str3 = "xpath=//*[@text='3.Class 7 [10.8.3.9.1, Step 3] ']";

		String str4 ="xpath=//*[@text='4.Subsidiary Risk, in parentheses, immediately following Class [10.8.3.9.1, Step 4] and Packing Group ifrequired for Subsidiary Risk [10.8.3.9.1, Step 5] . ']" ;

		client.click("WEB", PagesCheckList.checklistSectionIdentification, 0, 1);

		client.click("WEB", "xpath=//*[@text='No']", 0, 1);



		client.isFoundIn("WEB", str2, 0, "Down", "WEB", "xpath=//*[contains(text(),'Yes')]", 0, 0);

		//client.waitForElement("WEB", "xpath=//*[contains(text(),'Yes')]", 1, 8000);

		client.clickIn("WEB",str2, 0, "Down","WEB","xpath=//*[@text='No']",0,0, 0,1);

		client.isFoundIn("WEB", str3, 0, "Down", "WEB", "xpath=//*[contains(text(),'Yes')]", 0, 0);

		//client.waitForElement("WEB", "xpath=//*[contains(text(),'Yes')]", 2, 8000);

		client.clickIn("WEB",str3, 0, "Down","WEB","xpath=//*[@text='No']",0,0, 0,1);

		//client.waitForElement("WEB", "xpath=//*[contains(text(),'Yes')]", 3, 8000);

		client.isFoundIn("WEB", str4, 0, "Down", "WEB", "xpath=//*[contains(text(),'Yes')]", 0, 0);

		client.clickIn("WEB",str4, 0, "Down","WEB","xpath=//*[@text='No']",0,0, 0,1);




		//}


		client.click("WEB", PagesCheckList.doneButton, 0, 1);


	}




	public void serachPageValidation() {
		
		System.out.println("Yet to  commeit");
		
	}
}



*/