package com.company.pages;

import com.company.auto.TestBase;

public class PagesCheckList extends TestBase {
	
	//Screen 2
	public static final String titleCheckList ="xpath=//*[contains(text(),'"+TestBase.Gettextvalue("checklisttitle")+"')]";
	public static final String lblStartChecklist ="xpath=//*[contains(text(),'"+TestBase.Gettextvalue("startchecklist")+"')]" ;
    public static final String PCs = "xpath=//*[contains(text(),'Pcs')]";
    public static final String checkListName ="xpath=//*[@text='IATA DG checklist']" ;
    public static final String checklistSectionHeader ="xpath=//*[contains(text(),'Header')]";
    public static final String checklistSectionIdentification ="xpath=//*[contains(text(),'Identification')]";
    public static final String checklistSectionTemparature =  "xpath=//*[contains(text(),'Temperature')]";
    public static final String nextButton =  "xpath=//*[contains(text(),'NEXT')]";
    public static final String checklistTextField ="xpath=//*[@placeholder='Enter Checklist info']";
    public static final String checklistUseCameraIcon ="xpath=//*[@src='assets/images/calendar.svg']";	
    public static final String checklistUseCameraOption="xpath=//*[contains(text(),'Use Camera']";
    public static final String checklistUseGallaryOption="xpath=//*[@text='Use Gallery']";
    public static final String selectOption="xpath=//*[contains(text(),'Select Option']";
    public static final String recordVideo="xpath=//*[contains(text(),'Record Video']";
    public static final String captureImage="xpath=//*[contains(text(),'Capture Image']";
    public static final String cameraClick="xpath=//*[@id='shutter_button']";
    public static final String existingPhoto = "xpath=//*[@id='icon_thumb']";
    public static final String signatureIcon ="xpath=(//*[@nodeName='IMG'])[14]";
    public static final String signatureArea ="xpath=//*[@nodeName='CANVAS']";
    public static final String doneButton =  "xpath=//*[contains(text(),'Done')]";
    public static final String saveButton =  "xpath=//*[contains(text(),'SAVE')]";
    public static final String completionText ="xpath=//*[contains(text(),'CheckList completion')]";
    public static final String lblSaved =  "xpath=//*[@text='Saved']";
    
    
    
    
    
    
    
    
} 