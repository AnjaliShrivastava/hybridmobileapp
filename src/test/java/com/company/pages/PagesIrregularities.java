package com.company.pages;

import com.company.auto.TestBase;

public class PagesIrregularities extends TestBase{

	//WEB Objects	
	public static final String lblIrregularities = "xpath=//*[@text='"+TestBase.Gettextvalue("irregularityreport")+"']";;
	public static final String lblIrrReport = "xpath=//*[@text='"+TestBase.Gettextvalue("createOrViewIrregularityReport")+"']";
	
	//Screen 1
	public static final String btnCreateReport = "//*[contains(text(),' CREATE NEW REPORT')]";
	
	//Screen2
	public static final String lblCreateIrrReport ="xpath=//*[@id='irregularityLabel']";
	public static final String txtPieces = "xpath=//*[@placeholder='Pieces']";
	public static final String txtweight = "//*[@placeholder='Weight']";
	public static final String lblKG = "xpath=//*[@text='KG']";
	public static final String btnIrrDetails = "xpath=//*[contains(text(),'Irregularity Details')]";
	public static final String lblCodeandDesc = "xpath=//*[@text='Code and description']";
	
	public static final String lblIrrDetails = "xpath=//*[@text='Irregularity Details']";
	public static final String txtSearchIrrDetails = "xpath=//*[@placeholder='Start searching for irregularity codes' and @type='text']";
	public static final String lblIrrReportfor = "xpath=//*[contains(text(),'Irregularity Report for')]";
	public static final String lblExceedAWBpieces = "xpath=//*[@text='Pieces must not exceed AWB Pieces']";
	public static final String lblIrrCode = "xpath=//*[@text=' Code']";
	public static final String lblIrrDesc = "xpath=//*[@text='Description']";
	public static final String btnSave = "xpath=//*[contains(text(),'SAVE')]";
	public static final String lblSuccessfullysaved ="xpath=//*[@text='Successfully Saved!']";
	
}