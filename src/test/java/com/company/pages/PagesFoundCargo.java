package com.company.pages;

import com.company.auto.TestBase;

public class PagesFoundCargo extends TestBase{

	//WEB Objects
	//Screen 1
	public static final String lblFoundCargo ="xpath=//*[@text='Found Cargo']";
	public static final String lblRegister ="xpath=//*[@text='Register A Found Cargo']";
	
	public static final String btnUnknownAWB ="xpath=//*[@text='UnKnown AWB']";
	public static final String btnSearch ="xpath=//*[@name='search']";
	
	//Screen2
	public static final String lblOrigin ="xpath=//*[contains(text(),'Origin')]";
	public static final String lbltitle = "xpath=//*[contains(@class,'toolbar-title')]"; 
	public static final String txtOrigin ="xpath=//*[@text='                                Origin']";
	public static final String lblNatureofGoods ="xpath=//*[@text='Nature of Goods']";
	public static final String txtNatureofGoods ="xpath=//*[@placeholder='Nature of Goods' and @type='text']";
	public static final String lblFoundPieces ="xpath=//*[@text='Found Pieces']";
	public static final String txtFoundPieces ="//*[@placeholder='Found Pieces']";
	public static final String lblFoundWeight ="xpath=//*[@text='Found Weight']";
	public static final String txtFoundWeight ="//*[@placeholder='Found Weight']";
	public static final String lblUnit ="xpath=//*[@text='Unit']";
	public static final String lblKg ="xpath=//*[@text='KG']";
	public static final String lblSHC ="xpath=//*[@text='SHC']";
	public static final String btnAddSHC ="//*[@text='Add SHC']";
	public static final String lblPreferredLocation ="xpath=//*[@text='Preferred Location' and @visible='true']";
	public static final String lblStorageLocation ="xpath=//*[@text='Storage Location'and @visible='true']";
	public static final String lblLocation ="xpath=//*[@nodeName='ION-LABEL' and  [contains(text(),'Location')] and @visible='true']";
	public static final String txtLocation ="xpath=//*[@placeholder='Location' and @type='text' and @visible='true']";
	public static final String lblPieces ="xpath=//*[@nodeName='ION-LABEL' and [contains(text(),'Piece(s)')] and @visible='true']";
	public static final String txtPieces ="xpath=//*[@placeholder='Piece(s)' and @type='text' and @visible='true']";
	public static final String lblWeight ="xpath=//*[@nodeName='ION-LABEL' and  [contains(text(),'Weight')] and @visible='true']";
	public static final String txtWeight ="xpath=//*[@placeholder='Weight' and @type='text' and @visible='true']";
	public static final String btnSaveDisabled ="xpath=//*[@nodeName='IMG' and @src='assets/images/save-disable.svg']";
	public static final String btnSaveEnabled ="xpath=//*[@nodeName='IMG' and @src='assets/images/save.svg']";
	public static final String lblNotAvailable = "xpath=//*[@text='"+TestBase.Gettextvalue("notavailable")+"']";
	
	//Select Origin
	public static final String lblSelectOrigin ="xpath=//*[@text='Select Origin']";	
	public static final String btnOriginSearch ="xpath=//*[@name='search']";
	public static final String lblAirportList ="xpath=//*[@text='Airport List']";
	public static final String lblSelectOriginFor ="xpath=//*[contains(text(),'Select Origin for')]";
	
	//ADD SHC
	public static final String lblADDSHC ="xpath=//*[contains(text(),'SHC')]";
	
	public static final String btnSave ="xpath=//*[contains(text(),'SAVE')]";
	public static final String lblSHClist ="xpath=//*[contains(text(),'SHC List')]";
	public static final String btnOK = "xpath=//*[@text='OK']";
	
	//landscape
	public static final String mnustoragelocation = "xpath=//*[contains(text(),'"+TestBase.Gettextvalue("storagelocation")+"')]";
	public static final String  mnustoragelocationsubtext = "xpath=//*[contains(text(),'"+TestBase.Gettextvalue("storagelocationsubtext")+"')]";
}