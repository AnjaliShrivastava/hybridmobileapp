package com.company.pages;

import com.company.auto.TestBase;

public class PagesDeliveriesTransfers extends TestBase{
		
	//Home Screen of Delivery Transfer

	//WEB Objects	
	public static final String lblpopupheader = "xpath=//*[@text='Error']";
	public static final String lblpopupbody = "xpath=//*[@text='Enter the Delivery Receipt Number']";
	public static final String btnOK = "xpath=//*[@text='OK']";
	public static final String lblNORECORD = "xpath=//*[@text='No Record Found']";
	
	//Screen 1
	public static final String lblHeader ="xpath=//*[@text='Deliveries & Transfers']";
	public static final String txtReceiptNumber = "xpath=//*[@nodeName='INPUT' and @placeholder='Enter Receipt/Transfer Number']";
	public static final String btnSearch = "xpath=//*[@nodeName='IMG' and ./parent::*[@nodeName='ION-THUMBNAIL']]"; 
	public static final String lblEnterReceipt = "xpath=//*[@text='Enter Receipt/Transfer Number']";
	public static final String lblProcessDeliveryTransfer = "xpath=//*[@text='Process delivery/Transfer']";
	public static final String btnScanBarcode = "xpath=//*[contains(text(),'SCAN BARCODE')]";
	
	
	//Screen 2 - Receipt Number
	public static final String lblBondTransfer = "xpath=//*[@text='Bond Transfer']";
	public static final String lblDeliveryReceipt = "xpath=//*[@text='Delivery Receipt']";
	public static final String btnBondTransfer = "xpath=//*[@nodeName='H2' and @text='Bond Transfer']";
	public static final String btnDeliveryReceipt = "xpath=//*[@nodeName='H2' and @text='Delivery Receipt']";
	
	//Screen3 -  Bond Transfer
	public static final String lblTransferManifest = "xpath=//*[contains(text(),'Transfer Manifest')]";
	public static final String lblPiecesforDelivery = "xpath=//*[contains(text(),'Pieces for Delivery']";
	public static final String btnLocation = "xpath=//*[contains(text(),'Location')]";
	public static final String btnAWBHWB = "xpath=//*[contains(text(),'AWB/HWB')]";
	public static final String btnProceed = "xpath=//*[@text='"+TestBase.Gettextvalue("proceed")+"']";
	public static final String btnReset = "xpath=//*[@text='"+TestBase.Gettextvalue("reset")+"']";
	
	//Screen 4 - Delivery Receipt
	public static final String lblDeliveryReceiptNumber = "xpath=//*[contains(text(),'Delivery Receipt')]";
	
	//Screen 5 - Confirmation Delivery Receipt
	public static final String txtPersonId = "xpath=//*[@nodeName='INPUT' and @placeholder='Person ID']";
	public static final String txtDrivername = "xpath=//*[@nodeName='INPUT' and @placeholder='Driver Name']";
	public static final String txtPassno = "xpath=//*[@nodeName='INPUT' and @placeholder='Pass No']";
	public static final String txtLicenseno = "xpath=//*[@nodeName='INPUT' and @placeholder='License No']";
	public static final String txtSignature = "xpath=//*[@nodeName='CANVAS']";
	public static final String txttapSignature = "xpath=//*[@id='signaturePadCol']";
	public static final String btnSave ="xpath=//*[contains(text(),'SAVE')]";
	public static final String btnCommit = "xpath=//*[@text='COMMIT']";
			
	//Airline Transfer
	public static final String lblEnterTransferManifest ="xpath=//*[@text='Enter Transfer Manifest Number']";
	public static final String txtEnterNumber ="xpath=//*[@nodeName='INPUT' and @placeholder='Manifest Number']";
	public static final String lblAirlineTransfer ="xpath=//*[@text='Airline Transfer']";
	public static final String lblorScan ="xpath=//*[contains(text(),'or Scan')]";
	public static final String btnAirlineSearch ="xpath=//*[@nodeName='BUTTON' and @class='search-button']";
	public static final String btnClose ="xpath=//*[@text='Close']";
	
	
}