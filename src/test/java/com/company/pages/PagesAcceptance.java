package com.company.pages;

import com.company.auto.TestBase;

public class PagesAcceptance extends TestBase{

	//WEB Objects
	//Screen 1
	public static final String titleExportAcceptance = "xpath=//*[contains(text(),'"+TestBase.Gettextvalue("exportacceptancetitle")+"')]";
	public static final String lblstartExportAcceptance = "xpath=//*[contains(text(),'"+TestBase.Gettextvalue("createexportacceptancetitle")+"')]";
		
	//Screen 2
	public static final String lblQuanityPieces = "xpath=//*[contains(text(),'"+TestBase.Gettextvalue("quantitypieces")+"')]";
	public static final String lblstatus = "xpath=//*[contains(text(),'"+TestBase.Gettextvalue("status")+"')]";
	public static final String createNewRecordbutton = "xpath=//*[contains(text(),'CREATE NEW RECORD')]";
	
	
	//Screen 3 - Shipment Details
	public static final String lblstartanExportAcceptance = "xpath=//*[contains(text(),'"+TestBase.Gettextvalue("startanexportacceptance")+"')]";
	public static final String lblshipmentDetails = "xpath=//*[contains(text(),'"+TestBase.Gettextvalue("shipmentdetails")+"')]";
	public static final String lblFlightDetails = "xpath=//*[contains(text(),'"+TestBase.Gettextvalue("flightDetailTitle")+"')]";
	public static final String txtCarrier =  "xpath=//*[@placeholder='"+TestBase.Gettextvalue("flightCodeInput")+"']";
	public static final String txtFlight =  "xpath=//*[@placeholder='"+TestBase.Gettextvalue("flightNumberInput")+"']";
	public static final String lblPiecestitle = "xpath=//*[contains(text(),'"+TestBase.Gettextvalue("piecesTitle")+"')]";
	public static final String txtPieces =  "xpath=//*[@placeholder='"+TestBase.Gettextvalue("quantitypieces")+"']";
	public static final String lblWeight = "xpath=//*[contains(text(),'"+TestBase.Gettextvalue("weightTitle")+"')]";
	public static final String txtWeight =  "xpath=//*[@placeholder='"+TestBase.Gettextvalue("weightInput")+"']";
	public static final String lblUnit = "xpath=//*[contains(text(),'"+TestBase.Gettextvalue("unitTitle")+"')]";
	public static final String lblKG = "xpath=//*[contains(text(),'"+TestBase.Gettextvalue("unitInput")+"')]";
	public static final String lblPiecesAlreadyReceived = "xpath=//*[contains(text(),'"+TestBase.Gettextvalue("alreadyreceived")+"')]";
	public static final String lblPiecesAlreadyReceivedQty = "xpath=//*[@class='shipment-received-label shipment-input label label-ios']";
	public static final String lblWtAlreadyReceived = "xpath=//*[contains(text(),'"+TestBase.Gettextvalue("alreadyreceived")+"')]";
	public static final String lblWtAlreadyReceivedQty = "xpath=//*[@class='shipment-received-label shipment-input label label-ios']";
	public static final String Calendaricon = "xpath=//*[@class='calendar-icon icon icon-ios ion-ios-calendar-outline item-icon']";
	
	//Screen 3 - Dimensions
	public static final String lblDimensions = "xpath=//*[contains(text(),' Dimensions')]";
	public static final String lblvolumedetails = "xpath=//*[contains(text(),'"+TestBase.Gettextvalue("volumedetails")+"')]";
	//public static final String lbldimensionsfor = "xpath=//*[contains(text(),'"+TestBase.Gettextvalue("dimensionsfor")+"')]";
	public static final String lbltotalvolume = "xpath=//*[contains(text(),'"+TestBase.Gettextvalue("totalvolume")+"')]";
	public static final String lblUOM = "xpath=//*[@text=' Unit of Measurement']";
	public static final String txttotalvolume = "xpath=//*[@placeholder='"+TestBase.Gettextvalue("totalvolume")+"']";
	public static final String toggleINC = "xpath=//*[contains(text(),'"+TestBase.Gettextvalue("inc")+"')]";
	public static final String toggleCMT = "xpath=//*[contains(text(),'"+TestBase.Gettextvalue("cmt")+"')]";
	public static final String toggleCF = "xpath=//*[contains(text(),'"+TestBase.Gettextvalue("cubicfeet")+"')]";
	public static final String toggleMC = "xpath=//*[contains(text(),'"+TestBase.Gettextvalue("cubicmeter")+"')]";
	public static final String lblPieces= "xpath=//*[contains(text(),'"+TestBase.Gettextvalue("quantitypieces")+"')]";
	public static final String lbllength = "xpath=//*[contains(text(),'"+TestBase.Gettextvalue("length")+"')]";
	public static final String lblWidth = "xpath=//*[contains(text(),'"+TestBase.Gettextvalue("width")+"')]";
	public static final String lblHeight = "xpath=//*[contains(text(),'"+TestBase.Gettextvalue("height")+"')]";
	public static final String txtLength = "xpath=//*[@placeholder='"+TestBase.Gettextvalue("length")+"']";
	public static final String txtWidth = "xpath=//*[@placeholder='"+TestBase.Gettextvalue("width")+"']";
	public static final String txtHeight = "xpath=//*[@placeholder='"+TestBase.Gettextvalue("height")+"']";
	public static final String btnDone = "xpath=//IMG";
	public static final String successfullySavedMessage="xpath=//*[@text='Saved']";
	
	
	
	//Screen 3- Man Page objects
	public static final String lblSHC = "xpath=//*[contains(text(),'"+TestBase.Gettextvalue("shc")+"')]";
	public static final String lblOrigin = "xpath=//*[@text='Origin']";
	public static final String lblOD = "xpath=//INPUT";
	public static final String lblDestination = "xpath=//*[contains(text(),'"+TestBase.Gettextvalue("destination")+"')]";
	public static final String lblBUP = "xpath=//*[contains(text(),'BUP Details')]";
	public static final String lblULDTitle = "xpath=//*[contains(text(),'"+TestBase.Gettextvalue("uldTitle")+"')]";
	public static final String lblPreferredLocation = "xpath=//*[contains(text(),'"+TestBase.Gettextvalue("preferredlocation")+"')]";
	public static final String lblStorageLocation = "xpath=//*[contains(text(),'"+TestBase.Gettextvalue("storagelocation")+"')]";
	public static final String lblEUPallet = "xpath=//*[contains(text(),'"+TestBase.Gettextvalue("eupallet")+"')]";
	public static final String lblHandlingDetails = "xpath=//*[contains(text(),'Handling Details')]";
	public static final String lblOCIDetails = "xpath=//*[contains(text(),'"+TestBase.Gettextvalue("ocidetails")+"')]";
	public static final String StorageLocationField ="//*[@placeholder='Storage location']";
	public static final String Remarks ="xpath=//*[contains(text(),'Remarks')]";
	public static final String RemarksField ="xpath=//*[@placeholder='Remarks']";
	public static final String lblReadyForCarriage ="xpath=//*[contains(text(),'Ready For Carriage')]";
	public static final String lblFWB= "xpath=//*[contains(text(),'FWB')]";
	
	//*********    OCI DETAILS *************** //
	
	public static final String lblManual = "xpath=//*[@text='MANUAL']";
	public static final String SSRText ="xpath=//*[@type='textarea']";
	public static final String OSIText ="xpath=//*[@type='textarea']";
	public static final String lblSecured ="xpath=//*[@text=' Secured']";
	public static final String btnBoolean = "xpath=//*[@type='button']";
	
	// ******  BUP Details ************ //
	public static final String ULDIdentifier ="xpath=//*[@placeholder='ULD identifier ']";
	public static final String lblULDIdentifier ="xpath=//*[@text='ULD Identifier']";
	public static final String lblULDStorageLocation = "xpath=//*[@text='ULD storage location']";
	public static final String ULDStorageLocation = "xpath=//*[@placeholder='ULD storage location ']";
	public static final String ULDDestination="xpath=//*[@text='ULD Destination']";
	
	
	//***********EU Pallet  ************//
	
	public static final String PalletIncrementButton = "xpath=//*[@text='+']";
	
	
	//*********** Error MESSAges *************  //
	public static final String HWBErrorMessage = "xpath=//*[@text='Invalid HWB Number']";
	
	
	//************************* HWB  ***********************
	
	public static final String HWBTitle ="xpath=//*[contains(text(),'HWB')]";
	
	public static final String HWBListTitle="xpath=//*[@text='HWB List']";
	
	public static final String HWBNumber ="xpath=//*[@nodeName='P']";
	 
	
	//******HWB Screen *************** //
	
	public static final String HWBDetails ="xpath=//*[contains(text(),'HWB Details')]";
	
	public static final String BackArrow= "xpath=(//*[@type='submit'])[1]";
	
	public static final String  Volume = "xpath=//*[@type='submit']";
	
	

	
	
	
}