package com.company.pages;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import com.company.auto.TestBase;

public class PagesHomeMenus extends TestBase{
	
	//WEB Objects
	public static final String mnuHome = "xpath=//*[@src='assets/images/menu.svg']";
	public static final String btnArrow = "xpath=//*[@nodeName='BUTTON' and ./*[@text='        ']]";
	public static final String txtMenu = "xpath=//*[contains(text(),'"+TestBase.Gettextvalue("menu")+"')]";
	
	public static final String mnuitemImport = "xpath=//*[contains(text(),' Import')]";
	public static final String mnuitemExport = "xpath=//*[contains(text(),'"+TestBase.Gettextvalue("menuitemexport")+"')]";
	public static final String mnuitemAcceptance = "xpath=//*[contains(text(),'"+TestBase.Gettextvalue("acceptance")+"')]";
	public static final String mnuitemDeliveriesTransfers = "xpath=//*[contains(text(),'"+TestBase.Gettextvalue("menu-delivery")+"')]";
	public static final String mnuitemFoundCargo = "xpath=//*[contains(text(),'Found Cargo')]";
	public static final String mnuitemReceiptBondTransfer = "xpath=//*[contains(text(),'"+TestBase.Gettextvalue("receiptbondtransfer")+"')]";
	public static final String mnuitemAirlineTransfer = "xpath=//*[contains(text(),'"+TestBase.Gettextvalue("airlinetransfer")+"')]";
	//public static final String mnuitemWarehouse = "xpath=//*[contains(text(),'Warehouse')]";
	
	public static final String mnuitemWarehouse = "xpath=//*[contains(text(),'Warehouse')]";
	public static final String mnuitemDamage = "xpath=//*[contains(text(),'Damage Report')]";
	public static final String mnuitemIrregularities = "xpath=//*[@text='Irregularity Report']";
	public static final String mnuitemOtherServices = "xpath=//*[contains(text(),'"+TestBase.Gettextvalue("menuitemotherservices")+"')]";
	public static final String mnuitemAdditionalservices = "//*[@text='Additional Services']";
	public static final String checklist = "xpath=//*[contains(text(),'"+TestBase.Gettextvalue("checklist")+"')]";
	
	//Mock and API switching
	public static final String mnuMore = "xpath=//*[@src='assets/images/dotmenu.svg']";	
	public static final String mnuitemAPI = "xpath=//*[contains(text(),'Using API')]";
	
	//Common Objects
	public static final String lblSuccessfullysaved ="xpath=//*[@text='"+ TestBase.Gettextvalue("savedsuccessfully")+"']";
	
	//HWB pop up Objects
	public static final String lblResults = "xpath=//*[contains(text(),'AWB Results For')]";
	public static final String btnCancel = "xpath=//*[@text='CANCEL']";	
	public static final String btnSave = "xpath=//*[contains(text(),' SAVE')]";
	
	//Common Search Page Objects
	public static final String lblEnterNumber = "xpath=//*[contains(text(),'"+TestBase.Gettextvalue("enterNumber")+"')]";
	public static final String lblorScan = "xpath=//*[contains(text(),'"+TestBase.Gettextvalue("orScan")+"')]";
	public static final String btnAWB = "xpath=//*[contains(text(),'"+TestBase.Gettextvalue("awb")+"')]";
	public static final String btnHWB="xpath=//*[contains(text(),'"+TestBase.Gettextvalue("hwb")+"')]";
	public static final String btnFlight="xpath=//*[contains(text(),'"+TestBase.Gettextvalue("flight")+"')]";
	public static final String btnULD="xpath=//*[contains(text(),'"+TestBase.Gettextvalue("uld")+"')]";
	public static final String btnScanBarcode = "xpath=//*[contains(text(),'"+TestBase.Gettextvalue("scan")+"')]";
	public static final String btnABC ="//*[@src='assets/images/alpha_icon.svg']";
	public static final String btnSearch = "//*[@css='BUTTON.search-button']";
	public static final String btnSearchname = "xpath=//*[@name='search']";
	public static final String btnSearchDisabledinitial = "xpath=//*[contains(@class,'search-disabled')]";
	public static final String btnSearchDisabledreload = "xpath=//*[contains(@class,'search-disabled')]";
	public static final String btnSearchEnabled ="xpath=//*[contains(@class,'search-enabled')]";
	public static final String txtPrefixNumber = "xpath=//*[@placeholder='"+TestBase.Gettextvalue("prefix")+"']";
	public static final String txtSerialNumber = "//*[@placeholder='Serial number']";
	public static final String calendaricon = "xpath=//*[@class='app-calendar-icon']";
	public static final String txtSearchFlight = "xpath=//*[@placeholder='"+TestBase.Gettextvalue("searchflight")+"']";
	public static final String btnProceed = "xpath=//*[contains(text(),' PROCEED')]";
	public static final String txtULD = "xpath=//*[@placeholder='ULD identifier']";
	public static Calendar cal = Calendar.getInstance();
    public static SimpleDateFormat sdf = new SimpleDateFormat("ddMMMyy");
    public static final String strDate = sdf.format(cal.getTime()).toUpperCase();
    public static final String txtlabelDate = "xpath=//*[contains(text(),'"+strDate+"')]";
    public static final String lblInvalidULD = "xpath=//*[contains(text(),'"+TestBase.Gettextvalue("invaliduldindentifier")+"')]";
    public static final String BUILDUP = "//*[contains(text(),'Build up')]";
    
    
    public static final String MenuRelocation ="xpath=//*[@text='Relocation']";
	public static final String MenuLocationHistory ="xpath=//*[contains(text(),'Location History')]";
	public static final String MenuInventoryCheck ="xpath=//*[@text='Inventory Check']";
	public static final String ProceedAssignment  ="xpath=//*[@text='PROCEED']" ;
	
	
	
	public static final String checkinMenu ="xpath=//*[@text='Check-In']";
	
	
	





}