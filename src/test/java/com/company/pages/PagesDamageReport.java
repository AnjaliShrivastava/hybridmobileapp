package com.company.pages;

import com.company.auto.TestBase;

public class PagesDamageReport {

	//WEB Objects	
	
	//Screen 1
	public static final String lbltitleDamageReport = "xpath=//*[contains(text(),'"+TestBase.Gettextvalue("damagereport")+"')]";
	public static final String lblDamageReport = "xpath=//*[contains(text(),'"+TestBase.Gettextvalue("createOrViewDamageReport")+"')]";
	
	//Screen 2
	public static final String lblDamageReportLanding = "xpath=//*[contains(text(),'"+TestBase.Gettextvalue("createdamagereporttitle")+"')]";
	public static final String btnCreateReport = "xpath=//*[contains(text(),'"+TestBase.Gettextvalue("createnewreport")+"')]";
	
	//Screen 3 //Add Proof
	public static final String btnShipmentDetails = "xpath=//*[contains(text(),'"+TestBase.Gettextvalue("shipmentdetails")+"')]";
//	public static final String btnCamera = "xpath=//*[contains(text(),'camerawhite')]";
//	public static final String btnRecordVideo = "xpath=//*[contains(text(),'"+TestBase.Gettextvalue("recordVideo")+"')]";
//	public static final String btnCaptureImage = "xpath=//*[contains(text(),'"+TestBase.Gettextvalue("captureImage")+"')]";
	public static final String lblUseCamera = "xpath=//*[contains(text(),'"+TestBase.Gettextvalue("usecamera")+"')]";
	public static final String lblUseGallery = "xpath=//*[contains(text(),'"+TestBase.Gettextvalue("usegallery")+"')]";
	
	//Screen4 - Shipment Details
	public static final String txtflight = "xpath=//*[@placeholder='"+TestBase.Gettextvalue("flightNumberPlaceholder")+"']";
	public static final String txtDamagedPieces ="xpath=//*[@placeholder='"+TestBase.Gettextvalue("piecesInput")+"']";
	public static final String txtWeight ="xpath=//*[@placeholder='"+TestBase.Gettextvalue("weightInput")+"']";
	public static final String lblULDIdentifier = "xpath=//*[contains(text(),'"+TestBase.Gettextvalue("uldTitle")+"')]";
	public static final String txtULD = "xpath=//*[@placeholder='ULD identifier ']";
	public static final String btnNext = "xpath=//*[contains(text(),'NEXT')]";
	
	//Screen 5 - Packing Details
	public static final String lblPackingDetails = "xpath=//*[contains(text(),' Packing Details')]";
	
	//Screen 6 - Damage Details
	public static final String lblDamageDetails = "//*[contains(text(),' Damage Details ')]";
	
	//Screen 7 - Damage Detection
	public static final String lblDamageDetection = "xpath=//*[contains(text(),' Damage Detection')]";
	
	//Screen 8 - Recuperation
	public static final String lblRecuperation = "xpath=//*[contains(text(),'"+TestBase.Gettextvalue("recuperation")+"')]";
	
	//Screen 9 - Remarks and Boxes
	public static final String lblRemarksandBoxes = "xpath=//*[contains(text(),'"+TestBase.Gettextvalue("remarksandboxes")+"')]";
	public static final String lblRemarks = "xpath=//*[contains(text(),'"+TestBase.Gettextvalue("remarksTitle")+"')]";
	public static final String txtRemarks = "xpath=//*[@placeholder='"+TestBase.Gettextvalue("remarksTitle")+"']";
	public static final String lblNumberofBoxes = "xpath=//*[contains(text(),'"+TestBase.Gettextvalue("boxesTitle")+"')]";
	public static final String txtBoxes = "xpath=//*[@placeholder='"+TestBase.Gettextvalue("boxesTitle")+"']";
	public static final String btnSave = "xpath=//*[contains(text(),'"+TestBase.Gettextvalue("save")+"')]";
	public static final String btnPrint = "xpath=//*[contains(text(),'"+TestBase.Gettextvalue("print")+"')]";
	public static final String btnEmail = "xpath=//*[contains(text(),'"+TestBase.Gettextvalue("email")+"')]";
	public static final String btnBadge = "xpath=//*[@src='assets/images/completed.svg']";
	
	//************Screen - Add Proof ********************* //
	
	public static final String btnAddProof = "xpath=//*[contains(text(),'Add Proof')]";
	public static final String btnUseCamera ="//*[contains(text(),' Use Camera')]" ;
	public static final String btnUseGallery ="//*[contains(text(),' Use Gallery')]";
	public static final String btnRecordVideo = "xpath=//*[@text='Record Video']";
	public static final String btnCaptureImage = "xpath=//*[@text='Capture Image']";
	public static final String btnPickImage = "xpath=//*[@text='Pick Image']";
	public static final String btnPickVideo="xpath=//*[@text='Pick Video']";
	public static final String MediaSaveButton ="xpath=//*[contains(text(),'SAVE')]";
	public static final String NextButton="xpath=//*[contains(text(),'NEXT')]";
	public static final String OKButton ="//*[@id='OK']";
	
	
	
	
	
	
	
}