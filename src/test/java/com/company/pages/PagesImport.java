package com.company.pages;

import com.company.auto.TestBase;

public class PagesImport extends TestBase {

	//Screen 2

	public static final String Import ="xpath=//*[contains(text(),'Check In')]";
	public static final String titleImport ="xpath=//*[contains(text(),'Check-In')]";
	public static final String lblStartAnImprort ="xpath=//*[contains(text(),'START AN IMPORT PROCESS')]";
	public static final String fgtErrormessage = "xpath=//*[contains(text(),'Invalid Flight Number')]";
    public static final String nextButton =  "xpath=//*[contains(text(),'NEXT')]";

	public static final String doneButton =  "xpath=//*[contains(text(),'Done')]";
	public static final String saveButton =  "xpath=//*[contains(text(),'SAVE')]";
	
	public static final String lblSaved =  "xpath=//*[@text='Saved']";
	
	// ********  Cargo Arrival Details *************
	
	public static final String fgtCheckinCompleteButton  ="xpath=(//*[@nodeName='BUTTON'])[3]";
	public static final String lblFgtCheckinCompleteButton="xpath=//*[@text=' Flight Check-In Complete']";
	public static final String cargoArrivalText ="xpath=//*[@text='Cargo Arrival Details']";
	public static final String legDetailsText ="xpath=//*[contains(text(),'Leg Details')]";
	public static final String legLHR ="xpath=//*[contains(text(),'LHR')]";
	public static final String legFRA ="xpath=//*[contains(text(),'FRA-Frankfurt')]";
	public static final String allLeg ="xpath=//*[contains(text(),'All Legs')]";
	public static final String cargoArrivalButton  ="xpath=(//*[@nodeName='BUTTON'])[3]";
	public static final String checkInTxt = "xpath=//*[@text=' Check-in for']";
	public static final String cargoArrivalDetails ="xpath=//*[@text='Cargo Arrival Complete']";
	public static final String bulkULD ="xpath=//*[@text='BULK']";
	public static final String ThruULD ="xpath=//*[@text='Thru']";
	public static final String tImpULD ="public static final String bulkULD" ;
    


} 