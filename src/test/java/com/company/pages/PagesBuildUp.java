package com.company.pages;

import com.company.auto.TestBase;

public class PagesBuildUp extends TestBase{

	//WEB Objects
	//Screen 1
	public static final String btnSHOWULDs ="xpath=//*[@placeholder='Search for ULD ']";
	public static final String btnSHOWSHIPMENT ="xpath=//*[contains(text(),'SHOW SHIPMENTS')]";
	public static final String btnAssignShipment ="xpath=//*[contains(text(),'Assign Shipments')]";
	public static final String viewULDdetails ="xpath=//*[contains(text(),'VIEW ULD DETAILS')]";
	public static final String viewShipment ="xpath=//*[contains(text(),'VIEW SHIPMENTS')]";
	public static final String btnAssign="xpath=//*[contains(text(),'Assign')]";
	public static final String btnSplit="xpath=//*[contains(text(),'SPLIT')]";
	public static final String StatusPRE ="xpath=//*[contains(text(),'PRE')]";
	public static final String productArrow ="xpath=//*[@class='select-icon-inner']";
	public static final String searchULDField ="xpath=//*[@placeholder='Search for ULD ']";
	public static final String radioButton="xpath=//*[@class='radio-icon']";
	
	
	///IPAD ------------
	
	public static final String Filter ="xpath=//*[contains(text(),'Filter')]";
	public static final String btnApply="//*[@text='APPLY']";
	
	public static final String btnPROCEED ="xpath=//*[contains(text(),'PROCEED')]";
	public static final String AWBsearchBar = "xpath=//*[@placeholder='Search shipments, SHC ']";
	
	
	
	
}