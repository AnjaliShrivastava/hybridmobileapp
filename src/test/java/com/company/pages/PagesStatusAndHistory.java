package com.company.pages;

import com.company.auto.TestBase;

public class PagesStatusAndHistory extends TestBase {

	//Screen 2

	
	public static final String lblCurrentLocation = "xpath=//*[@text='Current Location']";
    public static final String pcsInputField =  "xpath=//*[@placeholder='Pieces']";

	public static final String relocateButton =  "xpath=//*[contains(text(),'RELOCATE')]";
	public static final String locationField=     "xpath=//*[@placeholder='Location ']" ;
	public static final String saveButton =  "xpath=//*[contains(text(),'SAVE')]";
	public static final String ULDStatusdropdown ="xpath=//*[@class='select-icon-inner']";
	
	public static final String lblSaved =  "xpath=//*[@text='Saved']";
	
	// ********  Inventory Check *************
	
	public static final String ICLocationField = "xpath=//*[@placeholder='Enter location']";
	public static final String LblInventoryCheck = "xpath=//*[@text='Inventory Check']";
	public static final String PiecesFieldIV ="xpath=//*[@placeholder='Pieces']" ;
	public static final String PiecesTextlbl ="//*[@text='Pieces']";
	public static final String NClbl ="xpath=//*[contains(text(),'Not checked')]";
	
	
	public static final String PAUSEBUTTON="xpath=//*[contains(text(),'PAUSE')]";
	public static final String COMPLETEBUTTON="xpath=//*[contains(text(),'COMPLETE')]";	
	public static final String FINALIZEBUTTON ="xpath=//*[contains(text(),' FINALISE')]";
	public static final String ADDSHIPMENT ="xpath=//*[contains(text(),'ADD SHIPMENTS')]";
	
	
	public static final String PausedText ="xpath=//*[contains(text(),'Paused')]";
	public static final String IVCheckSummary="xpath=//*[@text='Inventory Check Summary']";
	public static final String CompletedText ="xpath=//*[contains(text(),'Completed')]";
	
	public static final String oneLocationCompleted ="xpath//*[@text='1 Location(s) completed']";
	public static final String PrintButton ="xpath=//*[contains(text(),' PRINT')]" ;
	public static final String EmailButton ="xpath=//*[contains(text(),' EMAIL')]" ;
	
	
	public static final String incompleteWarning ="xpath=//*[@text='There are 1 incomplete locations']" ;
	public static final String ProceedButton ="//*[contains(text(),'PROCEED')]" ;
	
	
	// ADD Shipment ...............
	
	public static final String lblAddShipments ="xpath=//*[contains(text(),'Add Shipments')]";
	public static final String SAVEBUTTON ="//*[contains(text(),'SAVE')]";
	public static final String SuccessfullyAddedMSd="xpath=//*[@text='Successfully Added']";
	
	/// Next Location ............
	
	public static final String NEXTLOCATIONBUTTON = "xpath=//*[contains(text(),' NEXT LOCATION')]";
	public static final String LoadingIcon="//*[@text='Loading']";
	
	// Location History ............
	
	
	public static final String ShipmentDetails="xpath=//*[contains(text(),'Shipment Details')]";
	public static final String lblCurrentLocations="xpath=//*[contains(text(),'Current Locations')]";
	public static final String lblInPieces ="xpath=//*[contains(text(),'In Pieces')]";
	public static final String lblOutPieces ="xpath=//*[contains(text(),'Out Pieces')]";
	
    


} 