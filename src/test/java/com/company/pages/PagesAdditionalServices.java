package com.company.pages;

import com.company.auto.TestBase;

public class PagesAdditionalServices extends TestBase{

	//WEB Objects
	//Screen 2
	public static final String titleAdditionalServices = "xpath=//*[contains(text(),'"+TestBase.Gettextvalue("additionalservicestitle")+"')]";
	public static final String lblRegisterAdditionalServices = "xpath=//*[contains(text(),'"+TestBase.Gettextvalue("registeradditionalservices")+"')]";
	public static final String imgAdditionalServices = "//*[@src='assets/images/additionalservices.svg']";
	
	//Screen3 
	public static final String txtSearchCode = "xpath=//*[@placeholder='"+TestBase.Gettextvalue("searchcodeordescription")+"']";
	public static final String lblAddServicesFor = "xpath=//*[contains(text(),'"+TestBase.Gettextvalue("addServicesFor")+"')]";
	public static final String btnSearch = "//*[@name='"+TestBase.Gettextvalue("search")+"']";
	public static final String lblCode = "xpath=//*[contains(text(),'"+TestBase.Gettextvalue("code")+"')]";
	public static final String lblDescription = "xpath=//*[contains(text(),'"+TestBase.Gettextvalue("description")+"')]";
	public static final String lblQuantity = "xpath=//*[contains(text(),'"+TestBase.Gettextvalue("quantity")+"')]";
	public static final String lblSearchResults = "xpath=//*[contains(text(),'"+TestBase.Gettextvalue("searchresults")+"')]";
	//Screen 4
	public static final String txtQuantity = "xpath=//*[@placeholder='"+TestBase.Gettextvalue("quantity")+"']";
	public static final String btnCancel = "xpath=//*[contains(text(),'"+TestBase.Gettextvalue("cancel")+"')]";
	public static final String btnDone = "xpath=//*[@text=' SAVE']";
	
	
	//Pop Up
	public static final String lblLegSelectionFor = "xpath=//*[contains(text(),'"+TestBase.Gettextvalue("legselectionfor")+"')]";
	public static final String lblImport = "xpath=//*[contains(text(),'"+TestBase.Gettextvalue("menuitemimport")+"')]";
	public static final String lblExport = "xpath=//*[contains(text(),'"+TestBase.Gettextvalue("menuitemexport")+"')]";
	
	//AWB Flight Selection for ULD Search
	public static final String lblRegisterAddServicesforFlight = "xpath=//*[contains(text(),'"+TestBase.Gettextvalue("addServicesUldTitle")+"')]";
	public static final String txtSearchForFlights ="xpath=//*[@placeholder='"+TestBase.Gettextvalue("searchflightorshipments")+"']";
	public static final String lblflightcounttitle = "xpath=//*[contains(text(),'"+TestBase.Gettextvalue("flightcounttitle")+"')]";
	public static final String lblzeroflightcounttitle = "xpath=//*[contains(text(),'"+TestBase.Gettextvalue("zeroflightcounttitle")+"')]";
	public static final String lblShipments = "xpath=//*[contains(text(),'"+TestBase.Gettextvalue("shipments")+"')]";
	
	//public static final String  = "xpath=//*[contains(text(),'"+TestBase.Gettextvalue("")+"')]";
	
}