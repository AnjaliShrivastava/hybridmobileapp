package com.company.tests;

import com.company.auto.TestBase;

import org.testng.annotations.Test;

import com.company.auto.BCDeliveriesTransfers;
import com.company.auto.BCHomeApplication;

public class DeliveriesTransfers extends TestBase {
	
	@Test
	public void BondTransfer_01() throws Exception {
		BCDeliveriesTransfers DeliveryTransfer = new BCDeliveriesTransfers();
		BCHomeApplication home = new BCHomeApplication();
		home.navigateToActivity("HOME");
		home.navigateToActivity("DELIVERYTRANSFER");			
		DeliveryTransfer.SearchDTReceipt(data);
		DeliveryTransfer.ProcessDTReceipt(data);
	}
	@Test
	public void DeliveryReceipt_01() throws Exception {
		BCDeliveriesTransfers DeliveryTransfer = new BCDeliveriesTransfers();
		BCHomeApplication home = new BCHomeApplication();
		home.navigateToActivity("HOME");
		home.navigateToActivity("DELIVERYTRANSFER");				
		DeliveryTransfer.SearchDTReceipt(data);
		DeliveryTransfer.ProcessDTReceipt(data);
	}
	@Test
	public void ReceiptNumberValidation01_N() throws Exception {
		BCDeliveriesTransfers DeliveryTransfer = new BCDeliveriesTransfers();
		BCHomeApplication home = new BCHomeApplication();
		home.navigateToActivity("HOME");
		home.navigateToActivity("DELIVERYTRANSFER");	
		DeliveryTransfer.SearchDTReceipt(data);
	}
	@Test
	public void ReceiptNumberValidation02_N() throws Exception {
		BCDeliveriesTransfers DeliveryTransfer = new BCDeliveriesTransfers();
		BCHomeApplication home = new BCHomeApplication();
		home.navigateToActivity("HOME");
		home.navigateToActivity("DELIVERYTRANSFER");
		DeliveryTransfer.SearchDTReceipt(data);
	}
}