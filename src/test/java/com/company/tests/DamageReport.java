package com.company.tests;

import java.util.Map;

import org.testng.annotations.Test;

import com.company.auto.BCDamageReport;
import com.company.auto.BCHomeApplication;
import com.company.auto.TestBase;


public class DamageReport extends TestBase {




 @Test
	public void CreateDamageReport_AWB_MandatoryDetails() throws Exception {
		BCDamageReport DamageReport = new BCDamageReport();
		BCHomeApplication home = new BCHomeApplication();
		home.navigateToActivity("HOME");
		home.navigateToActivity("DAMAGEREPORT");
		DamageReport.SearchDamageReport(data);
		DamageReport.createNewReport(data);
		DamageReport.ShippingAddress(data);
		DamageReport.verifyReport();
	}	


   @Test
	public void CreateDamageReportWithImage() throws Exception {
	   BCDamageReport DamageReport = new BCDamageReport();
		BCHomeApplication home = new BCHomeApplication();
		home.navigateToActivity("HOME");
		home.navigateToActivity("DAMAGEREPORT");
		System.out.println("This isdamage Report");
		DamageReport.SearchDamageReport(data);
		DamageReport.createNewReport(data);
        DamageReport.CaptureImage(data);
        DamageReport.Save();
      // DamageReport.verifyReport();
        
	}
	
	//@Test
	public void CreateDamageReportWithVideo() throws Exception {
		BCDamageReport DamageReport = new BCDamageReport();
		BCHomeApplication home = new BCHomeApplication();
		home.navigateToActivity("HOME");
		home.navigateToActivity("DAMAGEREPORT");
		DamageReport.SearchDamageReport(data);
		DamageReport.createNewReport(data);
        DamageReport.CaptureVideo(data);
        DamageReport.Save();
        
	}
	
	//@Test
	public void CreateDamageReportWithexitingMedia() throws Exception {
		BCDamageReport DamageReport = new BCDamageReport();
		BCHomeApplication home = new BCHomeApplication();
		home.navigateToActivity("HOME");
		home.navigateToActivity("DAMAGEREPORT");
		DamageReport.SearchDamageReport(data);
		DamageReport.createNewReport(data);
        DamageReport.useExitingGallery(data);
        DamageReport.Save();
        
	}
	
	//@Test
	public void CreateDamageReportWithMedia() throws Exception {
		BCDamageReport DamageReport = new BCDamageReport();
		BCHomeApplication home = new BCHomeApplication();
		home.navigateToActivity("HOME");
		home.navigateToActivity("DAMAGEREPORT");
		DamageReport.SearchDamageReport(data);
		DamageReport.createNewReport(data);
		DamageReport.CaptureImage(data);
		DamageReport.ShippingAddress(data);
	}
	
	@Test
		public void CreateDamageReportWithHWB() throws Exception {
		BCDamageReport DamageReport = new BCDamageReport();
		BCHomeApplication home = new BCHomeApplication();
		home.navigateToActivity("HOME");
		home.navigateToActivity("DAMAGEREPORT");
			DamageReport.Search(data);
			DamageReport.createNewReport(data);
			DamageReport.ShippingAddress(data);
		}
	
	//@Test
		public void CreateDamageReportWithMediaUsingHWB() throws Exception {
		BCDamageReport DamageReport = new BCDamageReport();
		BCHomeApplication home = new BCHomeApplication();
		home.navigateToActivity("HOME");
		home.navigateToActivity("DAMAGEREPORT");
			DamageReport.Search(data);
			DamageReport.createNewReport(data);
			DamageReport.CaptureImage(data);
			DamageReport.ShippingAddress(data);
		}
	//@Test
		public void CreateDamageReportWithImageUsingHWB() throws Exception {
		BCDamageReport DamageReport = new BCDamageReport();
		BCHomeApplication home = new BCHomeApplication();
		home.navigateToActivity("HOME");
		home.navigateToActivity("DAMAGEREPORT");
			DamageReport.Search(data);
			DamageReport.createNewReport(data);
	        DamageReport.CaptureImage(data);
	        DamageReport.Save();
	        
		}
	   
	  //@Test
		public void CreateDamageReportWithexitingMediaUsingHWB() throws Exception {
		BCDamageReport DamageReport = new BCDamageReport();
		BCHomeApplication home = new BCHomeApplication();
		home.navigateToActivity("HOME");
		home.navigateToActivity("DAMAGEREPORT");
			DamageReport.Search(data);
			DamageReport.createNewReport(data);
	        DamageReport.useExitingGallery(data);
	        DamageReport.Save();
	        
	        

	        
		}
	
	//@Test
	public void ScanBARcodeTRAIAL() throws Exception {
		BCDamageReport DamageReport = new BCDamageReport();
		BCHomeApplication home = new BCHomeApplication();
		home.navigateToActivity("HOME");
		home.navigateToActivity("DAMAGEREPORT");
		//application.choosemock();
		DamageReport.scanBarcode();
		DamageReport.createNewReport(data);
        DamageReport.useExitingGallery(data);
        DamageReport.Save();
	}
	
	
	//@Test
	public void ScanBARcodeWithImage() throws Exception {
		BCDamageReport DamageReport = new BCDamageReport();
		//application.choosemock();
		DamageReport.scanBarcode();
		DamageReport.createNewReport(data);
        DamageReport.CaptureImage(data);
        DamageReport.Save();
	}
	
	
	
	
	
	
	
	


}