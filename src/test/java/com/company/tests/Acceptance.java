package com.company.tests;


import com.company.auto.TestBase;

import org.testng.annotations.Test;

import com.company.auto.BCAcceptance;
import com.company.auto.BCDeliveriesTransfers;
import com.company.auto.BCHomeApplication;

public class Acceptance extends TestBase {

	@Test
	public void ACC_CreateAcceptance() throws Exception {
		BCAcceptance acceptance = new BCAcceptance();
		BCHomeApplication home = new BCHomeApplication();		
		home.navigateToActivity("HOME");
		home.navigateToActivity("ACCEPTANCE");	
		//application.choosemock();
		acceptance.AcceptanceSearchPageValidation();
		acceptance.Search(data);
		acceptance.CreateNewPart();
		acceptance.shipmentDetails(data);
		//acceptance.Dimensions(data);
		//acceptance.BUPDetails(data);
		acceptance.OriginDestination(data);
		//acceptance.SHC(data);
		acceptance.StorageLocation(data);
		//acceptance.EUPallet(data);
		//acceptance.handlinDetails(data);
		//acceptance.Secured(data);		
		//acceptance.Remarks(data);
		//acceptance.ReadyForCarriage(data);
		//acceptance.OCIAndBlockMenifest(data);
		acceptance.Save();
		//acceptance.Search(data);
		//acceptance.verifyThePart(data);
	}	

	//@Test
	public void ACC_UpdateAWBPart() throws Exception {
		BCAcceptance acceptance = new BCAcceptance();
		BCHomeApplication home = new BCHomeApplication();		
		home.navigateToActivity("HOME");
		home.navigateToActivity("ACCEPTANCE");	
		//home.choosemock();
		acceptance.Search(data);
		acceptance.updateExistingPart();
		acceptance.shipmentDetails(data);
		acceptance.Dimensions(data);
		acceptance.OriginDestination(data);
		acceptance.StorageLocation(data);
		acceptance.handlinDetails(data);
		//acceptance.Secured(data);
		acceptance.OCIAndBlockMenifest(data);
		acceptance.Remarks(data);
		acceptance.Save();
		acceptance.Search(data);
		acceptance.verifyThePart(data);
	}	

	//@Test
	public void ACC_UpdateMasterAWBWithoutHWBPart() throws Exception {
		BCAcceptance acceptance = new BCAcceptance();
		BCHomeApplication home = new BCHomeApplication();		
		home.navigateToActivity("HOME");
		home.navigateToActivity("ACCEPTANCE");		
		home.choosemock();
		acceptance.Search(data);
		acceptance.updateExistingPart();
		acceptance.Dimensions(data);
		acceptance.OriginDestination(data);
		acceptance.BUPDetails(data);
		acceptance.StorageLocation(data);
		acceptance.EUPallet(data);
		acceptance.handlinDetails(data);
		acceptance.Secured(data);
		acceptance.OCIAndBlockMenifest(data);
		acceptance.Remarks(data);
		acceptance.ReadyForCarriage(data);
		acceptance.Save();

	}

	//@Test
	public void ACC_UpdateMasterAWBWithHWB() throws Exception {

		BCAcceptance acceptance = new BCAcceptance();
		BCHomeApplication home = new BCHomeApplication();		
		home.navigateToActivity("HOME");
		home.navigateToActivity("ACCEPTANCE");	
		home.choosemock();
		acceptance.Search(data);
		acceptance.updateExistingPart();
		acceptance.shipmentDetails(data);
		acceptance.HWBCard(data);
		acceptance.OriginDestination(data);		
		acceptance.BUPDetails(data);
		acceptance.EUPallet(data);
		acceptance.handlinDetails(data);
		acceptance.Secured(data);
		acceptance.OCIAndBlockMenifest(data);
		acceptance.Remarks(data);
		acceptance.ReadyForCarriage(data);
		acceptance.Save();
	}
	//@Test
	public void ACC_HWBValidation() throws Exception {

		BCAcceptance acceptance = new BCAcceptance();
		BCHomeApplication home = new BCHomeApplication();		
		home.navigateToActivity("HOME");
		home.navigateToActivity("ACCEPTANCE");	
		acceptance.NegativeValidation(data);

	}


	@Test
	public void ACC_CreateAcceptancewithNonExistingAWB() throws Exception {
		BCAcceptance acceptance = new BCAcceptance();
		BCHomeApplication home = new BCHomeApplication();		
		home.navigateToActivity("HOME");
		home.navigateToActivity("ACCEPTANCE");	
		//application.choosemock();
		acceptance.Search(data);
		acceptance.CreateNewPart();
		acceptance.shipmentDetails(data);
		acceptance.Dimensions(data);
		acceptance.SHC(data);
		acceptance.OriginDestination(data);
		acceptance.StorageLocation(data);
		acceptance.Save();
		acceptance.Search(data);
		acceptance.verifyThePart(data);
	}

	//@Test
	public void ACC_SHCValidation() throws Exception {
		BCAcceptance acceptance = new BCAcceptance();
		BCHomeApplication home = new BCHomeApplication();		
		home.navigateToActivity("HOME");
		home.navigateToActivity("ACCEPTANCE");		
		acceptance.Search(data);
		acceptance.CreateNewPart();	
		acceptance.SHCNegativeValidation(data);

	}







}