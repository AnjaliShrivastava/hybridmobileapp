package com.company.tests;


import com.company.auto.TestBase;

import org.testng.annotations.Test;

import com.company.auto.BCAdditionalServices;
import com.company.auto.BCFoundCargo;
import com.company.auto.BCHomeApplication;
import com.company.auto.BCImport;

public class Import extends TestBase {
	
	@Test
	public void FLightFlow_CARGOARRIVAl_AND_FlightCheckIn() throws Exception {
		BCAdditionalServices addServices = new BCAdditionalServices();
		BCImport imp = new BCImport() ;
		BCHomeApplication home = new BCHomeApplication();
		home.navigateToActivity("HOME");
		home.navigateToActivity("CHECKIN");	
		addServices.Search(data);
		imp.CargoArrivalCompletion(data);
		imp.FlightCheckIn(data);
		
				
	}
	@Test
	public void FLightFlow_ULDCheckIn() throws Exception {
		BCFoundCargo FC = new BCFoundCargo();
		BCHomeApplication home = new BCHomeApplication();
		home.navigateToActivity("HOME");
		home.navigateToActivity("FOUNDCARGO");			
		FC.SearchAWB(data);
		FC.RegisterFC(data);
	}
	@Test
	public void FLightFlow_AWBCheckIn() throws Exception {
		BCFoundCargo FC = new BCFoundCargo();
		BCHomeApplication home = new BCHomeApplication();
		home.navigateToActivity("HOME");
		home.navigateToActivity("FOUNDCARGO");			
		FC.SearchAWB(data);
		FC.RegisterFC(data);
	}
	
	@Test
	public void ULDFlow_ULDCheckIn() throws Exception {
		BCFoundCargo FC = new BCFoundCargo();
		BCHomeApplication home = new BCHomeApplication();
		home.navigateToActivity("HOME");
		home.navigateToActivity("FOUNDCARGO");			
		FC.SearchAWB(data);
		FC.RegisterFC(data);
	}
	
	@Test
	public void ULDFlow_AWBCheckIn() throws Exception {
		BCFoundCargo FC = new BCFoundCargo();
		BCHomeApplication home = new BCHomeApplication();
		home.navigateToActivity("HOME");
		home.navigateToActivity("FOUNDCARGO");			
		FC.SearchAWB(data);
		FC.RegisterFC(data);
	}
	@Test
	public void BulkList_AWBCheckIn() throws Exception {
		BCFoundCargo FC = new BCFoundCargo();
		BCHomeApplication home = new BCHomeApplication();
		home.navigateToActivity("HOME");
		home.navigateToActivity("FOUNDCARGO");			
		FC.SearchAWB(data);
		FC.RegisterFC(data);
	}
	@Test
	public void AWBFlow_AWBCheckIn() throws Exception {
		BCFoundCargo FC = new BCFoundCargo();
		BCHomeApplication home = new BCHomeApplication();
		home.navigateToActivity("HOME");
		home.navigateToActivity("FOUNDCARGO");			
		FC.SearchAWB(data);
		FC.RegisterFC(data);
	}
	
		
	
	
	
}