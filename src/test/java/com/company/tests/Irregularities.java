package com.company.tests;

import com.company.auto.TestBase;

import org.testng.annotations.Test;

import com.company.auto.BCHomeApplication;
import com.company.auto.BCIrregularities;

public class Irregularities extends TestBase {
	
	
	@Test
	public void IRR_CreateReport() throws Exception {
		try
		{
		BCIrregularities Irregularities = new BCIrregularities();
		BCHomeApplication home = new BCHomeApplication();
		home.navigateToActivity("HOME");
		home.navigateToActivity("IRREGULARITIES");	
		Irregularities.SearchIrrReport(data);
		Irregularities.CreateIrrReport(data);	
		}
		catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	
	@Test
	public void IRR_HWB_CreateReport() throws Exception {
		BCIrregularities Irregularities = new BCIrregularities();
		BCHomeApplication home = new BCHomeApplication();
		home.navigateToActivity("HOME");
		home.navigateToActivity("IRREGULARITIES");		
		Irregularities.SearchIrrReport(data);
		Irregularities.CreateIrrReport(data);	
	}
	
	//@Test
	public void IRR_HWB_SearchReportValidation_N() throws Exception {
		BCIrregularities Irregularities = new BCIrregularities();
		BCHomeApplication home = new BCHomeApplication();
		home.navigateToActivity("HOME");
		home.navigateToActivity("IRREGULARITIES");	
		Irregularities.SearchIrrReport(data);		
	}
	
	//@Test
	public void IRR_SearchReportValidation_N() throws Exception {
		BCIrregularities Irregularities = new BCIrregularities();
		BCHomeApplication home = new BCHomeApplication();
		home.navigateToActivity("HOME");
		home.navigateToActivity("IRREGULARITIES");		
		Irregularities.SearchIrrReport(data);		
	}
	
}