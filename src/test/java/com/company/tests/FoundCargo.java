package com.company.tests;

import com.company.auto.TestBase;

import org.testng.annotations.Test;

import com.company.auto.BCFoundCargo;
import com.company.auto.BCHomeApplication;

public class FoundCargo extends TestBase {
	
	@Test
	public void FC_CreateReport_1() throws Exception {
		BCFoundCargo FC = new BCFoundCargo();
		BCHomeApplication home = new BCHomeApplication();
		home.navigateToActivity("HOME");
		home.navigateToActivity("FOUNDCARGO");			
		FC.SearchAWB(data);
		FC.RegisterFC(data);		
	}	
	
	
	@Test
	public void FC_CreateReport_NonExist() throws Exception {
		BCFoundCargo FC = new BCFoundCargo();
		BCHomeApplication home = new BCHomeApplication();
		home.navigateToActivity("HOME");
		home.navigateToActivity("FOUNDCARGO");			
		FC.SearchAWB(data);
		FC.RegisterFC(data);		
	}
	
	@Test
	public void FC_CreateReport_UNK() throws Exception {
		BCFoundCargo FC = new BCFoundCargo();
		BCHomeApplication home = new BCHomeApplication();
		home.navigateToActivity("HOME");
		home.navigateToActivity("FOUNDCARGO");	
		FC.SearchAWB(data);
		FC.RegisterFC(data);		
	}	
	
	//@Test
	public void FC_SHCValidation_N() throws Exception {
		BCFoundCargo FC = new BCFoundCargo();
		BCHomeApplication home = new BCHomeApplication();
		home.navigateToActivity("HOME");
		home.navigateToActivity("FOUNDCARGO");
		FC.SearchAWB(data);	
		FC.RegisterFC(data);
	}	
	
}