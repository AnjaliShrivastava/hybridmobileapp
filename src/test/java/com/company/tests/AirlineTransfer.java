package com.company.tests;

import com.company.auto.TestBase;

import org.testng.annotations.Test;

import com.company.auto.BCDeliveriesTransfers;
import com.company.auto.BCHomeApplication;

public class AirlineTransfer extends TestBase {
	
	@Test
	public void AirlineTransfer_01() throws Exception {
		BCDeliveriesTransfers DeliveryTransfer = new BCDeliveriesTransfers();
		BCHomeApplication home = new BCHomeApplication();
		home.navigateToActivity("HOME");
		home.navigateToActivity("AIRLINETRANSFER");					
		DeliveryTransfer.SearchAirlineTransferReceipt(data);
		DeliveryTransfer.AirlineTransfer(data);
	}
	
	@Test
	public void AirlineTransfer_02() throws Exception {
		BCDeliveriesTransfers DeliveryTransfer = new BCDeliveriesTransfers();
		BCHomeApplication home = new BCHomeApplication();
		home.navigateToActivity("HOME");
		home.navigateToActivity("AIRLINETRANSFER");				
		DeliveryTransfer.SearchAirlineTransferReceipt(data);
		DeliveryTransfer.AirlineTransfer(data);
	}
	
	@Test
	public void AirlineTransfer_ReceiptValidation01_N() throws Exception {
		BCDeliveriesTransfers DeliveryTransfer = new BCDeliveriesTransfers();
		BCHomeApplication home = new BCHomeApplication();
		home.navigateToActivity("HOME");
		home.navigateToActivity("AIRLINETRANSFER");			
		DeliveryTransfer.SearchAirlineTransferReceipt(data);
	}
	
	@Test
	public void AirlineTransfer_ReceiptValidation02_N() throws Exception {
		BCDeliveriesTransfers DeliveryTransfer = new BCDeliveriesTransfers();
		BCHomeApplication home = new BCHomeApplication();
		home.navigateToActivity("HOME");
		home.navigateToActivity("AIRLINETRANSFER");				
		DeliveryTransfer.SearchAirlineTransferReceipt(data);
	}
	
}