package com.company.tests;


import com.company.auto.TestBase;

import org.testng.annotations.Test;

import com.company.auto.BCAcceptance;
import com.company.auto.BCFoundCargo;
import com.company.auto.BCHomeApplication;
import com.company.auto.BCStatusAndHistory;

public class StatusAndHistory extends TestBase {
	
	@Test
	public void Relocation_AWB() throws Exception {
		BCStatusAndHistory SH = new BCStatusAndHistory();
		BCHomeApplication home = new BCHomeApplication();
		home.navigateToActivity("HOME");
		home.navigateToActivity("RELOCATION");			
		SH.search(data);
		SH.relocate(data);		
	}	
	
	
	@Test
	public void Relocation_HWB() throws Exception {
		BCStatusAndHistory SH = new BCStatusAndHistory();
		BCHomeApplication home = new BCHomeApplication();
		home.navigateToActivity("HOME");
		home.navigateToActivity("RELOCATION");			
		SH.search(data);
		SH.relocate(data);		
	}
	
	@Test
	public void Relocation_ULD() throws Exception {
		BCStatusAndHistory SH = new BCStatusAndHistory();
		BCHomeApplication home = new BCHomeApplication();
		home.navigateToActivity("HOME");
		home.navigateToActivity("RELOCATION");			
		SH.search(data);
		SH.ULDRelocate(data);		
	}
	
	@Test
	public void InventoryCheck_Pause_Complete() throws Exception {
		BCStatusAndHistory SH = new BCStatusAndHistory();
		BCHomeApplication home = new BCHomeApplication();	
		home.navigateToActivity("HOME");
		home.navigateToActivity("INVENTORYCHECK");			
		SH.search(data);
		SH.PauseCheck(data);
		
	}
	
	@Test
	public void InventoryCheck_Pause_Finalized() throws Exception {
		BCStatusAndHistory SH = new BCStatusAndHistory();
		BCHomeApplication home = new BCHomeApplication();
		home.navigateToActivity("HOME");
		home.navigateToActivity("INVENTORYCHECK");			
		SH.search(data);
		SH.PauseFinalize(data);		
	}
	
	@Test
	public void InventoryCheck_AddShipments_Finalized() throws Exception {
		BCStatusAndHistory SH = new BCStatusAndHistory();
		BCHomeApplication home = new BCHomeApplication();
		home.navigateToActivity("HOME");
		home.navigateToActivity("INVENTORYCHECK");			
		SH.search(data);
		SH.AddShipmentAndFinalize(data);		
	}
	
	@Test
	public void InventoryCheck_MultipleLocations() throws Exception {
		BCStatusAndHistory SH = new BCStatusAndHistory();
		BCHomeApplication home = new BCHomeApplication();
		home.navigateToActivity("HOME");
		home.navigateToActivity("INVENTORYCHECK");			
		SH.search(data);
		SH.MultipleLocationFinalize(data);		
	}
	
	@Test
	public void LocationHistory_AWB() throws Exception {
		BCStatusAndHistory SH = new BCStatusAndHistory();
		BCHomeApplication home = new BCHomeApplication();
		home.navigateToActivity("HOME");
		home.navigateToActivity("LOCATIONHISTORY");			
		SH.search(data);
		SH.ViewLocationHistory(data);		
	}
	
	@Test
	public void LocationHistory_HWB() throws Exception {
		BCStatusAndHistory SH = new BCStatusAndHistory();
		BCHomeApplication home = new BCHomeApplication();
		home.navigateToActivity("HOME");
		home.navigateToActivity("LOCATIONHISTORY");			
		SH.search(data);
		SH.ViewLocationHistory(data);		
	}
	
	
		
	
	
}