package com.company.tests;

import com.company.auto.TestBase;

import org.testng.annotations.Test;

import com.company.auto.BCBuildUP;
import com.company.auto.BCHomeApplication;

public class BuildUp extends TestBase {
	
	@Test
	public void BUILDUP_USINGFLIGHT_SHOWULDs() throws Exception {
		BCBuildUP buildUP = new BCBuildUP();
		BCHomeApplication home = new BCHomeApplication();
		home.navigateToActivity("HOME");
		home.navigateToActivity("BUILDUP");			
		buildUP.search(data);
		buildUP.LegSelection(data);	
		buildUP.ShowULDs(data);		
	}	
	
	@Test
	public void BUILDUP_USINGFLIGHT_SHOWSHIPMENT() throws Exception {
		BCBuildUP buildUP = new BCBuildUP();
		BCHomeApplication home = new BCHomeApplication();
		home.navigateToActivity("HOME");
		home.navigateToActivity("BUILDUP");			
		buildUP.search(data);
		buildUP.LegSelection(data);	
		buildUP.ShowShipments(data);		
	}
	@Test
	public void BUILDUP_USINGFLIGHT_DRAGANDDROP() throws Exception {
		BCBuildUP buildUP = new BCBuildUP();
		BCHomeApplication home = new BCHomeApplication();
		home.navigateToActivity("HOME");
		home.navigateToActivity("BUILDUP");			
		buildUP.search(data);
		buildUP.LegSelection(data);	
		buildUP.DRAGANDDROP(data);		
	}
	
	
}