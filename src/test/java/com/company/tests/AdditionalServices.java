package com.company.tests;


import com.company.auto.TestBase;

import org.testng.annotations.Test;

import com.company.auto.BCAdditionalServices;
import com.company.auto.BCHomeApplication;

public class AdditionalServices extends TestBase {
	
	//@Test
	public void AddServices_SearchPageValidation() throws Exception {
		BCAdditionalServices addServices = new BCAdditionalServices();
		BCHomeApplication home = new BCHomeApplication();
		home.navigateToActivity("HOME");
		home.navigateToActivity("ADDITIONALSERVICES");	
		addServices.SearchPageValidation();
	}	
	@Test
	public void RegisterAddServices_AWB() throws Exception {
		BCAdditionalServices addServices = new BCAdditionalServices();
		BCHomeApplication home = new BCHomeApplication();
		home.navigateToActivity("HOME");
		home.navigateToActivity("ADDITIONALSERVICES");	
		//application.choosemock();
		addServices.Search(data);
		addServices.RegisterAddServices(data);
		addServices.Save();
	}
	
	@Test
	public void RegisterAddServices_FLIGHT_Import() throws Exception {
		BCAdditionalServices addServices = new BCAdditionalServices();
		BCHomeApplication home = new BCHomeApplication();
		home.navigateToActivity("HOME");
		home.navigateToActivity("ADDITIONALSERVICES");
		addServices.Search(data);
		addServices.RegisterAddServices(data);
		addServices.Save();
	}
	
	@Test
	public void RegisterAddServices_FLIGHT_Export() throws Exception {
		BCAdditionalServices addServices = new BCAdditionalServices();
		BCHomeApplication home = new BCHomeApplication();
		home.navigateToActivity("HOME");
		home.navigateToActivity("ADDITIONALSERVICES");
		addServices.Search(data);
		addServices.RegisterAddServices(data);
		addServices.Save();
	}
	
	@Test
	public void RegisterAddServices_ULD_FLIGHT_IMPORT() throws Exception {
		BCAdditionalServices addServices = new BCAdditionalServices();
		BCHomeApplication home = new BCHomeApplication();
		home.navigateToActivity("HOME");
		home.navigateToActivity("ADDITIONALSERVICES");
		addServices.Search(data);
		addServices.RegisterAddServices(data);
		addServices.Save();
	}
	@Test
	public void RegisterAddServices_ULD_FLIGHT_EXPORT() throws Exception {
		BCAdditionalServices addServices = new BCAdditionalServices();
		BCHomeApplication home = new BCHomeApplication();
		home.navigateToActivity("HOME");
		home.navigateToActivity("ADDITIONALSERVICES");
		addServices.Search(data);
		addServices.RegisterAddServices(data);
		addServices.Save();
	}
	@Test
	public void RegisterAddServices_ULD_AWB() throws Exception {
		BCAdditionalServices addServices = new BCAdditionalServices();
		BCHomeApplication home = new BCHomeApplication();
		home.navigateToActivity("HOME");
		home.navigateToActivity("ADDITIONALSERVICES");
		//application.choosemock();
		addServices.Search(data);
		addServices.RegisterAddServices(data);
		addServices.Save();
	}
}